(function(){if(window.jQuery){var _jQuery=window.jQuery
}var jQuery=window.jQuery=function(selector,context){return new jQuery.prototype.init(selector,context)
};
if(window.$){var _$=window.$
}window.$=jQuery;
var quickExpr=/^[^<]*(<(.|\s)+>)[^>]*$|^#(\w+)$/;
var isSimple=/^.[^:#\[\.]*$/;
jQuery.fn=jQuery.prototype={init:function(selector,context){selector=selector||document;
if(selector.nodeType){this[0]=selector;
this.length=1;
return this
}else{if(typeof selector=="string"){var match=quickExpr.exec(selector);
if(match&&(match[1]||!context)){if(match[1]){selector=jQuery.clean([match[1]],context)
}else{var elem=document.getElementById(match[3]);
if(elem){if(elem.id!=match[3]){return jQuery().find(selector)
}else{this[0]=elem;
this.length=1;
return this
}}else{selector=[]
}}}else{return new jQuery(context).find(selector)
}}else{if(jQuery.isFunction(selector)){return new jQuery(document)[jQuery.fn.ready?"ready":"load"](selector)
}}}return this.setArray(selector.constructor==Array&&selector||(selector.jquery||selector.length&&selector!=window&&!selector.nodeType&&selector[0]!=undefined&&selector[0].nodeType)&&jQuery.makeArray(selector)||[selector])
},jquery:"1.2.4a",size:function(){return this.length
},length:0,get:function(num){return num==undefined?jQuery.makeArray(this):this[num]
},pushStack:function(elems){var ret=jQuery(elems);
ret.prevObject=this;
return ret
},setArray:function(elems){this.length=0;
Array.prototype.push.apply(this,elems);
return this
},each:function(callback,args){return jQuery.each(this,callback,args)
},index:function(elem){var ret=-1;
this.each(function(i){if(this==elem){ret=i
}});
return ret
},attr:function(name,value,type){var options=name;
if(name.constructor==String){if(value==undefined){return this.length&&jQuery[type||"attr"](this[0],name)||undefined
}else{options={};
options[name]=value
}}return this.each(function(i){for(name in options){jQuery.attr(type?this.style:this,name,jQuery.prop(this,options[name],type,i,name))
}})
},css:function(key,value){if((key=="width"||key=="height")&&parseFloat(value)<0){value=undefined
}return this.attr(key,value,"curCSS")
},text:function(text){if(typeof text!="object"&&text!=null){return this.empty().append((this[0]&&this[0].ownerDocument||document).createTextNode(text))
}var ret="";
jQuery.each(text||this,function(){jQuery.each(this.childNodes,function(){if(this.nodeType!=8){ret+=this.nodeType!=1?this.nodeValue:jQuery.fn.text([this])
}})
});
return ret
},wrapAll:function(html){if(this[0]){jQuery(html,this[0].ownerDocument).clone().insertBefore(this[0]).map(function(){var elem=this;
while(elem.firstChild){elem=elem.firstChild
}return elem
}).append(this)
}return this
},wrapInner:function(html){return this.each(function(){jQuery(this).contents().wrapAll(html)
})
},wrap:function(html){return this.each(function(){jQuery(this).wrapAll(html)
})
},append:function(){return this.domManip(arguments,true,false,function(elem){if(this.nodeType==1){this.appendChild(elem)
}})
},prepend:function(){return this.domManip(arguments,true,true,function(elem){if(this.nodeType==1){this.insertBefore(elem,this.firstChild)
}})
},before:function(){return this.domManip(arguments,false,false,function(elem){this.parentNode.insertBefore(elem,this)
})
},after:function(){return this.domManip(arguments,false,true,function(elem){this.parentNode.insertBefore(elem,this.nextSibling)
})
},end:function(){return this.prevObject||jQuery([])
},find:function(selector){var elems=jQuery.map(this,function(elem){return jQuery.find(selector,elem)
});
return this.pushStack(/[^+>] [^+>]/.test(selector)||selector.indexOf("..")>-1?jQuery.unique(elems):elems)
},clone:function(events){var ret=this.map(function(){if(jQuery.browser.msie&&!jQuery.isXMLDoc(this)){var clone=this.cloneNode(true),container=document.createElement("div");
container.appendChild(clone);
return jQuery.clean([container.innerHTML])[0]
}else{return this.cloneNode(true)
}});
var clone=ret.find("*").andSelf().each(function(){if(this[expando]!=undefined){this[expando]=null
}});
if(events===true){this.find("*").andSelf().each(function(i){if(this.nodeType==3){return 
}var events=jQuery.data(this,"events");
for(var type in events){for(var handler in events[type]){jQuery.event.add(clone[i],type,events[type][handler],events[type][handler].data)
}}})
}return ret
},filter:function(selector){return this.pushStack(jQuery.isFunction(selector)&&jQuery.grep(this,function(elem,i){return selector.call(elem,i)
})||jQuery.multiFilter(selector,this))
},not:function(selector){if(selector.constructor==String){if(isSimple.test(selector)){return this.pushStack(jQuery.multiFilter(selector,this,true))
}else{selector=jQuery.multiFilter(selector,this)
}}var isArrayLike=selector.length&&selector[selector.length-1]!==undefined&&!selector.nodeType;
return this.filter(function(){return isArrayLike?jQuery.inArray(this,selector)<0:this!=selector
})
},add:function(selector){return !selector?this:this.pushStack(jQuery.merge(this.get(),selector.constructor==String?jQuery(selector).get():selector.length!=undefined&&(!selector.nodeName||jQuery.nodeName(selector,"form"))?selector:[selector]))
},is:function(selector){return selector?jQuery.multiFilter(selector,this).length>0:false
},hasClass:function(selector){return this.is("."+selector)
},val:function(value){if(value==undefined){if(this.length){var elem=this[0];
if(jQuery.nodeName(elem,"select")){var index=elem.selectedIndex,values=[],options=elem.options,one=elem.type=="select-one";
if(index<0){return null
}for(var i=one?index:0,max=one?index+1:options.length;
i<max;
i++){var option=options[i];
if(option.selected){value=jQuery.browser.msie&&!option.attributes.value.specified?option.text:option.value;
if(one){return value
}values.push(value)
}}return values
}else{return(this[0].value||"").replace(/\r/g,"")
}}return undefined
}return this.each(function(){if(this.nodeType!=1){return 
}if(value.constructor==Array&&/radio|checkbox/.test(this.type)){this.checked=(jQuery.inArray(this.value,value)>=0||jQuery.inArray(this.name,value)>=0)
}else{if(jQuery.nodeName(this,"select")){var values=value.constructor==Array?value:[value];
jQuery("option",this).each(function(){this.selected=(jQuery.inArray(this.value,values)>=0||jQuery.inArray(this.text,values)>=0)
});
if(!values.length){this.selectedIndex=-1
}}else{this.value=value
}}})
},html:function(value){return value==undefined?(this.length?this[0].innerHTML:null):this.empty().append(value)
},replaceWith:function(value){return this.after(value).remove()
},eq:function(i){return this.slice(i,i+1)
},slice:function(){return this.pushStack(Array.prototype.slice.apply(this,arguments))
},map:function(callback){return this.pushStack(jQuery.map(this,function(elem,i){return callback.call(elem,i,elem)
}))
},andSelf:function(){return this.add(this.prevObject)
},data:function(key,value){var parts=key.split(".");
parts[1]=parts[1]?"."+parts[1]:"";
if(value==null){var data=this.triggerHandler("getData"+parts[1]+"!",[parts[0]]);
if(data==undefined&&this.length){data=jQuery.data(this[0],key)
}return data==null&&parts[1]?this.data(parts[0]):data
}else{return this.trigger("setData"+parts[1]+"!",[parts[0],value]).each(function(){jQuery.data(this,key,value)
})
}},removeData:function(key){return this.each(function(){jQuery.removeData(this,key)
})
},domManip:function(args,table,reverse,callback){var clone=this.length>1,elems;
return this.each(function(){if(!elems){elems=jQuery.clean(args,this.ownerDocument);
if(reverse){elems.reverse()
}}var obj=this;
if(table&&jQuery.nodeName(this,"table")&&jQuery.nodeName(elems[0],"tr")){obj=this.getElementsByTagName("tbody")[0]||this.appendChild(this.ownerDocument.createElement("tbody"))
}var scripts=jQuery([]);
jQuery.each(elems,function(){var elem=clone?jQuery(this).clone(true)[0]:this;
if(jQuery.nodeName(elem,"script")){scripts=scripts.add(elem)
}else{if(elem.nodeType==1){scripts=scripts.add(jQuery("script",elem).remove())
}callback.call(obj,elem)
}});
scripts.each(evalScript)
})
}};
jQuery.prototype.init.prototype=jQuery.prototype;
function evalScript(i,elem){if(elem.src){jQuery.ajax({url:elem.src,async:false,dataType:"script"})
}else{jQuery.globalEval(elem.text||elem.textContent||elem.innerHTML||"")
}if(elem.parentNode){elem.parentNode.removeChild(elem)
}}jQuery.extend=jQuery.fn.extend=function(){var target=arguments[0]||{},i=1,length=arguments.length,deep=false,options;
if(target.constructor==Boolean){deep=target;
target=arguments[1]||{};
i=2
}if(typeof target!="object"&&typeof target!="function"){target={}
}if(length==1){target=this;
i=0
}for(;
i<length;
i++){if((options=arguments[i])!=null){for(var name in options){if(target===options[name]){continue
}if(deep&&options[name]&&typeof options[name]=="object"&&target[name]&&!options[name].nodeType){target[name]=jQuery.extend(deep,target[name],options[name])
}else{if(options[name]!=undefined){target[name]=options[name]
}}}}}return target
};
var expando="jQuery"+(new Date()).getTime(),uuid=0,windowData={};
var exclude=/z-?index|font-?weight|opacity|zoom|line-?height/i;
jQuery.extend({noConflict:function(deep){window.$=_$;
if(deep){window.jQuery=_jQuery
}return jQuery
},isFunction:function(fn){return !!fn&&typeof fn!="string"&&!fn.nodeName&&fn.constructor!=Array&&/function/i.test(fn+"")
},isXMLDoc:function(elem){return elem.documentElement&&!elem.body||elem.tagName&&elem.ownerDocument&&!elem.ownerDocument.body
},globalEval:function(data){data=jQuery.trim(data);
if(data){var head=document.getElementsByTagName("head")[0]||document.documentElement,script=document.createElement("script");
script.type="text/javascript";
if(jQuery.browser.msie){script.text=data
}else{script.appendChild(document.createTextNode(data))
}head.appendChild(script);
head.removeChild(script)
}},nodeName:function(elem,name){return elem.nodeName&&elem.nodeName.toUpperCase()==name.toUpperCase()
},cache:{},data:function(elem,name,data){elem=elem==window?windowData:elem;
var id=elem[expando];
if(!id){id=elem[expando]=++uuid
}if(name&&!jQuery.cache[id]){jQuery.cache[id]={}
}if(data!=undefined){jQuery.cache[id][name]=data
}return name?jQuery.cache[id][name]:id
},removeData:function(elem,name){elem=elem==window?windowData:elem;
var id=elem[expando];
if(name){if(jQuery.cache[id]){delete jQuery.cache[id][name];
name="";
for(name in jQuery.cache[id]){break
}if(!name){jQuery.removeData(elem)
}}}else{try{delete elem[expando]
}catch(e){if(elem.removeAttribute){elem.removeAttribute(expando)
}}delete jQuery.cache[id]
}},each:function(object,callback,args){if(args){if(object.length==undefined){for(var name in object){if(callback.apply(object[name],args)===false){break
}}}else{for(var i=0,length=object.length;
i<length;
i++){if(callback.apply(object[i],args)===false){break
}}}}else{if(object.length==undefined){for(var name in object){if(callback.call(object[name],name,object[name])===false){break
}}}else{for(var i=0,length=object.length,value=object[0];
i<length&&callback.call(value,i,value)!==false;
value=object[++i]){}}}return object
},prop:function(elem,value,type,i,name){if(jQuery.isFunction(value)){value=value.call(elem,i)
}return value&&value.constructor==Number&&type=="curCSS"&&!exclude.test(name)?value+"px":value
},className:{add:function(elem,classNames){jQuery.each((classNames||"").split(/\s+/),function(i,className){if(elem.nodeType==1&&!jQuery.className.has(elem.className,className)){elem.className+=(elem.className?" ":"")+className
}})
},remove:function(elem,classNames){if(elem.nodeType==1){elem.className=classNames!=undefined?jQuery.grep(elem.className.split(/\s+/),function(className){return !jQuery.className.has(classNames,className)
}).join(" "):""
}},has:function(elem,className){return jQuery.inArray(className,(elem.className||elem).toString().split(/\s+/))>-1
}},swap:function(elem,options,callback){var old={};
for(var name in options){old[name]=elem.style[name];
elem.style[name]=options[name]
}callback.call(elem);
for(var name in options){elem.style[name]=old[name]
}},css:function(elem,name,force){if(name=="width"||name=="height"){var val,props={position:"absolute",visibility:"hidden",display:"block"},which=name=="width"?["Left","Right"]:["Top","Bottom"];
function getWH(){val=name=="width"?elem.offsetWidth:elem.offsetHeight;
var padding=0,border=0;
jQuery.each(which,function(){padding+=parseFloat(jQuery.curCSS(elem,"padding"+this,true))||0;
border+=parseFloat(jQuery.curCSS(elem,"border"+this+"Width",true))||0
});
val-=Math.round(padding+border)
}if(jQuery(elem).is(":visible")){getWH()
}else{jQuery.swap(elem,props,getWH)
}return Math.max(0,val)
}return jQuery.curCSS(elem,name,force)
},curCSS:function(elem,name,force){var ret;
function color(elem){if(!jQuery.browser.safari){return false
}var ret=document.defaultView.getComputedStyle(elem,null);
return !ret||ret.getPropertyValue("color")==""
}if(name=="opacity"&&jQuery.browser.msie){ret=jQuery.attr(elem.style,"opacity");
return ret==""?"1":ret
}if(jQuery.browser.opera&&name=="display"){var save=elem.style.outline;
elem.style.outline="0 solid black";
elem.style.outline=save
}if(name.match(/float/i)){name=styleFloat
}if(!force&&elem.style&&elem.style[name]){ret=elem.style[name]
}else{if(document.defaultView&&document.defaultView.getComputedStyle){if(name.match(/float/i)){name="float"
}name=name.replace(/([A-Z])/g,"-$1").toLowerCase();
var getComputedStyle=document.defaultView.getComputedStyle(elem,null);
if(getComputedStyle&&!color(elem)){ret=getComputedStyle.getPropertyValue(name)
}else{var swap=[],stack=[];
for(var a=elem;
a&&color(a);
a=a.parentNode){stack.unshift(a)
}for(var i=0;
i<stack.length;
i++){if(color(stack[i])){swap[i]=stack[i].style.display;
stack[i].style.display="block"
}}ret=name=="display"&&swap[stack.length-1]!=null?"none":(getComputedStyle&&getComputedStyle.getPropertyValue(name))||"";
for(var i=0;
i<swap.length;
i++){if(swap[i]!=null){stack[i].style.display=swap[i]
}}}if(name=="opacity"&&ret==""){ret="1"
}}else{if(elem.currentStyle){var camelCase=name.replace(/\-(\w)/g,function(all,letter){return letter.toUpperCase()
});
ret=elem.currentStyle[name]||elem.currentStyle[camelCase];
if(!/^\d+(px)?$/i.test(ret)&&/^\d/.test(ret)){var style=elem.style.left,runtimeStyle=elem.runtimeStyle.left;
elem.runtimeStyle.left=elem.currentStyle.left;
elem.style.left=ret||0;
ret=elem.style.pixelLeft+"px";
elem.style.left=style;
elem.runtimeStyle.left=runtimeStyle
}}}}return ret
},clean:function(elems,context){var ret=[];
context=context||document;
if(typeof context.createElement=="undefined"){context=context.ownerDocument||context[0]&&context[0].ownerDocument||document
}jQuery.each(elems,function(i,elem){if(!elem){return 
}if(elem.constructor==Number){elem=elem.toString()
}if(typeof elem=="string"){elem=elem.replace(/(<(\w+)[^>]*?)\/>/g,function(all,front,tag){return tag.match(/^(abbr|br|col|img|input|link|meta|param|hr|area|embed)$/i)?all:front+"></"+tag+">"
});
var tags=jQuery.trim(elem).toLowerCase(),div=context.createElement("div");
var wrap=!tags.indexOf("<opt")&&[1,"<select multiple='multiple'>","</select>"]||!tags.indexOf("<leg")&&[1,"<fieldset>","</fieldset>"]||tags.match(/^<(thead|tbody|tfoot|colg|cap)/)&&[1,"<table>","</table>"]||!tags.indexOf("<tr")&&[2,"<table><tbody>","</tbody></table>"]||(!tags.indexOf("<td")||!tags.indexOf("<th"))&&[3,"<table><tbody><tr>","</tr></tbody></table>"]||!tags.indexOf("<col")&&[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"]||jQuery.browser.msie&&[1,"div<div>","</div>"]||[0,"",""];
div.innerHTML=wrap[1]+elem+wrap[2];
while(wrap[0]--){div=div.lastChild
}if(jQuery.browser.msie){var tbody=!tags.indexOf("<table")&&tags.indexOf("<tbody")<0?div.firstChild&&div.firstChild.childNodes:wrap[1]=="<table>"&&tags.indexOf("<tbody")<0?div.childNodes:[];
for(var j=tbody.length-1;
j>=0;
--j){if(jQuery.nodeName(tbody[j],"tbody")&&!tbody[j].childNodes.length){tbody[j].parentNode.removeChild(tbody[j])
}}if(/^\s/.test(elem)){div.insertBefore(context.createTextNode(elem.match(/^\s*/)[0]),div.firstChild)
}}elem=jQuery.makeArray(div.childNodes)
}if(elem.length===0&&(!jQuery.nodeName(elem,"form")&&!jQuery.nodeName(elem,"select"))){return 
}if(elem[0]==undefined||jQuery.nodeName(elem,"form")||elem.options){ret.push(elem)
}else{ret=jQuery.merge(ret,elem)
}});
return ret
},attr:function(elem,name,value){if(!elem||elem.nodeType==3||elem.nodeType==8){return undefined
}var fix=jQuery.isXMLDoc(elem)?{}:jQuery.props;
if(name=="selected"&&jQuery.browser.safari){elem.parentNode.selectedIndex
}if(fix[name]){if(value!=undefined){elem[fix[name]]=value
}return elem[fix[name]]
}else{if(jQuery.browser.msie&&name=="style"){return jQuery.attr(elem.style,"cssText",value)
}else{if(value==undefined&&jQuery.browser.msie&&jQuery.nodeName(elem,"form")&&(name=="action"||name=="method")){return elem.getAttributeNode(name).nodeValue
}else{if(elem.tagName){if(value!=undefined){if(name=="type"&&jQuery.nodeName(elem,"input")&&elem.parentNode){throw"type property can't be changed"
}elem.setAttribute(name,""+value)
}if(jQuery.browser.msie&&/href|src/.test(name)&&!jQuery.isXMLDoc(elem)){return elem.getAttribute(name,2)
}return elem.getAttribute(name)
}else{if(name=="opacity"&&jQuery.browser.msie){if(value!=undefined){elem.zoom=1;
elem.filter=(elem.filter||"").replace(/alpha\([^)]*\)/,"")+(parseFloat(value).toString()=="NaN"?"":"alpha(opacity="+value*100+")")
}return elem.filter&&elem.filter.indexOf("opacity=")>=0?(parseFloat(elem.filter.match(/opacity=([^)]*)/)[1])/100).toString():""
}name=name.replace(/-([a-z])/ig,function(all,letter){return letter.toUpperCase()
});
if(value!=undefined){elem[name]=value
}return elem[name]
}}}}},trim:function(text){return(text||"").replace(/^\s+|\s+$/g,"")
},makeArray:function(array){var ret=[];
if(array.constructor!=Array){for(var i=0,length=array.length;
i<length;
i++){ret.push(array[i])
}}else{ret=array.slice(0)
}return ret
},inArray:function(elem,array){for(var i=0,length=array.length;
i<length;
i++){if(array[i]==elem){return i
}}return -1
},merge:function(first,second){if(jQuery.browser.msie){for(var i=0;
second[i];
i++){if(second[i].nodeType!=8){first.push(second[i])
}}}else{for(var i=0;
second[i];
i++){first.push(second[i])
}}return first
},unique:function(array){var ret=[],done={};
try{for(var i=0,length=array.length;
i<length;
i++){var id=jQuery.data(array[i]);
if(!done[id]){done[id]=true;
ret.push(array[i])
}}}catch(e){ret=array
}return ret
},grep:function(elems,callback,inv){var ret=[];
for(var i=0,length=elems.length;
i<length;
i++){if(!inv&&callback(elems[i],i)||inv&&!callback(elems[i],i)){ret.push(elems[i])
}}return ret
},map:function(elems,callback){var ret=[];
for(var i=0,length=elems.length;
i<length;
i++){var value=callback(elems[i],i);
if(value!==null&&value!=undefined){if(value.constructor!=Array){value=[value]
}ret=ret.concat(value)
}}return ret
}});
var userAgent=navigator.userAgent.toLowerCase();
jQuery.browser={version:(userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/)||[])[1],safari:/webkit/.test(userAgent),opera:/opera/.test(userAgent),msie:/msie/.test(userAgent)&&!/opera/.test(userAgent),mozilla:/mozilla/.test(userAgent)&&!/(compatible|webkit)/.test(userAgent)};
var styleFloat=jQuery.browser.msie?"styleFloat":"cssFloat";
jQuery.extend({boxModel:!jQuery.browser.msie||document.compatMode=="CSS1Compat",props:{"for":"htmlFor","class":"className","float":styleFloat,cssFloat:styleFloat,styleFloat:styleFloat,innerHTML:"innerHTML",className:"className",value:"value",disabled:"disabled",checked:"checked",readonly:"readOnly",selected:"selected",maxlength:"maxLength",selectedIndex:"selectedIndex",defaultValue:"defaultValue",tagName:"tagName",nodeName:"nodeName"}});
jQuery.each({parent:function(elem){return elem.parentNode
},parents:function(elem){return jQuery.dir(elem,"parentNode")
},next:function(elem){return jQuery.nth(elem,2,"nextSibling")
},prev:function(elem){return jQuery.nth(elem,2,"previousSibling")
},nextAll:function(elem){return jQuery.dir(elem,"nextSibling")
},prevAll:function(elem){return jQuery.dir(elem,"previousSibling")
},siblings:function(elem){return jQuery.sibling(elem.parentNode.firstChild,elem)
},children:function(elem){return jQuery.sibling(elem.firstChild)
},contents:function(elem){return jQuery.nodeName(elem,"iframe")?elem.contentDocument||elem.contentWindow.document:jQuery.makeArray(elem.childNodes)
}},function(name,fn){jQuery.fn[name]=function(selector){var ret=jQuery.map(this,fn);
if(selector&&typeof selector=="string"){ret=jQuery.multiFilter(selector,ret)
}return this.pushStack(jQuery.unique(ret))
}
});
jQuery.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(name,original){jQuery.fn[name]=function(){var args=arguments;
return this.each(function(){for(var i=0,length=args.length;
i<length;
i++){jQuery(args[i])[original](this)
}})
}
});
jQuery.each({removeAttr:function(name){jQuery.attr(this,name,"");
if(this.nodeType==1){this.removeAttribute(name)
}},addClass:function(classNames){jQuery.className.add(this,classNames)
},removeClass:function(classNames){jQuery.className.remove(this,classNames)
},toggleClass:function(classNames){jQuery.className[jQuery.className.has(this,classNames)?"remove":"add"](this,classNames)
},remove:function(selector){if(!selector||jQuery.filter(selector,[this]).r.length){jQuery("*",this).add(this).each(function(){jQuery.event.remove(this);
jQuery.removeData(this)
});
if(this.parentNode){this.parentNode.removeChild(this)
}}},empty:function(){jQuery(">*",this).remove();
while(this.firstChild){this.removeChild(this.firstChild)
}}},function(name,fn){jQuery.fn[name]=function(){return this.each(fn,arguments)
}
});
jQuery.each(["Height","Width"],function(i,name){var type=name.toLowerCase();
jQuery.fn[type]=function(size){return this[0]==window?jQuery.browser.opera&&document.body["client"+name]||jQuery.browser.safari&&window["inner"+name]||document.compatMode=="CSS1Compat"&&document.documentElement["client"+name]||document.body["client"+name]:this[0]==document?Math.max(Math.max(document.body["scroll"+name],document.documentElement["scroll"+name]),Math.max(document.body["offset"+name],document.documentElement["offset"+name])):size==undefined?(this.length?jQuery.css(this[0],type):null):this.css(type,size.constructor==String?size:size+"px")
}
});
var chars=jQuery.browser.safari&&parseInt(jQuery.browser.version)<417?"(?:[\\w*_-]|\\\\.)":"(?:[\\w\u0128-\uFFFF*_-]|\\\\.)",quickChild=new RegExp("^>\\s*("+chars+"+)"),quickID=new RegExp("^("+chars+"+)(#)("+chars+"+)"),quickClass=new RegExp("^([#.]?)("+chars+"*)");
jQuery.extend({expr:{"":function(a,i,m){return m[2]=="*"||jQuery.nodeName(a,m[2])
},"#":function(a,i,m){return a.getAttribute("id")==m[2]
},":":{lt:function(a,i,m){return i<m[3]-0
},gt:function(a,i,m){return i>m[3]-0
},nth:function(a,i,m){return m[3]-0==i
},eq:function(a,i,m){return m[3]-0==i
},first:function(a,i){return i==0
},last:function(a,i,m,r){return i==r.length-1
},even:function(a,i){return i%2==0
},odd:function(a,i){return i%2
},"first-child":function(a){return a.parentNode.getElementsByTagName("*")[0]==a
},"last-child":function(a){return jQuery.nth(a.parentNode.lastChild,1,"previousSibling")==a
},"only-child":function(a){return !jQuery.nth(a.parentNode.lastChild,2,"previousSibling")
},parent:function(a){return a.firstChild
},empty:function(a){return !a.firstChild
},contains:function(a,i,m){return(a.textContent||a.innerText||jQuery(a).text()||"").indexOf(m[3])>=0
},visible:function(a){return"hidden"!=a.type&&jQuery.css(a,"display")!="none"&&jQuery.css(a,"visibility")!="hidden"
},hidden:function(a){return"hidden"==a.type||jQuery.css(a,"display")=="none"||jQuery.css(a,"visibility")=="hidden"
},enabled:function(a){return !a.disabled
},disabled:function(a){return a.disabled
},checked:function(a){return a.checked
},selected:function(a){return a.selected||jQuery.attr(a,"selected")
},text:function(a){return"text"==a.type
},radio:function(a){return"radio"==a.type
},checkbox:function(a){return"checkbox"==a.type
},file:function(a){return"file"==a.type
},password:function(a){return"password"==a.type
},submit:function(a){return"submit"==a.type
},image:function(a){return"image"==a.type
},reset:function(a){return"reset"==a.type
},button:function(a){return"button"==a.type||jQuery.nodeName(a,"button")
},input:function(a){return/input|select|textarea|button/i.test(a.nodeName)
},has:function(a,i,m){return jQuery.find(m[3],a).length
},header:function(a){return/h\d/i.test(a.nodeName)
},animated:function(a){return jQuery.grep(jQuery.timers,function(fn){return a==fn.elem
}).length
}}},parse:[/^(\[) *@?([\w-]+) *([!*$^~=]*) *('?"?)(.*?)\4 *\]/,/^(:)([\w-]+)\("?'?(.*?(\(.*?\))?[^(]*?)"?'?\)/,new RegExp("^([:.#]*)("+chars+"+)")],multiFilter:function(expr,elems,not){var old,cur=[];
while(expr&&expr!=old){old=expr;
var f=jQuery.filter(expr,elems,not);
expr=f.t.replace(/^\s*,\s*/,"");
cur=not?elems=f.r:jQuery.merge(cur,f.r)
}return cur
},find:function(t,context){if(typeof t!="string"){return[t]
}if(context&&context.nodeType!=1&&context.nodeType!=9){return[]
}context=context||document;
var ret=[context],done=[],last,nodeName;
while(t&&last!=t){var r=[];
last=t;
t=jQuery.trim(t);
var foundToken=false;
var re=quickChild;
var m=re.exec(t);
if(m){nodeName=m[1].toUpperCase();
for(var i=0;
ret[i];
i++){for(var c=ret[i].firstChild;
c;
c=c.nextSibling){if(c.nodeType==1&&(nodeName=="*"||c.nodeName.toUpperCase()==nodeName)){r.push(c)
}}}ret=r;
t=t.replace(re,"");
if(t.indexOf(" ")==0){continue
}foundToken=true
}else{re=/^([>+~])\s*(\w*)/i;
if((m=re.exec(t))!=null){r=[];
var merge={};
nodeName=m[2].toUpperCase();
m=m[1];
for(var j=0,rl=ret.length;
j<rl;
j++){var n=m=="~"||m=="+"?ret[j].nextSibling:ret[j].firstChild;
for(;
n;
n=n.nextSibling){if(n.nodeType==1){var id=jQuery.data(n);
if(m=="~"&&merge[id]){break
}if(!nodeName||n.nodeName.toUpperCase()==nodeName){if(m=="~"){merge[id]=true
}r.push(n)
}if(m=="+"){break
}}}}ret=r;
t=jQuery.trim(t.replace(re,""));
foundToken=true
}}if(t&&!foundToken){if(!t.indexOf(",")){if(context==ret[0]){ret.shift()
}done=jQuery.merge(done,ret);
r=ret=[context];
t=" "+t.substr(1,t.length)
}else{var re2=quickID;
var m=re2.exec(t);
if(m){m=[0,m[2],m[3],m[1]]
}else{re2=quickClass;
m=re2.exec(t)
}m[2]=m[2].replace(/\\/g,"");
var elem=ret[ret.length-1];
if(m[1]=="#"&&elem&&elem.getElementById&&!jQuery.isXMLDoc(elem)){var oid=elem.getElementById(m[2]);
if((jQuery.browser.msie||jQuery.browser.opera)&&oid&&typeof oid.id=="string"&&oid.id!=m[2]){oid=jQuery('[@id="'+m[2]+'"]',elem)[0]
}ret=r=oid&&(!m[3]||jQuery.nodeName(oid,m[3]))?[oid]:[]
}else{for(var i=0;
ret[i];
i++){var tag=m[1]=="#"&&m[3]?m[3]:m[1]!=""||m[0]==""?"*":m[2];
if(tag=="*"&&ret[i].nodeName.toLowerCase()=="object"){tag="param"
}r=jQuery.merge(r,ret[i].getElementsByTagName(tag))
}if(m[1]=="."){r=jQuery.classFilter(r,m[2])
}if(m[1]=="#"){var tmp=[];
for(var i=0;
r[i];
i++){if(r[i].getAttribute("id")==m[2]){tmp=[r[i]];
break
}}r=tmp
}ret=r
}t=t.replace(re2,"")
}}if(t){var val=jQuery.filter(t,r);
ret=r=val.r;
t=jQuery.trim(val.t)
}}if(t){ret=[]
}if(ret&&context==ret[0]){ret.shift()
}done=jQuery.merge(done,ret);
return done
},classFilter:function(r,m,not){m=" "+m+" ";
var tmp=[];
for(var i=0;
r[i];
i++){var pass=(" "+r[i].className+" ").indexOf(m)>=0;
if(!not&&pass||not&&!pass){tmp.push(r[i])
}}return tmp
},filter:function(t,r,not){var last;
while(t&&t!=last){last=t;
var p=jQuery.parse,m;
for(var i=0;
p[i];
i++){m=p[i].exec(t);
if(m){t=t.substring(m[0].length);
m[2]=m[2].replace(/\\/g,"");
break
}}if(!m){break
}if(m[1]==":"&&m[2]=="not"){r=isSimple.test(m[3])?jQuery.filter(m[3],r,true).r:jQuery(r).not(m[3])
}else{if(m[1]=="."){r=jQuery.classFilter(r,m[2],not)
}else{if(m[1]=="["){var tmp=[],type=m[3];
for(var i=0,rl=r.length;
i<rl;
i++){var a=r[i],z=a[jQuery.props[m[2]]||m[2]];
if(z==null||/href|src|selected/.test(m[2])){z=jQuery.attr(a,m[2])||""
}if((type==""&&!!z||type=="="&&z==m[5]||type=="!="&&z!=m[5]||type=="^="&&z&&!z.indexOf(m[5])||type=="$="&&z.substr(z.length-m[5].length)==m[5]||(type=="*="||type=="~=")&&z.indexOf(m[5])>=0)^not){tmp.push(a)
}}r=tmp
}else{if(m[1]==":"&&m[2]=="nth-child"){var merge={},tmp=[],test=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(m[3]=="even"&&"2n"||m[3]=="odd"&&"2n+1"||!/\D/.test(m[3])&&"0n+"+m[3]||m[3]),first=(test[1]+(test[2]||1))-0,last=test[3]-0;
for(var i=0,rl=r.length;
i<rl;
i++){var node=r[i],parentNode=node.parentNode,id=jQuery.data(parentNode);
if(!merge[id]){var c=1;
for(var n=parentNode.firstChild;
n;
n=n.nextSibling){if(n.nodeType==1){n.nodeIndex=c++
}}merge[id]=true
}var add=false;
if(first==0){if(node.nodeIndex==last){add=true
}}else{if((node.nodeIndex-last)%first==0&&(node.nodeIndex-last)/first>=0){add=true
}}if(add^not){tmp.push(node)
}}r=tmp
}else{var fn=jQuery.expr[m[1]];
if(typeof fn=="object"){fn=fn[m[2]]
}if(typeof fn=="string"){fn=eval("false||function(a,i){return "+fn+";}")
}r=jQuery.grep(r,function(elem,i){return fn(elem,i,m,r)
},not)
}}}}}return{r:r,t:t}
},dir:function(elem,dir){var matched=[];
var cur=elem[dir];
while(cur&&cur!=document){if(cur.nodeType==1){matched.push(cur)
}cur=cur[dir]
}return matched
},nth:function(cur,result,dir,elem){result=result||1;
var num=0;
for(;
cur;
cur=cur[dir]){if(cur.nodeType==1&&++num==result){break
}}return cur
},sibling:function(n,elem){var r=[];
for(;
n;
n=n.nextSibling){if(n.nodeType==1&&(!elem||n!=elem)){r.push(n)
}}return r
}});
jQuery.event={add:function(elem,types,handler,data){if(elem.nodeType==3||elem.nodeType==8){return 
}if(jQuery.browser.msie&&elem.setInterval!=undefined){elem=window
}if(!handler.guid){handler.guid=this.guid++
}if(data!=undefined){var fn=handler;
handler=function(){return fn.apply(this,arguments)
};
handler.data=data;
handler.guid=fn.guid
}var events=jQuery.data(elem,"events")||jQuery.data(elem,"events",{}),handle=jQuery.data(elem,"handle")||jQuery.data(elem,"handle",function(){var val;
if(typeof jQuery=="undefined"||jQuery.event.triggered){return val
}val=jQuery.event.handle.apply(arguments.callee.elem,arguments);
return val
});
handle.elem=elem;
jQuery.each(types.split(/\s+/),function(index,type){var parts=type.split(".");
type=parts[0];
handler.type=parts[1];
var handlers=events[type];
if(!handlers){handlers=events[type]={};
if(!jQuery.event.special[type]||jQuery.event.special[type].setup.call(elem)===false){if(elem.addEventListener){elem.addEventListener(type,handle,false)
}else{if(elem.attachEvent){elem.attachEvent("on"+type,handle)
}}}}handlers[handler.guid]=handler;
jQuery.event.global[type]=true
});
elem=null
},guid:1,global:{},remove:function(elem,types,handler){if(elem.nodeType==3||elem.nodeType==8){return 
}var events=jQuery.data(elem,"events"),ret,index;
if(events){if(types==undefined||(typeof types=="string"&&types.charAt(0)==".")){for(var type in events){this.remove(elem,type+(types||""))
}}else{if(types.type){handler=types.handler;
types=types.type
}jQuery.each(types.split(/\s+/),function(index,type){var parts=type.split(".");
type=parts[0];
if(events[type]){if(handler){delete events[type][handler.guid]
}else{for(handler in events[type]){if(!parts[1]||events[type][handler].type==parts[1]){delete events[type][handler]
}}}for(ret in events[type]){break
}if(!ret){if(!jQuery.event.special[type]||jQuery.event.special[type].teardown.call(elem)===false){if(elem.removeEventListener){elem.removeEventListener(type,jQuery.data(elem,"handle"),false)
}else{if(elem.detachEvent){elem.detachEvent("on"+type,jQuery.data(elem,"handle"))
}}}ret=null;
delete events[type]
}}})
}for(ret in events){break
}if(!ret){var handle=jQuery.data(elem,"handle");
if(handle){handle.elem=null
}jQuery.removeData(elem,"events");
jQuery.removeData(elem,"handle")
}}},trigger:function(type,data,elem,donative,extra){data=jQuery.makeArray(data||[]);
if(type.indexOf("!")>=0){type=type.slice(0,-1);
var exclusive=true
}if(!elem){if(this.global[type]){jQuery("*").add([window,document]).trigger(type,data)
}}else{if(elem.nodeType==3||elem.nodeType==8){return undefined
}var val,ret,fn=jQuery.isFunction(elem[type]||null),event=!data[0]||!data[0].preventDefault;
if(event){data.unshift(this.fix({type:type,target:elem}))
}data[0].type=type;
if(exclusive){data[0].exclusive=true
}if(jQuery.isFunction(jQuery.data(elem,"handle"))){val=jQuery.data(elem,"handle").apply(elem,data)
}if(!fn&&elem["on"+type]&&elem["on"+type].apply(elem,data)===false){val=false
}if(event){data.shift()
}if(extra&&jQuery.isFunction(extra)){ret=extra.apply(elem,val==null?data:data.concat(val));
if(ret!==undefined){val=ret
}}if(fn&&donative!==false&&val!==false&&!(jQuery.nodeName(elem,"a")&&type=="click")){this.triggered=true;
try{elem[type]()
}catch(e){}}this.triggered=false
}return val
},handle:function(event){var val;
event=jQuery.event.fix(event||window.event||{});
var parts=event.type.split(".");
event.type=parts[0];
var handlers=jQuery.data(this,"events")&&jQuery.data(this,"events")[event.type],args=Array.prototype.slice.call(arguments,1);
args.unshift(event);
for(var j in handlers){var handler=handlers[j];
args[0].handler=handler;
args[0].data=handler.data;
if(!parts[1]&&!event.exclusive||handler.type==parts[1]){var ret=handler.apply(this,args);
if(val!==false){val=ret
}if(ret===false){event.preventDefault();
event.stopPropagation()
}}}if(jQuery.browser.msie){event.target=event.preventDefault=event.stopPropagation=event.handler=event.data=null
}return val
},fix:function(event){var originalEvent=event;
event=jQuery.extend({},originalEvent);
event.preventDefault=function(){if(originalEvent.preventDefault){originalEvent.preventDefault()
}originalEvent.returnValue=false
};
event.stopPropagation=function(){if(originalEvent.stopPropagation){originalEvent.stopPropagation()
}originalEvent.cancelBubble=true
};
if(!event.target){event.target=event.srcElement||document
}if(event.target.nodeType==3){event.target=originalEvent.target.parentNode
}if(!event.relatedTarget&&event.fromElement){event.relatedTarget=event.fromElement==event.target?event.toElement:event.fromElement
}if(event.pageX==null&&event.clientX!=null){var doc=document.documentElement,body=document.body;
event.pageX=event.clientX+(doc&&doc.scrollLeft||body&&body.scrollLeft||0)-(doc.clientLeft||0);
event.pageY=event.clientY+(doc&&doc.scrollTop||body&&body.scrollTop||0)-(doc.clientTop||0)
}if(!event.which&&((event.charCode||event.charCode===0)?event.charCode:event.keyCode)){event.which=event.charCode||event.keyCode
}if(!event.metaKey&&event.ctrlKey){event.metaKey=event.ctrlKey
}if(!event.which&&event.button){event.which=(event.button&1?1:(event.button&2?3:(event.button&4?2:0)))
}return event
},special:{ready:{setup:function(){bindReady();
return 
},teardown:function(){return 
}},mouseenter:{setup:function(){if(jQuery.browser.msie){return false
}jQuery(this).bind("mouseover",jQuery.event.special.mouseenter.handler);
return true
},teardown:function(){if(jQuery.browser.msie){return false
}jQuery(this).unbind("mouseover",jQuery.event.special.mouseenter.handler);
return true
},handler:function(event){if(withinElement(event,this)){return true
}arguments[0].type="mouseenter";
return jQuery.event.handle.apply(this,arguments)
}},mouseleave:{setup:function(){if(jQuery.browser.msie){return false
}jQuery(this).bind("mouseout",jQuery.event.special.mouseleave.handler);
return true
},teardown:function(){if(jQuery.browser.msie){return false
}jQuery(this).unbind("mouseout",jQuery.event.special.mouseleave.handler);
return true
},handler:function(event){if(withinElement(event,this)){return true
}arguments[0].type="mouseleave";
return jQuery.event.handle.apply(this,arguments)
}}}};
jQuery.fn.extend({bind:function(type,data,fn){return type=="unload"?this.one(type,data,fn):this.each(function(){jQuery.event.add(this,type,fn||data,fn&&data)
})
},one:function(type,data,fn){return this.each(function(){jQuery.event.add(this,type,function(event){jQuery(this).unbind(event);
return(fn||data).apply(this,arguments)
},fn&&data)
})
},unbind:function(type,fn){return this.each(function(){jQuery.event.remove(this,type,fn)
})
},trigger:function(type,data,fn){return this.each(function(){jQuery.event.trigger(type,data,this,true,fn)
})
},triggerHandler:function(type,data,fn){if(this[0]){return jQuery.event.trigger(type,data,this[0],false,fn)
}return undefined
},toggle:function(){var args=arguments;
return this.click(function(event){this.lastToggle=0==this.lastToggle?1:0;
event.preventDefault();
return args[this.lastToggle].apply(this,arguments)||false
})
},hover:function(fnOver,fnOut){return this.bind("mouseenter",fnOver).bind("mouseleave",fnOut)
},ready:function(fn){bindReady();
if(jQuery.isReady){fn.call(document,jQuery)
}else{jQuery.readyList.push(function(){return fn.call(this,jQuery)
})
}return this
}});
jQuery.extend({isReady:false,readyList:[],ready:function(){if(!jQuery.isReady){jQuery.isReady=true;
if(jQuery.readyList){jQuery.each(jQuery.readyList,function(){this.apply(document)
});
jQuery.readyList=null
}jQuery(document).triggerHandler("ready")
}}});
var readyBound=false;
function bindReady(){if(readyBound){return 
}readyBound=true;
if(document.addEventListener&&!jQuery.browser.opera){document.addEventListener("DOMContentLoaded",jQuery.ready,false)
}if(jQuery.browser.msie&&window==top){(function(){if(jQuery.isReady){return 
}try{document.documentElement.doScroll("left")
}catch(error){setTimeout(arguments.callee,0);
return 
}jQuery.ready()
})()
}if(jQuery.browser.opera){document.addEventListener("DOMContentLoaded",function(){if(jQuery.isReady){return 
}for(var i=0;
i<document.styleSheets.length;
i++){if(document.styleSheets[i].disabled){setTimeout(arguments.callee,0);
return 
}}jQuery.ready()
},false)
}if(jQuery.browser.safari){var numStyles;
(function(){if(jQuery.isReady){return 
}if(document.readyState!="loaded"&&document.readyState!="complete"){setTimeout(arguments.callee,0);
return 
}if(numStyles===undefined){numStyles=jQuery("style, link[rel=stylesheet]").length
}if(document.styleSheets.length!=numStyles){setTimeout(arguments.callee,0);
return 
}jQuery.ready()
})()
}jQuery.event.add(window,"load",jQuery.ready)
}jQuery.each(("blur,focus,load,resize,scroll,unload,click,dblclick,mousedown,mouseup,mousemove,mouseover,mouseout,change,select,submit,keydown,keypress,keyup,error").split(","),function(i,name){jQuery.fn[name]=function(fn){return fn?this.bind(name,fn):this.trigger(name)
}
});
var withinElement=function(event,elem){var parent=event.relatedTarget;
while(parent&&parent!=elem){try{parent=parent.parentNode
}catch(error){parent=elem
}}return parent==elem
};
jQuery(window).bind("unload",function(){jQuery("*").add(document).unbind()
});
jQuery.fn.extend({load:function(url,params,callback){if(jQuery.isFunction(url)){return this.bind("load",url)
}var off=url.indexOf(" ");
if(off>=0){var selector=url.slice(off,url.length);
url=url.slice(0,off)
}callback=callback||function(){};
var type="GET";
if(params){if(jQuery.isFunction(params)){callback=params;
params=null
}else{params=jQuery.param(params);
type="POST"
}}var self=this;
jQuery.ajax({url:url,type:type,dataType:"html",data:params,complete:function(res,status){if(status=="success"||status=="notmodified"){self.html(selector?jQuery("<div/>").append(res.responseText.replace(/<script(.|\s)*?\/script>/g,"")).find(selector):res.responseText)
}self.each(callback,[res.responseText,status,res])
}});
return this
},serialize:function(){return jQuery.param(this.serializeArray())
},serializeArray:function(){return this.map(function(){return jQuery.nodeName(this,"form")?jQuery.makeArray(this.elements):this
}).filter(function(){return this.name&&!this.disabled&&(this.checked||/select|textarea/i.test(this.nodeName)||/text|hidden|password/i.test(this.type))
}).map(function(i,elem){var val=jQuery(this).val();
return val==null?null:val.constructor==Array?jQuery.map(val,function(val,i){return{name:elem.name,value:val}
}):{name:elem.name,value:val}
}).get()
}});
jQuery.each("ajaxStart,ajaxStop,ajaxComplete,ajaxError,ajaxSuccess,ajaxSend".split(","),function(i,o){jQuery.fn[o]=function(f){return this.bind(o,f)
}
});
var jsc=(new Date).getTime();
jQuery.extend({get:function(url,data,callback,type){if(jQuery.isFunction(data)){callback=data;
data=null
}return jQuery.ajax({type:"GET",url:url,data:data,success:callback,dataType:type})
},getScript:function(url,callback){return jQuery.get(url,null,callback,"script")
},getJSON:function(url,data,callback){return jQuery.get(url,data,callback,"json")
},post:function(url,data,callback,type){if(jQuery.isFunction(data)){callback=data;
data={}
}return jQuery.ajax({type:"POST",url:url,data:data,success:callback,dataType:type})
},ajaxSetup:function(settings){jQuery.extend(jQuery.ajaxSettings,settings)
},ajaxSettings:{global:true,type:"GET",timeout:0,contentType:"application/x-www-form-urlencoded",processData:true,async:true,data:null,username:null,password:null,accepts:{xml:"application/xml, text/xml",html:"text/html",script:"text/javascript, application/javascript",json:"application/json, text/javascript",text:"text/plain",_default:"*/*"}},lastModified:{},ajax:function(s){var jsonp,jsre=/=\?(&|$)/g,status,data;
s=jQuery.extend(true,s,jQuery.extend(true,{},jQuery.ajaxSettings,s));
if(s.data&&s.processData&&typeof s.data!="string"){s.data=jQuery.param(s.data)
}if(s.dataType=="jsonp"){if(s.type.toLowerCase()=="get"){if(!s.url.match(jsre)){s.url+=(s.url.match(/\?/)?"&":"?")+(s.jsonp||"callback")+"=?"
}}else{if(!s.data||!s.data.match(jsre)){s.data=(s.data?s.data+"&":"")+(s.jsonp||"callback")+"=?"
}}s.dataType="json"
}if(s.dataType=="json"&&(s.data&&s.data.match(jsre)||s.url.match(jsre))){jsonp="jsonp"+jsc++;
if(s.data){s.data=(s.data+"").replace(jsre,"="+jsonp+"$1")
}s.url=s.url.replace(jsre,"="+jsonp+"$1");
s.dataType="script";
window[jsonp]=function(tmp){data=tmp;
success();
complete();
window[jsonp]=undefined;
try{delete window[jsonp]
}catch(e){}if(head){head.removeChild(script)
}}
}if(s.dataType=="script"&&s.cache==null){s.cache=false
}if(s.cache===false&&s.type.toLowerCase()=="get"){var ts=(new Date()).getTime();
var ret=s.url.replace(/(\?|&)_=.*?(&|$)/,"$1_="+ts+"$2");
s.url=ret+((ret==s.url)?(s.url.match(/\?/)?"&":"?")+"_="+ts:"")
}if(s.data&&s.type.toLowerCase()=="get"){s.url+=(s.url.match(/\?/)?"&":"?")+s.data;
s.data=null
}if(s.global&&!jQuery.active++){jQuery.event.trigger("ajaxStart")
}if((!s.url.indexOf("http")||!s.url.indexOf("//"))&&s.dataType=="script"&&s.type.toLowerCase()=="get"){var head=document.getElementsByTagName("head")[0];
var script=document.createElement("script");
script.src=s.url;
if(s.scriptCharset){script.charset=s.scriptCharset
}if(!jsonp){var done=false;
script.onload=script.onreadystatechange=function(){if(!done&&(!this.readyState||this.readyState=="loaded"||this.readyState=="complete")){done=true;
success();
complete();
head.removeChild(script)
}}
}head.appendChild(script);
return undefined
}var requestDone=false;
var xml=window.ActiveXObject?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest();
xml.open(s.type,s.url,s.async,s.username,s.password);
try{if(s.data){xml.setRequestHeader("Content-Type",s.contentType)
}if(s.ifModified){xml.setRequestHeader("If-Modified-Since",jQuery.lastModified[s.url]||"Thu, 01 Jan 1970 00:00:00 GMT")
}xml.setRequestHeader("X-Requested-With","XMLHttpRequest");
xml.setRequestHeader("Accept",s.dataType&&s.accepts[s.dataType]?s.accepts[s.dataType]+", */*":s.accepts._default)
}catch(e){}if(s.beforeSend){s.beforeSend(xml)
}if(s.global){jQuery.event.trigger("ajaxSend",[xml,s])
}var onreadystatechange=function(isTimeout){if(!requestDone&&xml&&(xml.readyState==4||isTimeout=="timeout")){requestDone=true;
if(ival){clearInterval(ival);
ival=null
}status=isTimeout=="timeout"&&"timeout"||!jQuery.httpSuccess(xml)&&"error"||s.ifModified&&jQuery.httpNotModified(xml,s.url)&&"notmodified"||"success";
if(status=="success"){try{data=jQuery.httpData(xml,s.dataType)
}catch(e){status="parsererror"
}}if(status=="success"){var modRes;
try{modRes=xml.getResponseHeader("Last-Modified")
}catch(e){}if(s.ifModified&&modRes){jQuery.lastModified[s.url]=modRes
}if(!jsonp){success()
}}else{jQuery.handleError(s,xml,status)
}complete();
if(s.async){xml=null
}}};
if(s.async){var ival=setInterval(onreadystatechange,13);
if(s.timeout>0){setTimeout(function(){if(xml){xml.abort();
if(!requestDone){onreadystatechange("timeout")
}}},s.timeout)
}}try{xml.send(s.data)
}catch(e){jQuery.handleError(s,xml,null,e)
}if(!s.async){onreadystatechange()
}function success(){if(s.success){s.success(data,status)
}if(s.global){jQuery.event.trigger("ajaxSuccess",[xml,s])
}}function complete(){if(s.complete){s.complete(xml,status)
}if(s.global){jQuery.event.trigger("ajaxComplete",[xml,s])
}if(s.global&&!--jQuery.active){jQuery.event.trigger("ajaxStop")
}}return xml
},handleError:function(s,xml,status,e){if(s.error){s.error(xml,status,e)
}if(s.global){jQuery.event.trigger("ajaxError",[xml,s,e])
}},active:0,httpSuccess:function(r){try{return !r.status&&location.protocol=="file:"||(r.status>=200&&r.status<300)||r.status==304||r.status==1223||jQuery.browser.safari&&r.status==undefined
}catch(e){}return false
},httpNotModified:function(xml,url){try{var xmlRes=xml.getResponseHeader("Last-Modified");
return xml.status==304||xmlRes==jQuery.lastModified[url]||jQuery.browser.safari&&xml.status==undefined
}catch(e){}return false
},httpData:function(r,type){var ct=r.getResponseHeader("content-type");
var xml=type=="xml"||!type&&ct&&ct.indexOf("xml")>=0;
var data=xml?r.responseXML:r.responseText;
if(xml&&data.documentElement.tagName=="parsererror"){throw"parsererror"
}if(type=="script"){jQuery.globalEval(data)
}if(type=="json"){data=eval("("+data+")")
}return data
},param:function(a){var s=[];
if(a.constructor==Array||a.jquery){jQuery.each(a,function(){s.push(encodeURIComponent(this.name)+"="+encodeURIComponent(this.value))
})
}else{for(var j in a){if(a[j]&&a[j].constructor==Array){jQuery.each(a[j],function(){s.push(encodeURIComponent(j)+"="+encodeURIComponent(this))
})
}else{s.push(encodeURIComponent(j)+"="+encodeURIComponent(a[j]))
}}}return s.join("&").replace(/%20/g,"+")
}});
jQuery.fn.extend({show:function(speed,callback){return speed?this.animate({height:"show",width:"show",opacity:"show"},speed,callback):this.filter(":hidden").each(function(){this.style.display=this.oldblock||"";
if(jQuery.css(this,"display")=="none"){var elem=jQuery("<"+this.tagName+" />").appendTo("body");
this.style.display=elem.css("display");
if(this.style.display=="none"){this.style.display="block"
}elem.remove()
}}).end()
},hide:function(speed,callback){return speed?this.animate({height:"hide",width:"hide",opacity:"hide"},speed,callback):this.filter(":visible").each(function(){this.oldblock=this.oldblock||jQuery.css(this,"display");
this.style.display="none"
}).end()
},_toggle:jQuery.fn.toggle,toggle:function(fn,fn2){return jQuery.isFunction(fn)&&jQuery.isFunction(fn2)?this._toggle(fn,fn2):fn?this.animate({height:"toggle",width:"toggle",opacity:"toggle"},fn,fn2):this.each(function(){jQuery(this)[jQuery(this).is(":hidden")?"show":"hide"]()
})
},slideDown:function(speed,callback){return this.animate({height:"show"},speed,callback)
},slideUp:function(speed,callback){return this.animate({height:"hide"},speed,callback)
},slideToggle:function(speed,callback){return this.animate({height:"toggle"},speed,callback)
},fadeIn:function(speed,callback){return this.animate({opacity:"show"},speed,callback)
},fadeOut:function(speed,callback){return this.animate({opacity:"hide"},speed,callback)
},fadeTo:function(speed,to,callback){return this.animate({opacity:to},speed,callback)
},animate:function(prop,speed,easing,callback){var optall=jQuery.speed(speed,easing,callback);
return this[optall.queue===false?"each":"queue"](function(){if(this.nodeType!=1){return false
}var opt=jQuery.extend({},optall);
var hidden=jQuery(this).is(":hidden"),self=this;
for(var p in prop){if(prop[p]=="hide"&&hidden||prop[p]=="show"&&!hidden){return jQuery.isFunction(opt.complete)&&opt.complete.apply(this)
}if(p=="height"||p=="width"){opt.display=jQuery.css(this,"display");
opt.overflow=this.style.overflow
}}if(opt.overflow!=null){this.style.overflow="hidden"
}opt.curAnim=jQuery.extend({},prop);
jQuery.each(prop,function(name,val){var e=new jQuery.fx(self,opt,name);
if(/toggle|show|hide/.test(val)){e[val=="toggle"?hidden?"show":"hide":val](prop)
}else{var parts=val.toString().match(/^([+-]=)?([\d+-.]+)(.*)$/),start=e.cur(true)||0;
if(parts){var end=parseFloat(parts[2]),unit=parts[3]||"px";
if(unit!="px"){self.style[name]=(end||1)+unit;
start=((end||1)/e.cur(true))*start;
self.style[name]=start+unit
}if(parts[1]){end=((parts[1]=="-="?-1:1)*end)+start
}e.custom(start,end,unit)
}else{e.custom(start,val,"")
}}});
return true
})
},queue:function(type,fn){if(jQuery.isFunction(type)||(type&&type.constructor==Array)){fn=type;
type="fx"
}if(!type||(typeof type=="string"&&!fn)){return queue(this[0],type)
}return this.each(function(){if(fn.constructor==Array){queue(this,type,fn)
}else{queue(this,type).push(fn);
if(queue(this,type).length==1){fn.apply(this)
}}})
},stop:function(clearQueue,gotoEnd){var timers=jQuery.timers;
if(clearQueue){this.queue([])
}this.each(function(){for(var i=timers.length-1;
i>=0;
i--){if(timers[i].elem==this){if(gotoEnd){timers[i](true)
}timers.splice(i,1)
}}});
if(!gotoEnd){this.dequeue()
}return this
}});
var queue=function(elem,type,array){if(!elem){return undefined
}type=type||"fx";
var q=jQuery.data(elem,type+"queue");
if(!q||array){q=jQuery.data(elem,type+"queue",array?jQuery.makeArray(array):[])
}return q
};
jQuery.fn.dequeue=function(type){type=type||"fx";
return this.each(function(){var q=queue(this,type);
q.shift();
if(q.length){q[0].apply(this)
}})
};
jQuery.extend({speed:function(speed,easing,fn){var opt=speed&&speed.constructor==Object?speed:{complete:fn||!fn&&easing||jQuery.isFunction(speed)&&speed,duration:speed,easing:fn&&easing||easing&&easing.constructor!=Function&&easing};
opt.duration=(opt.duration&&opt.duration.constructor==Number?opt.duration:{slow:600,fast:200}[opt.duration])||400;
opt.old=opt.complete;
opt.complete=function(){if(opt.queue!==false){jQuery(this).dequeue()
}if(jQuery.isFunction(opt.old)){opt.old.apply(this)
}};
return opt
},easing:{linear:function(p,n,firstNum,diff){return firstNum+diff*p
},swing:function(p,n,firstNum,diff){return((-Math.cos(p*Math.PI)/2)+0.5)*diff+firstNum
}},timers:[],timerId:null,fx:function(elem,options,prop){this.options=options;
this.elem=elem;
this.prop=prop;
if(!options.orig){options.orig={}
}}});
jQuery.fx.prototype={update:function(){if(this.options.step){this.options.step.apply(this.elem,[this.now,this])
}(jQuery.fx.step[this.prop]||jQuery.fx.step._default)(this);
if(this.prop=="height"||this.prop=="width"){this.elem.style.display="block"
}},cur:function(force){if(this.elem[this.prop]!=null&&this.elem.style[this.prop]==null){return this.elem[this.prop]
}var r=parseFloat(jQuery.css(this.elem,this.prop,force));
return r&&r>-10000?r:parseFloat(jQuery.curCSS(this.elem,this.prop))||0
},custom:function(from,to,unit){this.startTime=(new Date()).getTime();
this.start=from;
this.end=to;
this.unit=unit||this.unit||"px";
this.now=this.start;
this.pos=this.state=0;
this.update();
var self=this;
function t(gotoEnd){return self.step(gotoEnd)
}t.elem=this.elem;
jQuery.timers.push(t);
if(jQuery.timerId==null){jQuery.timerId=setInterval(function(){var timers=jQuery.timers;
for(var i=0;
i<timers.length;
i++){if(!timers[i]()){timers.splice(i--,1)
}}if(!timers.length){clearInterval(jQuery.timerId);
jQuery.timerId=null
}},13)
}},show:function(){this.options.orig[this.prop]=jQuery.attr(this.elem.style,this.prop);
this.options.show=true;
this.custom(0,this.cur());
if(this.prop=="width"||this.prop=="height"){this.elem.style[this.prop]="1px"
}jQuery(this.elem).show()
},hide:function(){this.options.orig[this.prop]=jQuery.attr(this.elem.style,this.prop);
this.options.hide=true;
this.custom(this.cur(),0)
},step:function(gotoEnd){var t=(new Date()).getTime();
if(gotoEnd||t>this.options.duration+this.startTime){this.now=this.end;
this.pos=this.state=1;
this.update();
this.options.curAnim[this.prop]=true;
var done=true;
for(var i in this.options.curAnim){if(this.options.curAnim[i]!==true){done=false
}}if(done){if(this.options.display!=null){this.elem.style.overflow=this.options.overflow;
this.elem.style.display=this.options.display;
if(jQuery.css(this.elem,"display")=="none"){this.elem.style.display="block"
}}if(this.options.hide){this.elem.style.display="none"
}if(this.options.hide||this.options.show){for(var p in this.options.curAnim){jQuery.attr(this.elem.style,p,this.options.orig[p])
}}}if(done&&jQuery.isFunction(this.options.complete)){this.options.complete.apply(this.elem)
}return false
}else{var n=t-this.startTime;
this.state=n/this.options.duration;
this.pos=jQuery.easing[this.options.easing||(jQuery.easing.swing?"swing":"linear")](this.state,n,0,1,this.options.duration);
this.now=this.start+((this.end-this.start)*this.pos);
this.update()
}return true
}};
jQuery.fx.step={scrollLeft:function(fx){fx.elem.scrollLeft=fx.now
},scrollTop:function(fx){fx.elem.scrollTop=fx.now
},opacity:function(fx){jQuery.attr(fx.elem.style,"opacity",fx.now)
},_default:function(fx){fx.elem.style[fx.prop]=fx.now+fx.unit
}};
jQuery.fn.offset=function(){var left=0,top=0,elem=this[0],results;
if(elem){with(jQuery.browser){var parent=elem.parentNode,offsetChild=elem,offsetParent=elem.offsetParent,doc=elem.ownerDocument,safari2=safari&&parseInt(version)<522&&!/adobeair/i.test(userAgent),fixed=jQuery.css(elem,"position")=="fixed";
if(elem.getBoundingClientRect){var box=elem.getBoundingClientRect();
add(box.left+Math.max(doc.documentElement.scrollLeft,doc.body.scrollLeft),box.top+Math.max(doc.documentElement.scrollTop,doc.body.scrollTop));
add(-doc.documentElement.clientLeft,-doc.documentElement.clientTop)
}else{add(elem.offsetLeft,elem.offsetTop);
while(offsetParent){add(offsetParent.offsetLeft,offsetParent.offsetTop);
if(mozilla&&!/^t(able|d|h)$/i.test(offsetParent.tagName)||safari&&!safari2){border(offsetParent)
}if(!fixed&&jQuery.css(offsetParent,"position")=="fixed"){fixed=true
}offsetChild=/^body$/i.test(offsetParent.tagName)?offsetChild:offsetParent;
offsetParent=offsetParent.offsetParent
}while(parent&&parent.tagName&&!/^body|html$/i.test(parent.tagName)){if(!/^inline|table.*$/i.test(jQuery.css(parent,"display"))){add(-parent.scrollLeft,-parent.scrollTop)
}if(mozilla&&jQuery.css(parent,"overflow")!="visible"){border(parent)
}parent=parent.parentNode
}if((safari2&&(fixed||jQuery.css(offsetChild,"position")=="absolute"))||(mozilla&&jQuery.css(offsetChild,"position")!="absolute")){add(-doc.body.offsetLeft,-doc.body.offsetTop)
}if(fixed){add(Math.max(doc.documentElement.scrollLeft,doc.body.scrollLeft),Math.max(doc.documentElement.scrollTop,doc.body.scrollTop))
}}results={top:top,left:left}
}}function border(elem){add(jQuery.curCSS(elem,"borderLeftWidth",true),jQuery.curCSS(elem,"borderTopWidth",true))
}function add(l,t){left+=parseInt(l)||0;
top+=parseInt(t)||0
}return results
};
jQuery.each(["Height","Width"],function(i,name){var tl=name=="Height"?"Top":"Left",br=name=="Height"?"Bottom":"Right";
jQuery.fn["inner"+name]=function(){return this[name.toLowerCase()]()+num(this,"padding"+tl)+num(this,"padding"+br)
};
jQuery.fn["outer"+name]=function(margin){return this["inner"+name]()+num(this,"border"+tl+"Width")+num(this,"border"+br+"Width")+(!!margin?num(this,"margin"+tl)+num(this,"margin"+br):0)
}
});
function num(elem,prop){elem=elem.jquery?elem[0]:elem;
return elem&&parseInt(jQuery.curCSS(elem,prop,true))||0
}})();
(function($){$.fn.ajaxSubmit=function(options){if(typeof options=="function"){options={success:options}
}options=$.extend({url:this.attr("action")||window.location.toString(),type:this.attr("method")||"GET"},options||{});
var veto={};
$.event.trigger("form.pre.serialize",[this,options,veto]);
if(veto.veto){return this
}var a=this.formToArray(options.semantic);
if(options.data){for(var n in options.data){a.push({name:n,value:options.data[n]})
}}if(options.beforeSubmit&&options.beforeSubmit(a,this,options)===false){return this
}$.event.trigger("form.submit.validate",[a,this,options,veto]);
if(veto.veto){return this
}var q=$.param(a);
if(options.type.toUpperCase()=="GET"){options.url+=(options.url.indexOf("?")>=0?"&":"?")+q;
options.data=null
}else{options.data=q
}var $form=this,callbacks=[];
if(options.resetForm){callbacks.push(function(){$form.resetForm()
})
}if(options.clearForm){callbacks.push(function(){$form.clearForm()
})
}if(!options.dataType&&options.target){var oldSuccess=options.success||function(){};
callbacks.push(function(data){if(this.evalScripts){$(options.target).attr("innerHTML",data).evalScripts().each(oldSuccess,arguments)
}else{$(options.target).html(data).each(oldSuccess,arguments)
}})
}else{if(options.success){callbacks.push(options.success)
}}options.success=function(data,status){for(var i=0,max=callbacks.length;
i<max;
i++){callbacks[i](data,status,$form)
}};
var files=$("input:file",this).fieldValue();
var found=false;
for(var j=0;
j<files.length;
j++){if(files[j]){found=true
}}if(options.iframe||found){if($.browser.safari&&options.closeKeepAlive){$.get(options.closeKeepAlive,fileUpload)
}else{fileUpload()
}}else{$.ajax(options)
}$.event.trigger("form.submit.notify",[this,options]);
return this;
function fileUpload(){var form=$form[0];
var opts=$.extend({},$.ajaxSettings,options);
var id="jqFormIO"+$.fn.ajaxSubmit.counter++;
var $io=$('<iframe id="'+id+'" name="'+id+'" />');
var io=$io[0];
var op8=$.browser.opera&&window.opera.version()<9;
if($.browser.msie||op8){io.src='javascript:false;document.write("");'
}$io.css({position:"absolute",top:"-1000px",left:"-1000px"});
var xhr={responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){}};
var g=opts.global;
if(g&&!$.active++){$.event.trigger("ajaxStart")
}if(g){$.event.trigger("ajaxSend",[xhr,opts])
}var cbInvoked=0;
var timedOut=0;
setTimeout(function(){var encAttr=form.encoding?"encoding":"enctype";
var t=$form.attr("target");
$form.attr({target:id,method:"POST",action:opts.url});
form[encAttr]="multipart/form-data";
if(opts.timeout){setTimeout(function(){timedOut=true;
cb()
},opts.timeout)
}$io.appendTo("body");
io.attachEvent?io.attachEvent("onload",cb):io.addEventListener("load",cb,false);
form.submit();
$form.attr("target",t)
},10);
function cb(){if(cbInvoked++){return 
}io.detachEvent?io.detachEvent("onload",cb):io.removeEventListener("load",cb,false);
var ok=true;
try{if(timedOut){throw"timeout"
}var data,doc;
doc=io.contentWindow?io.contentWindow.document:io.contentDocument?io.contentDocument:io.document;
xhr.responseText=doc.body?doc.body.innerHTML:null;
xhr.responseXML=doc.XMLDocument?doc.XMLDocument:doc;
if(opts.dataType=="json"||opts.dataType=="script"){var ta=doc.getElementsByTagName("textarea")[0];
data=ta?ta.value:xhr.responseText;
if(opts.dataType=="json"){eval("data = "+data)
}else{$.globalEval(data)
}}else{if(opts.dataType=="xml"){data=xhr.responseXML;
if(!data&&xhr.responseText!=null){data=toXml(xhr.responseText)
}}else{data=xhr.responseText
}}}catch(e){ok=false;
$.handleError(opts,xhr,"error",e)
}if(ok){opts.success(data,"success");
if(g){$.event.trigger("ajaxSuccess",[xhr,opts])
}}if(g){$.event.trigger("ajaxComplete",[xhr,opts])
}if(g&&!--$.active){$.event.trigger("ajaxStop")
}if(opts.complete){opts.complete(xhr,ok?"success":"error")
}setTimeout(function(){$io.remove();
xhr.responseXML=null
},100)
}function toXml(s,doc){if(window.ActiveXObject){doc=new ActiveXObject("Microsoft.XMLDOM");
doc.async="false";
doc.loadXML(s)
}else{doc=(new DOMParser()).parseFromString(s,"text/xml")
}return(doc&&doc.documentElement&&doc.documentElement.tagName!="parsererror")?doc:null
}}};
$.fn.ajaxSubmit.counter=0;
$.fn.ajaxForm=function(options){return this.ajaxFormUnbind().submit(submitHandler).each(function(){this.formPluginId=$.fn.ajaxForm.counter++;
$.fn.ajaxForm.optionHash[this.formPluginId]=options;
$(":submit,input:image",this).click(clickHandler)
})
};
$.fn.ajaxForm.counter=1;
$.fn.ajaxForm.optionHash={};
function clickHandler(e){var $form=this.form;
$form.clk=this;
if(this.type=="image"){if(e.offsetX!=undefined){$form.clk_x=e.offsetX;
$form.clk_y=e.offsetY
}else{if(typeof $.fn.offset=="function"){var offset=$(this).offset();
$form.clk_x=e.pageX-offset.left;
$form.clk_y=e.pageY-offset.top
}else{$form.clk_x=e.pageX-this.offsetLeft;
$form.clk_y=e.pageY-this.offsetTop
}}}setTimeout(function(){$form.clk=$form.clk_x=$form.clk_y=null
},10)
}function submitHandler(){var id=this.formPluginId;
var options=$.fn.ajaxForm.optionHash[id];
$(this).ajaxSubmit(options);
return false
}$.fn.ajaxFormUnbind=function(){this.unbind("submit",submitHandler);
return this.each(function(){$(":submit,input:image",this).unbind("click",clickHandler)
})
};
$.fn.formToArray=function(semantic){var a=[];
if(this.length==0){return a
}var form=this[0];
var els=semantic?form.getElementsByTagName("*"):form.elements;
if(!els){return a
}for(var i=0,max=els.length;
i<max;
i++){var el=els[i];
var n=el.name;
if(!n){continue
}if(semantic&&form.clk&&el.type=="image"){if(!el.disabled&&form.clk==el){a.push({name:n+".x",value:form.clk_x},{name:n+".y",value:form.clk_y})
}continue
}var v=$.fieldValue(el,true);
if(v&&v.constructor==Array){for(var j=0,jmax=v.length;
j<jmax;
j++){a.push({name:n,value:v[j]})
}}else{if(v!==null&&typeof v!="undefined"){a.push({name:n,value:v})
}}}if(!semantic&&form.clk){var inputs=form.getElementsByTagName("input");
for(var i=0,max=inputs.length;
i<max;
i++){var input=inputs[i];
var n=input.name;
if(n&&!input.disabled&&input.type=="image"&&form.clk==input){a.push({name:n+".x",value:form.clk_x},{name:n+".y",value:form.clk_y})
}}}return a
};
$.fn.formSerialize=function(semantic){return $.param(this.formToArray(semantic))
};
$.fn.fieldSerialize=function(successful){var a=[];
this.each(function(){var n=this.name;
if(!n){return 
}var v=$.fieldValue(this,successful);
if(v&&v.constructor==Array){for(var i=0,max=v.length;
i<max;
i++){a.push({name:n,value:v[i]})
}}else{if(v!==null&&typeof v!="undefined"){a.push({name:this.name,value:v})
}}});
return $.param(a)
};
$.fn.fieldValue=function(successful){for(var val=[],i=0,max=this.length;
i<max;
i++){var el=this[i];
var v=$.fieldValue(el,successful);
if(v===null||typeof v=="undefined"||(v.constructor==Array&&!v.length)){continue
}v.constructor==Array?$.merge(val,v):val.push(v)
}return val
};
$.fieldValue=function(el,successful){var n=el.name,t=el.type,tag=el.tagName.toLowerCase();
if(typeof successful=="undefined"){successful=true
}if(successful&&(!n||el.disabled||t=="reset"||t=="button"||(t=="checkbox"||t=="radio")&&!el.checked||(t=="submit"||t=="image")&&el.form&&el.form.clk!=el||tag=="select"&&el.selectedIndex==-1)){return null
}if(tag=="select"){var index=el.selectedIndex;
if(index<0){return null
}var a=[],ops=el.options;
var one=(t=="select-one");
var max=(one?index+1:ops.length);
for(var i=(one?index:0);
i<max;
i++){var op=ops[i];
if(op.selected){var v=$.browser.msie&&!(op.attributes.value.specified)?op.text:op.value;
if(one){return v
}a.push(v)
}}return a
}return el.value
};
$.fn.clearForm=function(){return this.each(function(){$("input,select,textarea",this).clearFields()
})
};
$.fn.clearFields=$.fn.clearInputs=function(){return this.each(function(){var t=this.type,tag=this.tagName.toLowerCase();
if(t=="text"||t=="password"||tag=="textarea"){this.value=""
}else{if(t=="checkbox"||t=="radio"){this.checked=false
}else{if(tag=="select"){this.selectedIndex=-1
}}}})
};
$.fn.resetForm=function(){return this.each(function(){if(typeof this.reset=="function"||(typeof this.reset=="object"&&!this.reset.nodeType)){this.reset()
}})
};
$.fn.enable=function(b){if(b==undefined){b=true
}return this.each(function(){this.disabled=!b
})
};
$.fn.select=function(select){if(select==undefined){select=true
}return this.each(function(){var t=this.type;
if(t=="checkbox"||t=="radio"){this.checked=select
}else{if(this.tagName.toLowerCase()=="option"){var $sel=$(this).parent("select");
if(select&&$sel[0]&&$sel[0].type=="select-one"){$sel.find("option").select(false)
}this.selected=select
}}})
}
})(jQuery);
(function($){$.dimensions={version:"@VERSION"};
$.each(["Height","Width"],function(i,name){$.fn["inner"+name]=function(){if(!this[0]){return 
}var torl=name=="Height"?"Top":"Left",borr=name=="Height"?"Bottom":"Right";
return this[name.toLowerCase()]()+num(this,"padding"+torl)+num(this,"padding"+borr)
};
$.fn["outer"+name]=function(options){if(!this[0]){return 
}var torl=name=="Height"?"Top":"Left",borr=name=="Height"?"Bottom":"Right";
options=$.extend({margin:false},options||{});
return this[name.toLowerCase()]()+num(this,"border"+torl+"Width")+num(this,"border"+borr+"Width")+num(this,"padding"+torl)+num(this,"padding"+borr)+(options.margin?(num(this,"margin"+torl)+num(this,"margin"+borr)):0)
}
});
$.each(["Left","Top"],function(i,name){$.fn["scroll"+name]=function(val){if(!this[0]){return 
}return val!=undefined?this.each(function(){this==window||this==document?window.scrollTo(name=="Left"?val:$(window)["scrollLeft"](),name=="Top"?val:$(window)["scrollTop"]()):this["scroll"+name]=val
}):this[0]==window||this[0]==document?self[(name=="Left"?"pageXOffset":"pageYOffset")]||$.boxModel&&document.documentElement["scroll"+name]||document.body["scroll"+name]:this[0]["scroll"+name]
}
});
$.fn.extend({position:function(){var left=0,top=0,elem=this[0],offset,parentOffset,offsetParent,results;
if(elem){offsetParent=this.offsetParent();
offset=this.offset();
parentOffset=offsetParent.offset();
offset.top-=num(elem,"marginTop");
offset.left-=num(elem,"marginLeft");
parentOffset.top+=num(offsetParent,"borderTopWidth");
parentOffset.left+=num(offsetParent,"borderLeftWidth");
results={top:offset.top-parentOffset.top,left:offset.left-parentOffset.left}
}return results
},offsetParent:function(){var offsetParent=this[0].offsetParent;
while(offsetParent&&(!/^body|html$/i.test(offsetParent.tagName)&&$.css(offsetParent,"position")=="static")){offsetParent=offsetParent.offsetParent
}return $(offsetParent)
}});
var num=function(el,prop){return parseInt($.css(el.jquery?el[0]:el,prop))||0
}
})(jQuery);
(function($){$.ui=$.ui||{};
$.extend($.ui,{plugin:{add:function(module,option,set){var proto=$.ui[module].prototype;
for(var i in set){proto.plugins[i]=proto.plugins[i]||[];
proto.plugins[i].push([option,set[i]])
}},call:function(instance,name,arguments){var set=instance.plugins[name];
if(!set){return 
}for(var i=0;
i<set.length;
i++){if(instance.options[set[i][0]]){set[i][1].apply(instance.element,arguments)
}}}},cssCache:{},css:function(name){if($.ui.cssCache[name]){return $.ui.cssCache[name]
}var tmp=$('<div class="ui-resizable-gen">').addClass(name).css({position:"absolute",top:"-5000px",left:"-5000px",display:"block"}).appendTo("body");
$.ui.cssCache[name]=!!((!/auto|default/.test(tmp.css("cursor"))||(/^[1-9]/).test(tmp.css("height"))||(/^[1-9]/).test(tmp.css("width"))||!(/none/).test(tmp.css("backgroundImage"))||!(/transparent|rgba\(0, 0, 0, 0\)/).test(tmp.css("backgroundColor"))));
try{$("body").get(0).removeChild(tmp.get(0))
}catch(e){}return $.ui.cssCache[name]
},disableSelection:function(e){e.unselectable="on";
e.onselectstart=function(){return false
};
if(e.style){e.style.MozUserSelect="none"
}},enableSelection:function(e){e.unselectable="off";
e.onselectstart=function(){return true
};
if(e.style){e.style.MozUserSelect=""
}},hasScroll:function(e,a){var scroll=/top/.test(a||"top")?"scrollTop":"scrollLeft",has=false;
if(e[scroll]>0){return true
}e[scroll]=1;
has=e[scroll]>0?true:false;
e[scroll]=0;
return has
}});
$.each(["Left","Top"],function(i,name){if(!$.fn["scroll"+name]){$.fn["scroll"+name]=function(v){return v!=undefined?this.each(function(){this==window||this==document?window.scrollTo(name=="Left"?v:$(window)["scrollLeft"](),name=="Top"?v:$(window)["scrollTop"]()):this["scroll"+name]=v
}):this[0]==window||this[0]==document?self[(name=="Left"?"pageXOffset":"pageYOffset")]||$.boxModel&&document.documentElement["scroll"+name]||document.body["scroll"+name]:this[0]["scroll"+name]
}
}});
var _remove=$.fn.remove;
$.fn.extend({position:function(){var offset=this.offset();
var offsetParent=this.offsetParent();
var parentOffset=offsetParent.offset();
return{top:offset.top-num(this[0],"marginTop")-parentOffset.top-num(offsetParent,"borderTopWidth"),left:offset.left-num(this[0],"marginLeft")-parentOffset.left-num(offsetParent,"borderLeftWidth")}
},offsetParent:function(){var offsetParent=this[0].offsetParent;
while(offsetParent&&(!/^body|html$/i.test(offsetParent.tagName)&&$.css(offsetParent,"position")=="static")){offsetParent=offsetParent.offsetParent
}return $(offsetParent)
},mouseInteraction:function(o){return this.each(function(){new $.ui.mouseInteraction(this,o)
})
},removeMouseInteraction:function(o){return this.each(function(){if($.data(this,"ui-mouse")){$.data(this,"ui-mouse").destroy()
}})
},remove:function(){jQuery("*",this).add(this).trigger("remove");
return _remove.apply(this,arguments)
}});
function num(el,prop){return parseInt($.curCSS(el.jquery?el[0]:el,prop,true))||0
}$.ui.mouseInteraction=function(element,options){var self=this;
this.element=element;
$.data(this.element,"ui-mouse",this);
this.options=$.extend({},options);
$(element).bind("mousedown.draggable",function(){return self.click.apply(self,arguments)
});
if($.browser.msie){$(element).attr("unselectable","on")
}$(element).mouseup(function(){if(self.timer){clearInterval(self.timer)
}})
};
$.extend($.ui.mouseInteraction.prototype,{destroy:function(){$(this.element).unbind("mousedown.draggable")
},trigger:function(){return this.click.apply(this,arguments)
},click:function(e){if(e.which!=1||$.inArray(e.target.nodeName.toLowerCase(),this.options.dragPrevention||[])!=-1||(this.options.condition&&!this.options.condition.apply(this.options.executor||this,[e,this.element]))){return true
}var self=this;
var initialize=function(){self._MP={left:e.pageX,top:e.pageY};
$(document).bind("mouseup.draggable",function(){return self.stop.apply(self,arguments)
});
$(document).bind("mousemove.draggable",function(){return self.drag.apply(self,arguments)
});
if(!self.initalized&&Math.abs(self._MP.left-e.pageX)>=self.options.distance||Math.abs(self._MP.top-e.pageY)>=self.options.distance){if(self.options.start){self.options.start.call(self.options.executor||self,e,self.element)
}if(self.options.drag){self.options.drag.call(self.options.executor||self,e,this.element)
}self.initialized=true
}};
if(this.options.delay){if(this.timer){clearInterval(this.timer)
}this.timer=setTimeout(initialize,this.options.delay)
}else{initialize()
}return false
},stop:function(e){var o=this.options;
if(!this.initialized){return $(document).unbind("mouseup.draggable").unbind("mousemove.draggable")
}if(this.options.stop){this.options.stop.call(this.options.executor||this,e,this.element)
}$(document).unbind("mouseup.draggable").unbind("mousemove.draggable");
this.initialized=false;
return false
},drag:function(e){var o=this.options;
if($.browser.msie&&!e.button){return this.stop.apply(this,[e])
}if(!this.initialized&&(Math.abs(this._MP.left-e.pageX)>=o.distance||Math.abs(this._MP.top-e.pageY)>=o.distance)){if(this.options.start){this.options.start.call(this.options.executor||this,e,this.element)
}this.initialized=true
}else{if(!this.initialized){return false
}}if(o.drag){o.drag.call(this.options.executor||this,e,this.element)
}return false
}})
})(jQuery);
(function($){$.extend($.expr[":"],{shadowed:"(' '+a.className+' ').indexOf(' fx-shadowed ')"});
$.fn.shadowEnable=function(){return $(this).find("+ .fx-shadow").show().end()
};
$.fn.shadowDisable=function(){return $(this).find("+ .fx-shadow").hide().end()
};
$.fn.shadowDestroy=function(){return $(this).find("+ .fx-shadow").remove().end()
};
$.fn.shadow=function(options){options=$.extend({offset:1,opacity:0.2,color:"#000",monitor:false},options||{});
options.offset-=1;
return this.each(function(){var $element=$(this).shadowDestroy(),$shadow=$("<div class='fx-shadow' style='position: relative;'></div>").insertAfter($element);
baseWidth=$element.outerWidth(),baseHeight=$element.outerHeight(),position=$element.position(),zIndex=parseInt($element.css("zIndex"))||1;
$('<div class="fx-shadow-color fx-shadow-layer-1"></div>').css({position:"absolute",opacity:options.opacity-0.05,left:options.offset,top:options.offset,width:baseWidth+1,height:baseHeight+1}).appendTo($shadow);
$('<div class="fx-shadow-color fx-shadow-layer-2"></div>').css({position:"absolute",opacity:options.opacity-0.1,left:options.offset+2,top:options.offset+2,width:baseWidth,height:baseHeight-3}).appendTo($shadow);
$('<div class="fx-shadow-color fx-shadow-layer-3"></div>').css({position:"absolute",opacity:options.opacity-0.1,left:options.offset+2,top:options.offset+2,width:baseWidth-3,height:baseHeight}).appendTo($shadow);
$('<div class="fx-shadow-color fx-shadow-layer-4"></div>').css({position:"absolute",opacity:options.opacity,left:options.offset+1,top:options.offset+1,width:baseWidth-1,height:baseHeight-1}).appendTo($shadow);
$("div.fx-shadow-color",$shadow).css("background-color",options.color);
$element.css({position:($element.css("position")=="static"?"relative":"")});
$shadow.css({position:"absolute",zIndex:zIndex-1,top:position.top+"px",left:position.left+"px",width:baseWidth,height:baseHeight,marginLeft:$element.css("marginLeft"),marginRight:$element.css("marginRight"),marginBottom:$element.css("marginBottom"),marginTop:$element.css("marginTop")});
if(options.monitor){function rearrangeShadow(){var $element=$(this),$shadow=$element.next();
$shadow.css({top:parseInt($element.css("top"))+"px",left:parseInt($element.css("left"))+"px"});
$(">*",$shadow).css({height:this.offsetHeight+"px",width:this.offsetWidth+"px"})
}if($shadow[0].style.setExpression){$shadow[0].style.setExpression("top","parseInt(this.previousSibling.currentStyle.top ) + 'px'");
$shadow[0].style.setExpression("left","parseInt(this.previousSibling.currentStyle.left) + 'px'")
}else{$element.bind("DOMAttrModified",rearrangeShadow)
}}})
}
})(jQuery);
jQuery.cookie=function(name,value,options){if(typeof value!="undefined"){options=options||{};
if(value===null){value="";
options.expires=-1
}var expires="";
if(options.expires&&(typeof options.expires=="number"||options.expires.toUTCString)){var date;
if(typeof options.expires=="number"){date=new Date();
date.setTime(date.getTime()+(options.expires*24*60*60*1000))
}else{date=options.expires
}expires="; expires="+date.toUTCString()
}var path=options.path?"; path="+(options.path):"";
var domain=options.domain?"; domain="+(options.domain):"";
var secure=options.secure?"; secure":"";
document.cookie=[name,"=",encodeURIComponent(value),expires,path,domain,secure].join("")
}else{var cookieValue=null;
if(document.cookie&&document.cookie!=""){var cookies=document.cookie.split(";");
for(var i=0;
i<cookies.length;
i++){var cookie=jQuery.trim(cookies[i]);
if(cookie.substring(0,name.length+1)==(name+"=")){cookieValue=decodeURIComponent(cookie.substring(name.length+1));
break
}}}return cookieValue
}};
(function($){$.ui=$.ui||{};
$.fn.tabs=function(initial,options){if(initial&&initial.constructor==Object){options=initial;
initial=null
}options=options||{};
initial=initial&&initial.constructor==Number&&--initial||0;
return this.each(function(){new $.ui.tabs(this,$.extend(options,{initial:initial}))
})
};
$.each(["Add","Remove","Enable","Disable","Click","Load","Href"],function(i,method){$.fn["tabs"+method]=function(){var args=arguments;
return this.each(function(){var instance=$.ui.tabs.getInstance(this);
instance[method.toLowerCase()].apply(instance,args)
})
}
});
$.fn.tabsSelected=function(){var selected=-1;
if(this[0]){var instance=$.ui.tabs.getInstance(this[0]),$lis=$("li",this);
selected=$lis.index($lis.filter("."+instance.options.selectedClass)[0])
}return selected>=0?++selected:-1
};
$.ui.tabs=function(el,options){this.source=el;
this.options=$.extend({initial:0,event:"click",disabled:[],cookie:null,unselected:false,unselect:options.unselected?true:false,spinner:"Loading&#8230;",cache:false,idPrefix:"ui-tabs-",ajaxOptions:{},fxSpeed:"normal",add:function(){},remove:function(){},enable:function(){},disable:function(){},click:function(){},hide:function(){},show:function(){},load:function(){},tabTemplate:'<li><a href="#{href}"><span>#{text}</span></a></li>',panelTemplate:"<div></div>",navClass:"ui-tabs-nav",selectedClass:"ui-tabs-selected",unselectClass:"ui-tabs-unselect",disabledClass:"ui-tabs-disabled",panelClass:"ui-tabs-panel",hideClass:"ui-tabs-hide",loadingClass:"ui-tabs-loading"},options);
this.options.event+=".ui-tabs";
this.options.cookie=$.cookie&&$.cookie.constructor==Function&&this.options.cookie;
$.data(el,$.ui.tabs.INSTANCE_KEY,this);
this.tabify(true)
};
$.ui.tabs.INSTANCE_KEY="ui_tabs_instance";
$.ui.tabs.getInstance=function(el){return $.data(el,$.ui.tabs.INSTANCE_KEY)
};
$.extend($.ui.tabs.prototype,{tabId:function(a){return a.title?a.title.replace(/\s/g,"_"):this.options.idPrefix+$.data(a)
},tabify:function(init){this.$lis=$("li:has(a[href])",this.source);
this.$tabs=this.$lis.map(function(){return $("a",this)[0]
});
this.$panels=$([]);
var self=this,o=this.options;
this.$tabs.each(function(i,a){if(a.hash&&a.hash.replace("#","")){self.$panels=self.$panels.add(a.hash)
}else{if($(a).attr("href")!="#"){$.data(a,"href",a.href);
var id=self.tabId(a);
a.href="#"+id;
self.$panels=self.$panels.add($("#"+id)[0]||$(o.panelTemplate).attr("id",id).addClass(o.panelClass).insertAfter(self.$panels[i-1]||self.source))
}else{o.disabled.push(i+1)
}}});
if(init){$(this.source).hasClass(o.navClass)||$(this.source).addClass(o.navClass);
this.$panels.each(function(){var $this=$(this);
$this.hasClass(o.panelClass)||$this.addClass(o.panelClass)
});
for(var i=0,position;
position=o.disabled[i];
i++){this.disable(position)
}this.$tabs.each(function(i,a){if(location.hash){if(a.hash==location.hash){o.initial=i;
if($.browser.msie||$.browser.opera){var $toShow=$(location.hash),toShowId=$toShow.attr("id");
$toShow.attr("id","");
setTimeout(function(){$toShow.attr("id",toShowId)
},500)
}scrollTo(0,0);
return false
}}else{if(o.cookie){o.initial=parseInt($.cookie($.ui.tabs.INSTANCE_KEY+$.data(self.source)))||0;
return false
}else{if(self.$lis.eq(i).hasClass(o.selectedClass)){o.initial=i;
return false
}}}});
var n=this.$lis.length;
while(this.$lis.eq(o.initial).hasClass(o.disabledClass)&&n){o.initial=++o.initial<this.$lis.length?o.initial:0;
n--
}if(!n){o.unselected=o.unselect=true
}this.$panels.addClass(o.hideClass);
this.$lis.removeClass(o.selectedClass);
if(!o.unselected){this.$panels.eq(o.initial).show().removeClass(o.hideClass);
this.$lis.eq(o.initial).addClass(o.selectedClass)
}if(o.initial>=this.$tabs.length){o.initial=0
}var href=!o.unselected&&$.data(this.$tabs[o.initial],"href");
if(href){this.load(o.initial+1,href)
}if(!/^click/.test(o.event)){this.$tabs.bind("click",function(e){e.preventDefault()
})
}}var showAnim={},showSpeed=o.fxShowSpeed||o.fxSpeed,hideAnim={},hideSpeed=o.fxHideSpeed||o.fxSpeed;
if(o.fxSlide||o.fxFade){if(o.fxSlide){showAnim.height="show";
hideAnim.height="hide"
}if(o.fxFade){showAnim.opacity="show";
hideAnim.opacity="hide"
}}else{if(o.fxShow){showAnim=o.fxShow
}else{showAnim["min-width"]=0;
showSpeed=1
}if(o.fxHide){hideAnim=o.fxHide
}else{hideAnim["min-width"]=0;
hideSpeed=1
}}var resetCSS={display:"",overflow:""};
if(!$.browser.msie){resetCSS.opacity=""
}function hideTab(clicked,$hide,$show){$hide.animate(hideAnim,hideSpeed,function(){$hide.addClass(o.hideClass).css(resetCSS);
if($.browser.msie&&hideAnim.opacity){$hide[0].style.filter=""
}o.hide(clicked,$hide[0],$show&&$show[0]||null);
if($show){showTab(clicked,$show,$hide)
}})
}function showTab(clicked,$show,$hide){if(!(o.fxSlide||o.fxFade||o.fxShow)){$show.css("display","block")
}$show.animate(showAnim,showSpeed,function(){$show.removeClass(o.hideClass).css(resetCSS);
if($.browser.msie&&showAnim.opacity){$show[0].style.filter=""
}o.show(clicked,$show[0],$hide&&$hide[0]||null)
})
}function switchTab(clicked,$li,$hide,$show){$li.addClass(o.selectedClass).siblings().removeClass(o.selectedClass);
hideTab(clicked,$hide,$show)
}this.$tabs.unbind(o.event).bind(o.event,function(){var $li=$(this).parents("li:eq(0)"),$hide=self.$panels.filter(":visible"),$show=$(this.hash);
if(($li.hasClass(o.selectedClass)&&!o.unselect)||$li.hasClass(o.disabledClass)||o.click(this,$show[0],$hide[0])===false){this.blur();
return false
}if(o.cookie){$.cookie($.ui.tabs.INSTANCE_KEY+$.data(self.source),self.$tabs.index(this),o.cookie)
}if(o.unselect){if($li.hasClass(o.selectedClass)){$li.removeClass(o.selectedClass);
self.$panels.stop();
hideTab(this,$hide);
this.blur();
return false
}else{if(!$hide.length){self.$panels.stop();
if($.data(this,"href")){var a=this;
self.load(self.$tabs.index(this)+1,$.data(this,"href"),function(){$li.addClass(o.selectedClass).addClass(o.unselectClass);
showTab(a,$show)
})
}else{$li.addClass(o.selectedClass).addClass(o.unselectClass);
showTab(this,$show)
}this.blur();
return false
}}}self.$panels.stop();
if($show.length){if($.data(this,"href")){var a=this;
self.load(self.$tabs.index(this)+1,$.data(this,"href"),function(){switchTab(a,$li,$hide,$show)
})
}else{switchTab(this,$li,$hide,$show)
}}else{throw"jQuery UI Tabs: Mismatching fragment identifier."
}if($.browser.msie){this.blur()
}return false
})
},add:function(url,text,position){if(url&&text){position=position||this.$tabs.length;
var o=this.options,$li=$(o.tabTemplate.replace(/#\{href\}/,url).replace(/#\{text\}/,text));
var id=url.indexOf("#")==0?url.replace("#",""):this.tabId($("a:first-child",$li)[0]);
var $panel=$("#"+id);
$panel=$panel.length&&$panel||$(o.panelTemplate).attr("id",id).addClass(o.panelClass).addClass(o.hideClass);
if(position>=this.$lis.length){$li.appendTo(this.source);
$panel.appendTo(this.source.parentNode)
}else{$li.insertBefore(this.$lis[position-1]);
$panel.insertBefore(this.$panels[position-1])
}this.tabify();
if(this.$tabs.length==1){$li.addClass(o.selectedClass);
$panel.removeClass(o.hideClass);
var href=$.data(this.$tabs[0],"href");
if(href){this.load(position+1,href)
}}o.add(this.$tabs[position],this.$panels[position])
}else{throw"jQuery UI Tabs: Not enough arguments to add tab."
}},remove:function(position){if(position&&position.constructor==Number){var o=this.options,$li=this.$lis.eq(position-1).remove(),$panel=this.$panels.eq(position-1).remove();
if($li.hasClass(o.selectedClass)&&this.$tabs.length>1){this.click(position+(position<this.$tabs.length?1:-1))
}this.tabify();
o.remove($li.end()[0],$panel[0])
}},enable:function(position){var o=this.options,$li=this.$lis.eq(position-1);
$li.removeClass(o.disabledClass);
if($.browser.safari){$li.css("display","inline-block");
setTimeout(function(){$li.css("display","block")
},0)
}o.enable(this.$tabs[position-1],this.$panels[position-1])
},disable:function(position){var o=this.options;
this.$lis.eq(position-1).addClass(o.disabledClass);
o.disable(this.$tabs[position-1],this.$panels[position-1])
},click:function(position){this.$tabs.eq(position-1).trigger(this.options.event)
},load:function(position,url,callback){var self=this,o=this.options,$a=this.$tabs.eq(position-1),a=$a[0],$span=$("span",a);
if(url&&url.constructor==Function){callback=url;
url=null
}if(url){$.data(a,"href",url)
}else{url=$.data(a,"href")
}if(o.spinner){$.data(a,"title",$span.html());
$span.html("<em>"+o.spinner+"</em>")
}var finish=function(){self.$tabs.filter("."+o.loadingClass).each(function(){$(this).removeClass(o.loadingClass);
if(o.spinner){$("span",this).html($.data(this,"title"))
}});
self.xhr=null
};
var ajaxOptions=$.extend(o.ajaxOptions,{url:url,success:function(r){$(a.hash).html(r);
finish();
if(callback&&callback.constructor==Function){callback()
}if(o.cache){$.removeData(a,"href")
}o.load(self.$tabs[position-1],self.$panels[position-1])
}});
if(this.xhr){this.xhr.abort();
finish()
}$a.addClass(o.loadingClass);
setTimeout(function(){self.xhr=$.ajax(ajaxOptions)
},0)
},href:function(position,href){$.data(this.$tabs.eq(position-1)[0],"href",href)
}})
})(jQuery);
(function($){$.fn.extend({slider:function(options){var args=Array.prototype.slice.call(arguments,1);
if(options=="value"){return $.data(this[0],"slider").value(arguments[1])
}return this.each(function(){if(typeof options=="string"){var slider=$.data(this,"slider");
if(slider){slider[options].apply(slider,args)
}}else{if(!$.data(this,"slider")){new $.ui.slider(this,options)
}}})
}});
$.ui.slider=function(element,options){var self=this;
this.element=$(element);
$.data(element,"slider",this);
this.element.addClass("ui-slider");
this.options=$.extend({},$.ui.slider.defaults,options);
var o=this.options;
$.extend(o,{axis:o.axis||(element.offsetWidth<element.offsetHeight?"vertical":"horizontal"),max:!isNaN(parseInt(o.max,10))?{x:parseInt(o.max,10),y:parseInt(o.max,10)}:({x:o.max&&o.max.x||100,y:o.max&&o.max.y||100}),min:!isNaN(parseInt(o.min,10))?{x:parseInt(o.min,10),y:parseInt(o.min,10)}:({x:o.min&&o.min.x||0,y:o.min&&o.min.y||0})});
o.realMax={x:o.max.x-o.min.x,y:o.max.y-o.min.y};
o.stepping={x:o.stepping&&o.stepping.x||parseInt(o.stepping,10)||(o.steps&&o.steps.x?o.realMax.x/o.steps.x:0),y:o.stepping&&o.stepping.y||parseInt(o.stepping,10)||(o.steps&&o.steps.y?o.realMax.y/o.steps.y:0)};
$(element).bind("setData.slider",function(event,key,value){self.options[key]=value
}).bind("getData.slider",function(event,key){return self.options[key]
});
this.handle=$(o.handle,element);
if(!this.handle.length){self.handle=self.generated=$(o.handles||[0]).map(function(){var handle=$("<div/>").addClass("ui-slider-handle").appendTo(element);
if(this.id){handle.attr("id",this.id)
}return handle[0]
})
}$(this.handle).mouseInteraction({executor:this,delay:o.delay,distance:o.distance!=undefined?o.distance:1,dragPrevention:o.prevention?o.prevention.toLowerCase().split(","):["input","textarea","button","select","option"],start:this.start,stop:this.stop,drag:this.drag,condition:function(e,handle){if(!this.disabled){if(this.currentHandle){this.blur(this.currentHandle)
}this.focus(handle,1);
return !this.disabled
}}}).wrap('<a href="javascript:void(0)" style="cursor:default;"></a>').parent().bind("focus",function(e){self.focus(this.firstChild)
}).bind("blur",function(e){self.blur(this.firstChild)
}).bind("keydown",function(e){if(/(37|38|39|40)/.test(e.keyCode)){self.moveTo({x:/(37|39)/.test(e.keyCode)?(e.keyCode==37?"-":"+")+"="+self.oneStep(1):null,y:/(38|40)/.test(e.keyCode)?(e.keyCode==38?"-":"+")+"="+self.oneStep(2):null},this.firstChild)
}});
this.actualSize={width:this.element.outerWidth(),height:this.element.outerHeight()};
this.element.bind("mousedown.slider",function(e){self.click.apply(self,[e]);
self.currentHandle.data("ui-mouse").trigger(e);
self.firstValue=self.firstValue+1
});
$.each(o.handles||[],function(index,handle){self.moveTo(handle.start,index,true)
});
if(!isNaN(o.startValue)){this.moveTo(o.startValue,0,true)
}if(this.handle.length==1){this.previousHandle=this.handle
}if(this.handle.length==2&&o.range){this.createRange()
}};
$.extend($.ui.slider.prototype,{plugins:{},createRange:function(){this.rangeElement=$("<div></div>").addClass("ui-slider-range").css({position:"absolute"}).appendTo(this.element);
this.updateRange()
},updateRange:function(){var prop=this.options.axis=="vertical"?"top":"left";
var size=this.options.axis=="vertical"?"height":"width";
this.rangeElement.css(prop,parseInt($(this.handle[0]).css(prop),10)+this.handleSize(0,this.options.axis=="vertical"?2:1)/2);
this.rangeElement.css(size,parseInt($(this.handle[1]).css(prop),10)-parseInt($(this.handle[0]).css(prop),10))
},getRange:function(){return this.rangeElement?this.convertValue(parseInt(this.rangeElement.css(this.options.axis=="vertical"?"height":"width"),10)):null
},ui:function(e){return{instance:this,options:this.options,handle:this.currentHandle,value:this.options.axis!="both"||!this.options.axis?Math.round(this.value(null,this.options.axis=="vertical"?2:1)):{x:Math.round(this.value(null,1)),y:Math.round(this.value(null,2))},range:this.getRange()}
},propagate:function(n,e){$.ui.plugin.call(this,n,[e,this.ui()]);
this.element.triggerHandler(n=="slide"?n:"slide"+n,[e,this.ui()],this.options[n])
},destroy:function(){this.element.removeClass("ui-slider ui-slider-disabled").removeData("slider").unbind(".slider");
this.handle.removeMouseInteraction();
this.generated&&this.generated.remove()
},enable:function(){this.element.removeClass("ui-slider-disabled");
this.disabled=false
},disable:function(){this.element.addClass("ui-slider-disabled");
this.disabled=true
},focus:function(handle,hard){this.currentHandle=$(handle).addClass("ui-slider-handle-active");
if(hard){this.currentHandle.parent()[0].focus()
}},blur:function(handle){$(handle).removeClass("ui-slider-handle-active");
if(this.currentHandle&&this.currentHandle[0]==handle){this.previousHandle=this.currentHandle;
this.currentHandle=null
}},value:function(handle,axis){if(this.handle.length==1){this.currentHandle=this.handle
}if(!axis){axis=this.options.axis=="vertical"?2:1
}var value=((parseInt($(handle!=undefined&&handle!==null?this.handle[handle]||handle:this.currentHandle).css(axis==1?"left":"top"),10)/(this.actualSize[axis==1?"width":"height"]-this.handleSize(null,axis)))*this.options.realMax[axis==1?"x":"y"])+this.options.min[axis==1?"x":"y"];
var o=this.options;
if(o.stepping[axis==1?"x":"y"]){value=Math.round(value/o.stepping[axis==1?"x":"y"])*o.stepping[axis==1?"x":"y"]
}return value
},convertValue:function(value,axis){if(!axis){axis=this.options.axis=="vertical"?2:1
}return this.options.min[axis==1?"x":"y"]+(value/(this.actualSize[axis==1?"width":"height"]-this.handleSize(null,axis)))*this.options.realMax[axis==1?"x":"y"]
},translateValue:function(value,axis){if(!axis){axis=this.options.axis=="vertical"?2:1
}return((value-this.options.min[axis==1?"x":"y"])/this.options.realMax[axis==1?"x":"y"])*(this.actualSize[axis==1?"width":"height"]-this.handleSize(null,axis))
},handleSize:function(handle,axis){if(!axis){axis=this.options.axis=="vertical"?2:1
}return $(handle!=undefined&&handle!==null?this.handle[handle]:this.currentHandle)[axis==1?"outerWidth":"outerHeight"]()
},click:function(e){var pointer=[e.pageX,e.pageY];
var clickedHandle=false;
this.handle.each(function(){if(this==e.target){clickedHandle=true
}});
if(clickedHandle||this.disabled||!(this.currentHandle||this.previousHandle)){return 
}if(this.previousHandle){this.focus(this.previousHandle,1)
}this.offset=this.element.offset();
this.moveTo({y:this.convertValue(e.pageY-this.offset.top-this.currentHandle.outerHeight()/2),x:this.convertValue(e.pageX-this.offset.left-this.currentHandle.outerWidth()/2)},null,false)
},start:function(e,handle){var o=this.options;
if(!this.currentHandle){this.focus(this.previousHandle,true)
}this.offset=this.element.offset();
this.handleOffset=this.currentHandle.offset();
this.clickOffset={top:e.pageY-this.handleOffset.top,left:e.pageX-this.handleOffset.left};
this.firstValue=this.value();
this.propagate("start",e);
return false
},stop:function(e){this.propagate("stop",e);
if(this.firstValue!=this.value()){this.propagate("change",e)
}this.focus(this.currentHandle,true);
return false
},oneStep:function(axis){if(!axis){axis=this.options.axis=="vertical"?2:1
}return this.options.stepping[axis==1?"x":"y"]?this.options.stepping[axis==1?"x":"y"]:(this.options.realMax[axis==1?"x":"y"]/this.actualSize[axis==1?"width":"height"])*5
},translateRange:function(value,axis){if(this.rangeElement){if(this.currentHandle[0]==this.handle[0]&&value>=this.translateValue(this.value(1),axis)){value=this.translateValue(this.value(1,axis)-this.oneStep(axis),axis)
}if(this.currentHandle[0]==this.handle[1]&&value<=this.translateValue(this.value(0),axis)){value=this.translateValue(this.value(0,axis)+this.oneStep(axis))
}}if(this.options.handles){var handle=this.options.handles[this.handleIndex()];
if(value<this.translateValue(handle.min,axis)){value=this.translateValue(handle.min,axis)
}else{if(value>this.translateValue(handle.max,axis)){value=this.translateValue(handle.max,axis)
}}}return value
},handleIndex:function(){return this.handle.index(this.currentHandle[0])
},translateLimits:function(value,axis){if(!axis){axis=this.options.axis=="vertical"?2:1
}if(value>=this.actualSize[axis==1?"width":"height"]-this.handleSize(null,axis)){value=this.actualSize[axis==1?"width":"height"]-this.handleSize(null,axis)
}if(value<=0){value=0
}return value
},drag:function(e,handle){var o=this.options;
var position={top:e.pageY-this.offset.top-this.clickOffset.top,left:e.pageX-this.offset.left-this.clickOffset.left};
if(!this.currentHandle){this.focus(this.previousHandle,true)
}position.left=this.translateLimits(position.left,1);
position.top=this.translateLimits(position.top,2);
if(o.stepping.x){var value=this.convertValue(position.left,1);
value=Math.round(value/o.stepping.x)*o.stepping.x;
position.left=this.translateValue(value,1)
}if(o.stepping.y){var value=this.convertValue(position.top,2);
value=Math.round(value/o.stepping.y)*o.stepping.y;
position.top=this.translateValue(value,2)
}position.left=this.translateRange(position.left,1);
position.top=this.translateRange(position.top,2);
if(o.axis!="vertical"){this.currentHandle.css({left:position.left})
}if(o.axis!="horizontal"){this.currentHandle.css({top:position.top})
}if(this.rangeElement){this.updateRange()
}this.propagate("slide",e);
return false
},moveTo:function(value,handle,noPropagation){var o=this.options;
if(handle==undefined&&!this.currentHandle&&this.handle.length!=1){return false
}if(handle==undefined&&!this.currentHandle){handle=0
}if(handle!=undefined){this.currentHandle=this.previousHandle=$(this.handle[handle]||handle)
}if(value.x!==undefined&&value.y!==undefined){var x=value.x;
var y=value.y
}else{var x=value,y=value
}if(x&&x.constructor!=Number){var me=/^\-\=/.test(x),pe=/^\+\=/.test(x);
if(me){x=this.value(null,1)-parseInt(x.replace("-=",""),10)
}else{if(pe){x=this.value(null,1)+parseInt(x.replace("+=",""),10)
}}}if(y&&y.constructor!=Number){var me=/^\-\=/.test(y),pe=/^\+\=/.test(y);
if(me){y=this.value(null,2)-parseInt(y.replace("-=",""),10)
}else{if(pe){y=this.value(null,2)+parseInt(y.replace("+=",""),10)
}}}if(o.axis!="vertical"&&x){if(o.stepping.x){x=Math.round(x/o.stepping.x)*o.stepping.x
}x=this.translateValue(x,1);
x=this.translateLimits(x,1);
x=this.translateRange(x,1);
this.currentHandle.css({left:x})
}if(o.axis!="horizontal"&&y){if(o.stepping.y){y=Math.round(y/o.stepping.y)*o.stepping.y
}y=this.translateValue(y,2);
y=this.translateLimits(y,2);
y=this.translateRange(y,2);
this.currentHandle.css({top:y})
}if(this.rangeElement){this.updateRange()
}if(!noPropagation){this.propagate("start",null);
this.propagate("stop",null);
this.propagate("change",null);
this.propagate("slide",null)
}}});
$.ui.slider.defaults={handle:".ui-slider-handle"}
})(jQuery);
(function($){$.fn.extend({accordion:function(options,data){return this.each(function(){var instance=$.data(this,"accordion");
if(!instance){$.data(this,"accordion",new $.ui.accordion(this,options))
}else{if(typeof options=="string"){instance[options](data)
}}})
}});
$.ui.accordion=function(container,options){this.options=options=$.extend({},$.ui.accordion.defaults,options);
this.element=$(container);
if(options.navigation){var current=this.element.find("a").filter(options.navigationFilter);
if(current.length){if(current.filter(options.header).length){options.active=current
}else{options.active=current.parent().parent().prev();
current.addClass("current")
}}}options.headers=this.element.find(options.header);
options.active=findActive(options.headers,options.active);
if(!this.element.hasClass("ui-accordion")){this.element.addClass("ui-accordion");
$("<span class='ui-accordion-left'/>").insertBefore(options.headers);
$("<span class='ui-accordion-right'/>").appendTo(options.headers);
options.headers.addClass("ui-accordion-header").attr("tabindex","0")
}if(options.fillSpace){var maxHeight=this.element.parent().height();
options.headers.each(function(){maxHeight-=$(this).outerHeight()
});
var maxPadding=0;
options.headers.next().each(function(){maxPadding=Math.max(maxPadding,$(this).innerHeight()-$(this).height())
}).height(maxHeight-maxPadding)
}else{if(options.autoHeight){var maxHeight=0;
options.headers.next().each(function(){maxHeight=Math.max(maxHeight,$(this).outerHeight())
}).height(maxHeight)
}}options.headers.not(options.active||"").next().hide();
options.active.parent().andSelf().addClass(options.selectedClass);
if(options.event){this.element.bind((options.event)+".accordion",clickHandler)
}};
$.ui.accordion.prototype={activate:function(index){clickHandler.call(this.element[0],{target:findActive(this.options.headers,index)[0]})
},enable:function(){this.options.disabled=false
},disable:function(){this.options.disabled=true
},destroy:function(){this.options.headers.next().css("display","");
if(this.options.fillSpace||this.options.autoHeight){this.options.headers.next().css("height","")
}$.removeData(this.element[0],"accordion");
this.element.removeClass("ui-accordion").unbind(".accordion")
}};
function scopeCallback(callback,scope){return function(){return callback.apply(scope,arguments)
}
}function completed(cancel){if(!$.data(this,"accordion")){return 
}var instance=$.data(this,"accordion");
var options=instance.options;
options.running=cancel?0:--options.running;
if(options.running){return 
}if(options.clearStyle){options.toShow.add(options.toHide).css({height:"",overflow:""})
}$(this).triggerHandler("accordionchange",[options.data],options.change)
}function toggle(toShow,toHide,data,clickedActive,down){var options=$.data(this,"accordion").options;
options.toShow=toShow;
options.toHide=toHide;
options.data=data;
var complete=scopeCallback(completed,this);
options.running=toHide.size()==0?toShow.size():toHide.size();
if(options.animated){if(!options.alwaysOpen&&clickedActive){$.ui.accordion.animations[options.animated]({toShow:jQuery([]),toHide:toHide,complete:complete,down:down,autoHeight:options.autoHeight})
}else{$.ui.accordion.animations[options.animated]({toShow:toShow,toHide:toHide,complete:complete,down:down,autoHeight:options.autoHeight})
}}else{if(!options.alwaysOpen&&clickedActive){toShow.toggle()
}else{toHide.hide();
toShow.show()
}complete(true)
}}function clickHandler(event){var options=$.data(this,"accordion").options;
if(options.disabled){return false
}if(!event.target&&!options.alwaysOpen){options.active.parent().andSelf().toggleClass(options.selectedClass);
var toHide=options.active.next(),data={instance:this,options:options,newHeader:jQuery([]),oldHeader:options.active,newContent:jQuery([]),oldContent:toHide},toShow=options.active=$([]);
toggle.call(this,toShow,toHide,data);
return false
}var clicked=$(event.target);
if(clicked.parents(options.header).length){while(!clicked.is(options.header)){clicked=clicked.parent()
}}var clickedActive=clicked[0]==options.active[0];
if(options.running||(options.alwaysOpen&&clickedActive)){return false
}if(!clicked.is(options.header)){return 
}options.active.parent().andSelf().toggleClass(options.selectedClass);
if(!clickedActive){clicked.parent().andSelf().addClass(options.selectedClass)
}var toShow=clicked.next(),toHide=options.active.next(),data={instance:this,options:options,newHeader:clicked,oldHeader:options.active,newContent:toShow,oldContent:toHide},down=options.headers.index(options.active[0])>options.headers.index(clicked[0]);
options.active=clickedActive?$([]):clicked;
toggle.call(this,toShow,toHide,data,clickedActive,down);
return false
}function findActive(headers,selector){return selector!=undefined?typeof selector=="number"?headers.filter(":eq("+selector+")"):headers.not(headers.not(selector)):selector===false?$([]):headers.filter(":eq(0)")
}$.extend($.ui.accordion,{defaults:{selectedClass:"selected",alwaysOpen:true,animated:"slide",event:"click",header:"a",autoHeight:true,running:0,navigationFilter:function(){return this.href.toLowerCase()==location.href.toLowerCase()
}},animations:{slide:function(options,additions){options=$.extend({easing:"swing",duration:300},options,additions);
if(!options.toHide.size()){options.toShow.animate({height:"show"},options);
return 
}var hideHeight=options.toHide.height(),showHeight=options.toShow.height(),difference=showHeight/hideHeight;
options.toShow.css({height:0,overflow:"hidden"}).show();
options.toHide.filter(":hidden").each(options.complete).end().filter(":visible").animate({height:"hide"},{step:function(now){var current=(hideHeight-now)*difference;
if($.browser.msie||$.browser.opera){current=Math.ceil(current)
}options.toShow.height(current)
},duration:options.duration,easing:options.easing,complete:function(){if(!options.autoHeight){options.toShow.css("height","auto")
}options.complete()
}})
},bounceslide:function(options){this.slide(options,{easing:options.down?"bounceout":"swing",duration:options.down?1000:200})
},easeslide:function(options){this.slide(options,{easing:"easeinout",duration:700})
}}});
$.fn.activate=function(index){return this.accordion("activate",index)
}
})(jQuery);
(function($){$.fn.extend({dialog:function(options){var args=Array.prototype.slice.call(arguments,1);
return this.each(function(){if(typeof options=="string"){var elem=$(this).is(".ui-dialog")?this:$(this).parents(".ui-dialog:first").find(".ui-dialog-content")[0];
var dialog=elem?$.data(elem,"dialog"):{};
if(dialog[options]){dialog[options].apply(dialog,args)
}}else{if(!$(this).is(".ui-dialog-content")){new $.ui.dialog(this,options)
}}})
}});
$.ui.dialog=function(el,options){this.options=options=$.extend({},$.ui.dialog.defaults,options);
this.element=el;
var self=this;
$.data(this.element,"dialog",this);
$(el).bind("remove",function(){self.destroy()
});
$(el).bind("setData.dialog",function(event,key,value){switch(key){case"draggable":uiDialog.draggable(value?"enable":"disable");
break;
case"dragStart":uiDialog.data("start.draggable",value);
break;
case"drag":uiDialog.data("drag.draggable",value);
break;
case"dragStop":uiDialog.data("stop.draggable",value);
break;
case"height":uiDialog.height(value);
break;
case"maxHeight":case"minHeight":case"maxWidth":case"minWidth":uiDialog.data(key+".resizable",value);
break;
case"position":self.position(value);
break;
case"resizable":uiDialog.resizable(value?"enable":"disable");
break;
case"resizeStart":uiDialog.data("start.resizable",value);
break;
case"resize":uiDialog.data("resize.resizable",value);
break;
case"resizeStop":uiDialog.data("stop.resizable",value);
break;
case"title":$(".ui-dialog-title",uiDialogTitlebar).text(value);
break;
case"width":break
}options[key]=value
}).bind("getData.dialog",function(event,key){return options[key]
});
var uiDialogContent=$(el).addClass("ui-dialog-content");
if(!uiDialogContent.parent().length){uiDialogContent.appendTo("body")
}uiDialogContent.wrap(document.createElement("div")).wrap(document.createElement("div"));
var uiDialogContainer=uiDialogContent.parent().addClass("ui-dialog-container").css({position:"relative"});
var uiDialog=this.uiDialog=uiDialogContainer.parent().hide().addClass("ui-dialog window").css({position:"absolute",width:options.width,height:options.height,overflow:"hidden",visibility:"visible"});
var classNames=uiDialogContent.attr("className").split(" ");
$.each(classNames,function(i,className){if(className!="ui-dialog-content"){uiDialog.addClass(className)
}});
if($.fn.resizable){uiDialog.append('<div class="ui-resizable-n ui-resizable-handle"></div>').append('<div class="ui-resizable-s ui-resizable-handle"></div>').append('<div class="ui-resizable-e ui-resizable-handle"></div>').append('<div class="ui-resizable-w ui-resizable-handle"></div>').append('<div class="ui-resizable-ne ui-resizable-handle"></div>').append('<div class="ui-resizable-se ui-resizable-handle"></div>').append('<div class="ui-resizable-sw ui-resizable-handle"></div>').append('<div class="ui-resizable-nw ui-resizable-handle"></div>');
uiDialog.resizable({maxWidth:options.maxWidth,maxHeight:options.maxHeight,minWidth:options.minWidth,minHeight:options.minHeight,start:options.resizeStart,resize:options.resize,stop:function(e,ui){options.resizeStop&&options.resizeStop.apply(this,arguments);
$.ui.dialog.overlay.resize()
}});
if(!options.resizable){uiDialog.resizable("disable")
}}uiDialogContainer.prepend('<div class="ui-dialog-titlebar"></div>');
var uiDialogTitlebar=$(".ui-dialog-titlebar",uiDialogContainer);
var title=(options.title)?options.title:(uiDialogContent.attr("title"))?uiDialogContent.attr("title"):"";
uiDialogTitlebar.append('<span class="ui-dialog-title">'+title+"</span>");
uiDialogTitlebar.append('<a href="#" class="ui-dialog-titlebar-close"><span>X</span></a>');
this.uiDialogTitlebarClose=$(".ui-dialog-titlebar-close",uiDialogTitlebar).hover(function(){$(this).addClass("ui-dialog-titlebar-close-hover")
},function(){$(this).removeClass("ui-dialog-titlebar-close-hover")
}).mousedown(function(ev){ev.stopPropagation()
}).click(function(){self.close();
return false
}).keydown(function(ev){var ESC=27;
ev.keyCode&&ev.keyCode==ESC&&self.close()
});
var l=0;
$.each(options.buttons,function(){l=1;
return false
});
if(l==1){uiDialog.append('<div class="ui-dialog-buttonpane"></div>');
var uiDialogButtonPane=$(".ui-dialog-buttonpane",uiDialog);
$.each(options.buttons,function(name,value){var btn=$(document.createElement("button")).text(name).click(value);
$(btn).addClass("button");
uiDialogButtonPane.append(btn)
});
if(options.buttonsText){uiDialogButtonPane.append(options.buttonsText)
}}if($.fn.draggable){uiDialog.draggable({handle:".ui-dialog-titlebar",start:function(e,ui){self.activate();
options.dragStart&&options.dragStart.apply(this,arguments)
},drag:options.drag,stop:function(e,ui){options.dragStop&&options.dragStop.apply(this,arguments);
$.ui.dialog.overlay.resize()
}});
if(!options.draggable){uiDialog.draggable("disable")
}}uiDialog.mousedown(function(){self.activate()
});
uiDialogTitlebar.click(function(){self.activate()
});
options.bgiframe&&$.fn.bgiframe&&uiDialog.bgiframe();
this.position=function(pos){var wnd=$(window),doc=$(document),top=doc.scrollTop(),left=doc.scrollLeft();
if(pos.constructor==Array){top+=pos[1];
left+=pos[0]
}else{switch(pos){case"center":top+=(wnd.height()/2)-(uiDialog.height()/2);
left+=(wnd.width()/2)-(uiDialog.width()/2);
break;
case"top":top+=0;
left+=(wnd.width()/2)-(uiDialog.width()/2);
break;
case"right":top+=(wnd.height()/2)-(uiDialog.height()/2);
left+=(wnd.width())-(uiDialog.width());
break;
case"bottom":top+=(wnd.height())-(uiDialog.height());
left+=(wnd.width()/2)-(uiDialog.width()/2);
break;
case"left":top+=(wnd.height()/2)-(uiDialog.height()/2);
left+=0;
break;
default:top+=(wnd.height()/2)-(uiDialog.height()/2);
left+=(wnd.width()/2)-(uiDialog.width()/2)
}}top=top<doc.scrollTop()?doc.scrollTop():top;
uiDialog.css({top:top,left:left})
};
this.open=function(){this.overlay=options.modal?new $.ui.dialog.overlay(self):null;
uiDialog.appendTo("body");
this.position(options.position);
uiDialog.show();
self.moveToTop();
self.activate();
var openEV=null;
var openUI={options:options};
this.uiDialogTitlebarClose.focus();
$(this.element).triggerHandler("dialogopen",[openEV,openUI],options.open)
};
this.activate=function(){!options.modal&&this.moveToTop()
};
this.moveToTop=function(){var maxZ=options.zIndex;
$(".ui-dialog:visible").each(function(){maxZ=Math.max(maxZ,parseInt($(this).css("z-index"),10)||options.zIndex)
});
this.overlay&&this.overlay.$el.css("z-index",++maxZ);
uiDialog.css("z-index",++maxZ)
};
this.close=function(){this.overlay&&this.overlay.destroy();
uiDialog.hide();
var closeEV=null;
var closeUI={options:options};
$(this.element).triggerHandler("dialogclose",[closeEV,closeUI],options.close);
$.ui.dialog.overlay.resize()
};
this.destroy=function(){this.overlay&&this.overlay.destroy();
uiDialog.hide();
$(el).unbind(".dialog").removeClass("ui-dialog-content").hide().appendTo("body");
uiDialog.remove();
$.removeData(this.element,"dialog")
};
if(options.autoOpen){this.open()
}};
$.extend($.ui.dialog,{defaults:{autoOpen:true,bgiframe:false,buttons:[],draggable:true,height:200,minHeight:100,minWidth:150,modal:false,overlay:{},position:"center",resizable:true,width:300,zIndex:1000},overlay:function(dialog){this.$el=$.ui.dialog.overlay.create(dialog)
}});
$.extend($.ui.dialog.overlay,{instances:[],events:$.map("focus,mousedown,mouseup,keydown,keypress,click".split(","),function(e){return e+".dialog-overlay"
}).join(" "),create:function(dialog){if(this.instances.length===0){$("a, :input").bind(this.events,function(){var allow=false;
var $dialog=$(this).parents(".ui-dialog");
if($dialog.length){var $overlays=$(".ui-dialog-overlay");
if($overlays.length){var maxZ=parseInt($overlays.css("z-index"),10);
$overlays.each(function(){maxZ=Math.max(maxZ,parseInt($(this).css("z-index"),10))
});
allow=parseInt($dialog.css("z-index"),10)>maxZ
}else{allow=true
}}return allow
});
$(document).bind("keydown.dialog-overlay",function(e){var ESC=27;
e.keyCode&&e.keyCode==ESC&&dialog.close()
});
$(window).bind("resize.dialog-overlay",$.ui.dialog.overlay.resize)
}var $el=$("<div/>").appendTo(document.body).addClass("ui-dialog-overlay").css($.extend({borderWidth:0,margin:0,padding:0,position:"absolute",top:0,left:0,width:this.width(),height:this.height()},dialog.options.overlay));
dialog.options.bgiframe&&$.fn.bgiframe&&$el.bgiframe();
this.instances.push($el);
return $el
},destroy:function($el){this.instances.splice($.inArray(this.instances,$el),1);
if(this.instances.length===0){$("a, :input").add([document,window]).unbind(".dialog-overlay")
}$el.remove()
},height:function(){if($.browser.msie&&$.browser.version<7){var scrollHeight=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight);
var offsetHeight=Math.max(document.documentElement.offsetHeight,document.body.offsetHeight);
if(scrollHeight<offsetHeight){return $(window).height()+"px"
}else{return scrollHeight+"px"
}}else{return $(document).height()+"px"
}},width:function(){if($.browser.msie&&$.browser.version<7){var scrollWidth=Math.max(document.documentElement.scrollWidth,document.body.scrollWidth);
var offsetWidth=Math.max(document.documentElement.offsetWidth,document.body.offsetWidth);
if(scrollWidth<offsetWidth){return $(window).width()+"px"
}else{return scrollWidth+"px"
}}else{return $(document).width()+"px"
}},resize:function(){var $overlays=$([]);
$.each($.ui.dialog.overlay.instances,function(){$overlays=$overlays.add(this)
});
$overlays.css({width:0,height:0}).css({width:$.ui.dialog.overlay.width(),height:$.ui.dialog.overlay.height()})
}});
$.extend($.ui.dialog.overlay.prototype,{destroy:function(){$.ui.dialog.overlay.destroy(this.$el)
}})
})(jQuery);
/* Copyright (c) 2009 Brandon Aaron (http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 *
 * Version: 3.0.2
 * 
 * Requires: 1.2.2+
 */
(function($){var types=["DOMMouseScroll","mousewheel"];
$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;
i;
){this.addEventListener(types[--i],handler,false)
}}else{this.onmousewheel=handler
}},teardown:function(){if(this.removeEventListener){for(var i=types.length;
i;
){this.removeEventListener(types[--i],handler,false)
}}else{this.onmousewheel=null
}}};
$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel")
},unmousewheel:function(fn){return this.unbind("mousewheel",fn)
}});
function handler(event){var args=[].slice.call(arguments,1),delta=0,returnValue=true;
event=$.event.fix(event||window.event);
event.type="mousewheel";
if(event.wheelDelta){delta=event.wheelDelta/120
}if(event.detail){delta=-event.detail/3
}args.unshift(event,delta);
return $.event.handle.apply(this,args)
}})(jQuery);
(function($){var _hours="hours";
var _minutes="minutes";
var _day="days";
var _month="months";
var _year="years";
function rDF(value){var df=value.toString();
return(df.length==1?0+df:df)
}function rTB(type,value,style,disabled){var tb=$(document.createElement("input"));
tb.attr("type","text");
tb.attr("maxLength",(type==_year?4:2));
tb.attr("disabled",disabled);
tb.addClass(type);
tb[0].value=value;
if(typeof style!="undefined"&&style!=null){tb.css(style)
}return tb[0]
}function rTN(t){return document.createTextNode(t)
}function rT(o,d){var hours=rTB(_hours,rDF(d.getHours()),o.style,o.disabled);
var minutes=rTB(_minutes,rDF(d.getMinutes()),o.style,o.disabled);
var frag=document.createDocumentFragment();
frag.appendChild(hours);
frag.appendChild(rTN(":"));
frag.appendChild(minutes);
return frag
}function rUI(o){var d=o.date;
var frag=document.createDocumentFragment();
if(o.show=="dt"||o.show=="d"){var day=rTB(_day,rDF(d.getDate()),o.style,o.disabled);
var month=rTB(_month,rDF(d.getMonth()+1),o.style,o.disabled);
var year=rTB(_year,rDF(d.getFullYear()),((o.style&&o.yearStyle)==null?null:$.extend({},o.style,o.yearStyle)),o.disabled);
switch(o.format){case"int-r":frag.appendChild(year);
frag.appendChild(rTN("-"));
frag.appendChild(month);
frag.appendChild(rTN("-"));
frag.appendChild(day);
break;
case"eur":frag.appendChild(day);
frag.appendChild(rTN("/"));
frag.appendChild(month);
frag.appendChild(rTN("/"));
frag.appendChild(year);
break;
case"usa":frag.appendChild(day);
frag.appendChild(rTN("/"));
frag.appendChild(month);
frag.appendChild(rTN("/"));
frag.appendChild(year);
break;
default:frag.appendChild(year);
frag.appendChild(rTN("-"));
frag.appendChild(month);
frag.appendChild(rTN("-"));
frag.appendChild(day);
break
}}if(o.show=="dt"||o.show=="t"){switch(o.format){case"int-r":case"eur":case"usa":if(o.show=="t"){frag.appendChild(rT(o,d))
}else{frag.insertBefore(rTN("  "),frag.childNodes[0]);
frag.insertBefore(rT(o,d),frag.childNodes[0])
}break;
default:frag.appendChild(rTN("  "));
frag.appendChild(rT(o,d));
break
}}return frag
}function init(el,o){var type=el[0].nodeName.toLowerCase();
switch(type){case"div":case"label":el.children().remove();
el.append(rUI(o));
el.find("input").keydown(_keydown).keyup(_keyup).change(_change).mouseup(_mouseclick).mousedown(_mouseclick);
el[0]._keyUpClick=_keyUpClick;
el[0]._keyDownClick=_keyDownClick;
break;
case"input":default:var w=el[0].tdWrap=$(document.createElement(o.wrapper));
w.append(rUI(o));
el.css("display","none").after(w);
w.find("input").keydown(_keydown).keyup(_keyup).change(_change).mouseup(_mouseclick).mousedown(_mouseclick);
break
}el[0].timeDateUI=o
}function dispose(el){if(typeof el[0].tdWrap!="undefined"){el[0].tdWrap.remove();
el[0].tdWrap=undefined;
el.css("display","")
}else{el.children().remove()
}el[0].timeDateUI=undefined
}function isLeapYear(year){return(new Date(year,1,29).getDate()==29)
}function getDays(month,year){var days=31;
switch(month){case 2:if(isLeapYear(year)){days=29
}else{days=28
}break;
case 3:days=29;
break;
case 4:case 6:case 8:case 9:case 11:days=30;
break
}return days
}function cMon(month,type){var retValue=null;
month=(isNaN(month)?month:Number(month));
switch(month){case"jan":case"January":retValue=1;
break;
case"feb":case"February":retValue=2;
break;
case"mar":case"March":retValue=3;
break;
case"apr":case"April":retValue=4;
break;
case"may":case"May":retValue=5;
break;
case"jun":case"June":retValue=6;
break;
case"jul":case"July":retValue=7;
break;
case"aug":case"August":retValue=8;
break;
case"sep":case"September":retValue=9;
break;
case"oct":case"October":retValue=10;
break;
case"nov":case"November":retValue=11;
break;
case"dec":case"December":retValue=12;
break;
case 1:retValue=(type=="l"?"January":"jan");
break;
case 2:retValue=(type=="l"?"February":"feb");
break;
case 3:retValue=(type=="l"?"March":"mar");
break;
case 4:retValue=(type=="l"?"April":"apr");
break;
case 5:retValue=(type=="l"?"May":"may");
break;
case 6:retValue=(type=="l"?"June":"jun");
break;
case 7:retValue=(type=="l"?"July":"jul");
break;
case 8:retValue=(type=="l"?"August":"aug");
break;
case 9:retValue=(type=="l"?"September":"sep");
break;
case 10:retValue=(type=="l"?"October":"oct");
break;
case 11:retValue=(type=="l"?"November":"nov");
break;
case 12:retValue=(type=="l"?"December":"dec");
break;
default:retValue=-1;
break
}return retValue
}function rMV(month){return(isNaN(month)?cMon(month):Number(month))
}function _mouseclick(){var spinnerElement=$(this).parent();
spinnerElement[0].__lastSelectedPart=$(this).attr("class")
}function _keydown(evt){var key=evt.which;
if(key>=65&&key<=90){evt.preventDefault()
}else{if(key==38||key==40||key==107||key==109){var w=$(this).parent();
var value=Number(this.value);
var className=$(this).attr("class");
var min=0;
var max=23;
var month=w.find("."+_month);
month=(month.length==1?rMV(month[0].value):null);
var year=w.find("."+_year);
year=(year.length==1?Number(year[0].value):null);
switch(className){case _minutes:max=59;
break;
case _day:min=1;
max=getDays(month,year);
break;
case _month:min=1;
max=12;
break;
case _year:min=1900;
max=3000;
break
}if(!isNaN(value)){switch(key){case 107:case 38:value=(value<max?value+1:value);
break;
case 109:case 40:value=(value>min?value-1:value);
break
}}value=value.toString();
this.value=(value.length==1?0+value:value)
}}}function _keyup(evt){var key=evt.which;
if(key==38||key==40||key==107||key==109){var w=$(this).parent();
var value=Number(this.value);
var className=$(this).attr("class");
var day=w.find("."+_day);
day=(day.length==1?Number(day[0].value):null);
var month=w.find("."+_month);
month=(month.length==1?rMV(month[0].value):null);
var year=w.find("."+_year);
year=(year.length==1?Number(year[0].value):null);
if(className==_year&&month==2&&!isLeapYear(value)&&day==29){w.find("."+_day)[0].value=28
}else{if(className==_month){var maxDays=getDays(month,year);
if(day>maxDays){w.find("."+_day)[0].value=maxDays
}}}}}function _change(evt){var w=$(this).parent();
var value=Number(this.value.replace(/[^0-9]/g,""));
var className=$(this).attr("class");
var min=0;
var max=23;
var day=w.find("."+_day);
day=(day.length==1?Number(day[0].value):null);
var month=w.find("."+_month);
month=(month.length==1?rMV(month[0].value):null);
var year=w.find("."+_year);
year=(year.length==1?Number(year[0].value):null);
switch(className){case _minutes:max=59;
break;
case _day:min=1;
max=getDays(month,year);
break;
case _month:min=1;
max=12;
break;
case _year:min=1900;
max=3000;
break
}if(value>max){value=max
}else{if(value<min){value=min
}}if(className==_year&&month==2&&!isLeapYear(value)&&day==29){w.find("."+_day)[0].value=28
}else{if(className==_month){var maxDays=getDays(month,year);
if(day>maxDays){w.find("."+_day)[0].value=maxDays
}}}value=value.toString();
this.value=(value.length==1?0+value:value)
}function _keyUpClick(){var spinnerElement=$(this);
var selectedField=spinnerElement.find("."+this.__lastSelectedPart);
var value=Number(selectedField[0].value);
var className=$(selectedField[0]).attr("class");
var min=0;
var max=23;
var month=spinnerElement.find("."+_month);
month=(month.length==1?rMV(month[0].value):null);
var year=spinnerElement.find("."+_year);
year=(year.length==1?Number(year[0].value):null);
switch(className){case _minutes:max=59;
break;
case _day:min=1;
max=getDays(month,year);
break;
case _month:min=1;
max=12;
break;
case _year:min=1900;
max=3000;
break
}if(!isNaN(value)){value=(value<max?value+1:value)
}value=value.toString();
selectedField[0].value=(value.length==1?0+value:value);
var spinnerValueFieldId=this.id+"Value";
var spinnerValueField=document.getElementById(spinnerValueFieldId);
spinnerValueField.value=spinnerElement.timeDateUI().getTime()
}function _keyDownClick(){var spinnerElement=$(this);
var selectedField=spinnerElement.find("."+this.__lastSelectedPart);
var value=Number(selectedField[0].value);
var className=$(selectedField[0]).attr("class");
var min=0;
var max=23;
var month=spinnerElement.find("."+_month);
month=(month.length==1?rMV(month[0].value):null);
var year=spinnerElement.find("."+_year);
year=(year.length==1?Number(year[0].value):null);
switch(className){case _minutes:max=59;
break;
case _day:min=1;
max=getDays(month,year);
break;
case _month:min=1;
max=12;
break;
case _year:min=1900;
max=3000;
break
}if(!isNaN(value)){value=(value>min?value-1:value)
}value=value.toString();
selectedField[0].value=(value.length==1?0+value:value);
var spinnerValueFieldId=this.id+"Value";
var spinnerValueField=document.getElementById(spinnerValueFieldId);
spinnerValueField.value=spinnerElement.timeDateUI().getTime()
}$.fn.timeDateUI=function(options){var opts=$.extend({},$.timeDateUI.defaults,options);
if(typeof options=="undefined"&&typeof this[0].timeDateUI!="undefined"){var first=this[0];
var w=(typeof first.tdWrap!="undefined"?first.tdWrap:$(first));
var hour=w.find("."+_hours);
hour=(hour.length==1?Number(hour[0].value):null);
var minute=w.find("."+_minutes);
minute=(minute.length==1?Number(minute[0].value):null);
var day=w.find("."+_day);
day=(day.length==1?Number(day[0].value):null);
var month=w.find("."+_month);
month=(month.length==1?rMV(month[0].value)-1:null);
var year=w.find("."+_year);
year=(year.length==1?Number(year[0].value):null);
return new Date(year,month,day,hour,minute)
}else{return this.each(function(){var $this=$(this);
var o=$.meta?$.extend({},opts,$this.data()):opts;
if(typeof this.timeDateUI!="undefined"){if(options=="remove"||o.remove==true){dispose($this)
}}else{init($this,o)
}})
}};
$.timeDateUI={};
$.timeDateUI.defaults={wrapper:"div",remove:false,style:null,yearStyle:null,date:new Date(),format:"eur",show:"dt",monthType:"d",disabled:false}
})(jQuery);
function DateSpinnerKeyUp(spinnerId){var spinner=document.getElementById(spinnerId);
spinner._keyUpClick()
}function DateSpinnerKeyDown(spinnerId){var spinner=document.getElementById(spinnerId);
spinner._keyDownClick()
}(function($){$.fn.extend({draggable:function(options){var args=Array.prototype.slice.call(arguments,1);
return this.each(function(){if(typeof options=="string"){var drag=$.data(this,"draggable");
if(drag){drag[options].apply(drag,args)
}}else{if(!$.data(this,"draggable")){new $.ui.draggable(this,options)
}}})
}});
$.ui.draggable=function(element,options){var self=this;
this.element=$(element);
$.data(element,"draggable",this);
this.element.addClass("ui-draggable");
this.options=$.extend({},options);
var o=this.options;
$.extend(o,{helper:o.ghosting==true?"clone":(o.helper||"original"),handle:o.handle?($(o.handle,element)[0]?$(o.handle,element):this.element):this.element,appendTo:o.appendTo||"parent"});
$(element).bind("setData.draggable",function(event,key,value){self.options[key]=value
}).bind("getData.draggable",function(event,key){return self.options[key]
});
$(o.handle).mouseInteraction({executor:this,delay:o.delay,distance:o.distance||1,dragPrevention:o.cancel||o.cancel===""?o.cancel.toLowerCase().split(","):["input","textarea","button","select","option"],start:this.start,stop:this.stop,drag:this.drag,condition:function(e){return !(e.target.className.indexOf("ui-resizable-handle")!=-1||this.options.disabled)
}});
if(o.helper=="original"&&(this.element.css("position")=="static"||this.element.css("position")=="")){this.element.css("position","relative")
}if(o.cursorAt&&o.cursorAt.constructor==Array){o.cursorAt={left:o.cursorAt[0],top:o.cursorAt[1]}
}};
$.extend($.ui.draggable.prototype,{plugins:{},ui:function(e){return{helper:this.helper,position:this.position,absolutePosition:this.positionAbs,instance:this,options:this.options,element:this.element}
},propagate:function(n,e){$.ui.plugin.call(this,n,[e,this.ui()]);
return this.element.triggerHandler(n=="drag"?n:"drag"+n,[e,this.ui()],this.options[n])
},destroy:function(){if(!$.data(this.element[0],"draggable")){return 
}this.options.handle.removeMouseInteraction();
this.element.removeClass("ui-draggable ui-draggable-disabled").removeData("draggable").unbind(".draggable")
},enable:function(){this.element.removeClass("ui-draggable-disabled");
this.options.disabled=false
},disable:function(){this.element.addClass("ui-draggable-disabled");
this.options.disabled=true
},setContrains:function(minLeft,maxLeft,minTop,maxTop){this.minLeft=minLeft;
this.maxLeft=maxLeft;
this.minTop=minTop;
this.maxTop=maxTop;
this.constrainsSet=true
},checkConstrains:function(){if(!this.constrainsSet){return 
}if(this.position.left<this.minLeft){this.position.left=this.minLeft
}if(this.position.left>this.maxLeft-this.helperProportions.width){this.position.left=this.maxLeft-this.helperProportions.width
}if(this.position.top<this.minTop){this.position.top=this.minTop
}if(this.position.top>this.maxTop-this.helperProportions.height){this.position.top=this.maxTop-this.helperProportions.height
}},recallOffset:function(e){var elementPosition={left:this.elementOffset.left-this.offsetParentOffset.left,top:this.elementOffset.top-this.offsetParentOffset.top};
var r=this.helper.css("position")=="relative";
this.originalPosition={left:(r?parseInt(this.helper.css("left"),10)||0:elementPosition.left+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollLeft)),top:(r?parseInt(this.helper.css("top"),10)||0:elementPosition.top+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollTop))};
this.offset={left:this._pageX-this.originalPosition.left,top:this._pageY-this.originalPosition.top}
},start:function(e){var o=this.options;
if($.ui.ddmanager){$.ui.ddmanager.current=this
}this.helper=typeof o.helper=="function"?$(o.helper.apply(this.element[0],[e])):(o.helper=="clone"?this.element.clone().appendTo((o.appendTo=="parent"?this.element[0].parentNode:o.appendTo)):this.element);
if(this.helper[0]!=this.element[0]){this.helper.css("position","absolute")
}if(!this.helper.parents("body").length){this.helper.appendTo((o.appendTo=="parent"?this.element[0].parentNode:o.appendTo))
}this.offsetParent=(function(cp){while(cp){if(cp.style&&(/(absolute|relative|fixed)/).test($.css(cp,"position"))){return $(cp)
}cp=cp.parentNode?cp.parentNode:null
}return $("body")
})(this.helper[0].parentNode);
this.elementOffset=this.element.offset();
this.offsetParentOffset=this.offsetParent.offset();
var elementPosition={left:this.elementOffset.left-this.offsetParentOffset.left,top:this.elementOffset.top-this.offsetParentOffset.top};
this._pageX=e.pageX;
this._pageY=e.pageY;
this.clickOffset={left:e.pageX-this.elementOffset.left,top:e.pageY-this.elementOffset.top};
var r=this.helper.css("position")=="relative";
this.originalPosition={left:(r?parseInt(this.helper.css("left"),10)||0:elementPosition.left+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollLeft)),top:(r?parseInt(this.helper.css("top"),10)||0:elementPosition.top+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollTop))};
if(this.element.css("position")=="fixed"){this.originalPosition.top-=this.offsetParent[0]==document.body?$(document).scrollTop():this.offsetParent[0].scrollTop;
this.originalPosition.left-=this.offsetParent[0]==document.body?$(document).scrollLeft():this.offsetParent[0].scrollLeft
}this.offset={left:e.pageX-this.originalPosition.left,top:e.pageY-this.originalPosition.top};
if(this.element[0]!=this.helper[0]){this.offset.left+=parseInt(this.element.css("marginLeft"),10)||0;
this.offset.top+=parseInt(this.element.css("marginTop"),10)||0
}this.propagate("start",e);
this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()};
if($.ui.ddmanager&&!o.dropBehaviour){$.ui.ddmanager.prepareOffsets(this,e)
}if(o.cursorAt){if(o.cursorAt.top!=undefined||o.cursorAt.bottom!=undefined){this.offset.top-=this.clickOffset.top-(o.cursorAt.top!=undefined?o.cursorAt.top:(this.helperProportions.height-o.cursorAt.bottom));
this.clickOffset.top=(o.cursorAt.top!=undefined?o.cursorAt.top:(this.helperProportions.height-o.cursorAt.bottom))
}if(o.cursorAt.left!=undefined||o.cursorAt.right!=undefined){this.offset.left-=this.clickOffset.left-(o.cursorAt.left!=undefined?o.cursorAt.left:(this.helperProportions.width-o.cursorAt.right));
this.clickOffset.left=(o.cursorAt.left!=undefined?o.cursorAt.left:(this.helperProportions.width-o.cursorAt.right))
}}return false
},clear:function(){if($.ui.ddmanager){$.ui.ddmanager.current=null
}this.helper=null
},stop:function(e){if($.ui.ddmanager&&!this.options.dropBehaviour){$.ui.ddmanager.drop(this,e)
}this.propagate("stop",e);
if(this.cancelHelperRemoval){return false
}if(this.options.helper!="original"){this.helper.remove()
}this.clear();
return false
},drag:function(e){this.position={top:e.pageY-this.offset.top,left:e.pageX-this.offset.left};
this.positionAbs={left:e.pageX-this.clickOffset.left,top:e.pageY-this.clickOffset.top};
this.checkConstrains();
this.position=this.propagate("drag",e)||this.position;
this.checkConstrains();
$(this.helper).css({left:this.position.left+"px",top:this.position.top+"px"});
if($.ui.ddmanager){$.ui.ddmanager.drag(this,e)
}return false
}});
$.ui.plugin.add("draggable","cursor",{start:function(e,ui){var t=$("body");
if(t.css("cursor")){ui.options._cursor=t.css("cursor")
}t.css("cursor",ui.options.cursor)
},stop:function(e,ui){if(ui.options._cursor){$("body").css("cursor",ui.options._cursor)
}}});
$.ui.plugin.add("draggable","zIndex",{start:function(e,ui){var t=$(ui.helper);
if(t.css("zIndex")){ui.options._zIndex=t.css("zIndex")
}t.css("zIndex",ui.options.zIndex)
},stop:function(e,ui){if(ui.options._zIndex){$(ui.helper).css("zIndex",ui.options._zIndex)
}}});
$.ui.plugin.add("draggable","opacity",{start:function(e,ui){var t=$(ui.helper);
if(t.css("opacity")){ui.options._opacity=t.css("opacity")
}t.css("opacity",ui.options.opacity)
},stop:function(e,ui){if(ui.options._opacity){$(ui.helper).css("opacity",ui.options._opacity)
}}});
$.ui.plugin.add("draggable","revert",{stop:function(e,ui){var self=ui.instance,helper=$(self.helper);
self.cancelHelperRemoval=true;
$(ui.helper).animate({left:self.originalPosition.left,top:self.originalPosition.top},parseInt(ui.options.revert,10)||500,function(){if(ui.options.helper!="original"){helper.remove()
}if(!helper){self.clear()
}})
}});
$.ui.plugin.add("draggable","iframeFix",{start:function(e,ui){var o=ui.options;
if(ui.instance.slowMode){return 
}if(o.iframeFix.constructor==Array){for(var i=0;
i<o.iframeFix.length;
i++){var co=$(o.iframeFix[i]).offset({border:false});
$('<div class="DragDropIframeFix"" style="background: #fff;"></div>').css("width",$(o.iframeFix[i])[0].offsetWidth+"px").css("height",$(o.iframeFix[i])[0].offsetHeight+"px").css("position","absolute").css("opacity","0.001").css("z-index","1000").css("top",co.top+"px").css("left",co.left+"px").appendTo("body")
}}else{$("iframe").each(function(){var co=$(this).offset({border:false});
$('<div class="DragDropIframeFix" style="background: #fff;"></div>').css("width",this.offsetWidth+"px").css("height",this.offsetHeight+"px").css("position","absolute").css("opacity","0.001").css("z-index","1000").css("top",co.top+"px").css("left",co.left+"px").appendTo("body")
})
}},stop:function(e,ui){if(ui.options.iframeFix){$("div.DragDropIframeFix").each(function(){this.parentNode.removeChild(this)
})
}}});
$.ui.plugin.add("draggable","containment",{start:function(e,ui){var o=ui.options;
var self=ui.instance;
if((o.containment.left!=undefined||o.containment.constructor==Array)&&!o._containment){return 
}if(!o._containment){o._containment=o.containment
}if(o._containment=="parent"){o._containment=this[0].parentNode
}if(o._containment=="document"){o.containment=[0,0,$(document).width(),($(document).height()||document.body.parentNode.scrollHeight)]
}else{var ce=$(o._containment)[0];
var co=$(o._containment).offset();
o.containment=[co.left,co.top,co.left+(ce.offsetWidth||ce.scrollWidth),co.top+(ce.offsetHeight||ce.scrollHeight)]
}var c=o.containment;
ui.instance.setContrains(c[0]-(self.offset.left-self.clickOffset.left),c[2]-(self.offset.left-self.clickOffset.left),c[1]-(self.offset.top-self.clickOffset.top),c[3]-(self.offset.top-self.clickOffset.top))
}});
$.ui.plugin.add("draggable","grid",{drag:function(e,ui){var o=ui.options;
var newLeft=ui.instance.originalPosition.left+Math.round((e.pageX-ui.instance._pageX)/o.grid[0])*o.grid[0];
var newTop=ui.instance.originalPosition.top+Math.round((e.pageY-ui.instance._pageY)/o.grid[1])*o.grid[1];
ui.instance.position.left=newLeft;
ui.instance.position.top=newTop
}});
$.ui.plugin.add("draggable","axis",{drag:function(e,ui){var o=ui.options;
if(o.constraint){o.axis=o.constraint
}switch(o.axis){case"x":ui.instance.position.top=ui.instance.originalPosition.top;
break;
case"y":ui.instance.position.left=ui.instance.originalPosition.left;
break
}}});
$.ui.plugin.add("draggable","scroll",{start:function(e,ui){var o=ui.options;
o.scrollSensitivity=o.scrollSensitivity||20;
o.scrollSpeed=o.scrollSpeed||20;
ui.instance.overflowY=function(el){do{if(/auto|scroll/.test(el.css("overflow"))||(/auto|scroll/).test(el.css("overflow-y"))){return el
}el=el.parent()
}while(el[0].parentNode);
return $(document)
}(this);
ui.instance.overflowX=function(el){do{if(/auto|scroll/.test(el.css("overflow"))||(/auto|scroll/).test(el.css("overflow-x"))){return el
}el=el.parent()
}while(el[0].parentNode);
return $(document)
}(this)
},drag:function(e,ui){var o=ui.options;
var i=ui.instance;
if(i.overflowY[0]!=document&&i.overflowY[0].tagName!="HTML"){if(i.overflowY[0].offsetHeight-(ui.position.top-i.overflowY[0].scrollTop+i.clickOffset.top)<o.scrollSensitivity){i.overflowY[0].scrollTop=i.overflowY[0].scrollTop+o.scrollSpeed
}if((ui.position.top-i.overflowY[0].scrollTop+i.clickOffset.top)<o.scrollSensitivity){i.overflowY[0].scrollTop=i.overflowY[0].scrollTop-o.scrollSpeed
}}else{if(e.pageY-$(document).scrollTop()<o.scrollSensitivity){$(document).scrollTop($(document).scrollTop()-o.scrollSpeed)
}if($(window).height()-(e.pageY-$(document).scrollTop())<o.scrollSensitivity){$(document).scrollTop($(document).scrollTop()+o.scrollSpeed)
}}if(i.overflowX[0]!=document&&i.overflowX[0].tagName!="HTML"){if(i.overflowX[0].offsetWidth-(ui.position.left-i.overflowX[0].scrollLeft+i.clickOffset.left)<o.scrollSensitivity){i.overflowX[0].scrollLeft=i.overflowX[0].scrollLeft+o.scrollSpeed
}if((ui.position.top-i.overflowX[0].scrollLeft+i.clickOffset.left)<o.scrollSensitivity){i.overflowX[0].scrollLeft=i.overflowX[0].scrollLeft-o.scrollSpeed
}}else{if(e.pageX-$(document).scrollLeft()<o.scrollSensitivity){$(document).scrollLeft($(document).scrollLeft()-o.scrollSpeed)
}if($(window).width()-(e.pageX-$(document).scrollLeft())<o.scrollSensitivity){$(document).scrollLeft($(document).scrollLeft()+o.scrollSpeed)
}}ui.instance.recallOffset(e)
}});
$.ui.plugin.add("draggable","snap",{start:function(e,ui){ui.instance.snapElements=[];
$(ui.options.snap===true?".ui-draggable":ui.options.snap).each(function(){var $t=$(this);
var $o=$t.offset();
if(this!=ui.instance.element[0]){ui.instance.snapElements.push({item:this,width:$t.outerWidth(),height:$t.outerHeight(),top:$o.top,left:$o.left})
}})
},drag:function(e,ui){var d=ui.options.snapTolerance||20;
var x1=ui.absolutePosition.left,x2=x1+ui.instance.helperProportions.width,y1=ui.absolutePosition.top,y2=y1+ui.instance.helperProportions.height;
for(var i=ui.instance.snapElements.length-1;
i>=0;
i--){var l=ui.instance.snapElements[i].left,r=l+ui.instance.snapElements[i].width,t=ui.instance.snapElements[i].top,b=t+ui.instance.snapElements[i].height;
if(!((l-d<x1&&x1<r+d&&t-d<y1&&y1<b+d)||(l-d<x1&&x1<r+d&&t-d<y2&&y2<b+d)||(l-d<x2&&x2<r+d&&t-d<y1&&y1<b+d)||(l-d<x2&&x2<r+d&&t-d<y2&&y2<b+d))){continue
}if(ui.options.snapMode!="inner"){var ts=Math.abs(t-y2)<=20;
var bs=Math.abs(b-y1)<=20;
var ls=Math.abs(l-x2)<=20;
var rs=Math.abs(r-x1)<=20;
if(ts){ui.position.top=t-ui.instance.offset.top+ui.instance.clickOffset.top-ui.instance.helperProportions.height
}if(bs){ui.position.top=b-ui.instance.offset.top+ui.instance.clickOffset.top
}if(ls){ui.position.left=l-ui.instance.offset.left+ui.instance.clickOffset.left-ui.instance.helperProportions.width
}if(rs){ui.position.left=r-ui.instance.offset.left+ui.instance.clickOffset.left
}}if(ui.options.snapMode!="outer"){var ts=Math.abs(t-y1)<=20;
var bs=Math.abs(b-y2)<=20;
var ls=Math.abs(l-x1)<=20;
var rs=Math.abs(r-x2)<=20;
if(ts){ui.position.top=t-ui.instance.offset.top+ui.instance.clickOffset.top
}if(bs){ui.position.top=b-ui.instance.offset.top+ui.instance.clickOffset.top-ui.instance.helperProportions.height
}if(ls){ui.position.left=l-ui.instance.offset.left+ui.instance.clickOffset.left
}if(rs){ui.position.left=r-ui.instance.offset.left+ui.instance.clickOffset.left-ui.instance.helperProportions.width
}}}}});
$.ui.plugin.add("draggable","connectToSortable",{start:function(e,ui){ui.instance.sortable=$.data($(ui.options.connectToSortable)[0],"sortable");
ui.instance.sortableOffset=ui.instance.sortable.element.offset();
ui.instance.sortableOuterWidth=ui.instance.sortable.element.outerWidth();
ui.instance.sortableOuterHeight=ui.instance.sortable.element.outerHeight();
if(ui.instance.sortable.options.revert){ui.instance.sortable.shouldRevert=true
}},stop:function(e,ui){var inst=ui.instance.sortable;
if(inst.isOver){inst.isOver=0;
ui.instance.cancelHelperRemoval=true;
inst.cancelHelperRemoval=false;
if(inst.shouldRevert){inst.options.revert=true
}inst.stop(e);
inst.options.helper="original"
}},drag:function(e,ui){var inst=ui.instance.sortable;
ui.instance.position.absolute=ui.absolutePosition;
if(inst.intersectsWith.call(ui.instance,{left:ui.instance.sortableOffset.left,top:ui.instance.sortableOffset.top,width:ui.instance.sortableOuterWidth,height:ui.instance.sortableOuterHeight})){if(!inst.isOver){inst.isOver=1;
var height=inst.options.placeholderElement?$(inst.options.placeholderElement,$(inst.options.items,inst.element)).innerHeight():$(inst.options.items,inst.element).innerHeight();
var width=inst.options.placeholderElement?$(inst.options.placeholderElement,$(inst.options.items,inst.element)).innerWidth():$(inst.options.items,inst.element).innerWidth();
inst.currentItem=$(this).clone().appendTo(inst.element);
inst.options.helper=function(){return ui.helper[0]
};
inst.start(e);
inst.clickOffset.top=ui.instance.clickOffset.top;
inst.clickOffset.left=ui.instance.clickOffset.left;
inst.offset.left-=ui.absolutePosition.left-inst.position.absolute.left;
inst.offset.top-=ui.absolutePosition.top-inst.position.absolute.top;
inst.helperProportions={width:width,height:height};
ui.helper.animate({height:height,width:width},500);
ui.instance.propagate("toSortable",e)
}if(inst.currentItem){inst.drag(e)
}}else{if(inst.isOver){inst.isOver=0;
inst.cancelHelperRemoval=true;
inst.options.revert=false;
inst.stop(e);
inst.options.helper="original";
inst.currentItem.remove();
inst.placeholder.remove();
ui.helper.animate({height:this.innerHeight(),width:this.innerWidth()},500);
ui.instance.propagate("fromSortable",e)
}}}})
})(jQuery);
(function($){$.fn.extend({droppable:function(options){var args=Array.prototype.slice.call(arguments,1);
return this.each(function(){if(typeof options=="string"){var drop=$.data(this,"droppable");
if(drop){drop[options].apply(drop,args)
}}else{if(!$.data(this,"droppable")){new $.ui.droppable(this,options)
}}})
}});
$.ui.droppable=function(element,options){var instance=this;
this.element=$(element);
$.data(element,"droppable",this);
this.element.addClass("ui-droppable");
var o=this.options=options=$.extend({},$.ui.droppable.defaults,options);
var accept=o.accept;
o=$.extend(o,{accept:o.accept&&o.accept.constructor==Function?o.accept:function(d){return $(d).is(accept)
}});
$(element).bind("setData.droppable",function(event,key,value){o[key]=value
}).bind("getData.droppable",function(event,key){return o[key]
}).bind("remove",function(){instance.destroy()
});
this.proportions={width:this.element.outerWidth(),height:this.element.outerHeight()};
$.ui.ddmanager.droppables.push({item:this,over:0,out:1})
};
$.extend($.ui.droppable,{defaults:{disabled:false,tolerance:"intersect"}});
$.extend($.ui.droppable.prototype,{plugins:{},ui:function(c){return{instance:this,draggable:(c.currentItem||c.element),helper:c.helper,position:c.position,absolutePosition:c.positionAbs,options:this.options,element:this.element}
},destroy:function(){var drop=$.ui.ddmanager.droppables;
for(var i=0;
i<drop.length;
i++){if(drop[i].item==this){drop.splice(i,1)
}}this.element.removeClass("ui-droppable ui-droppable-disabled").removeData("droppable").unbind(".droppable")
},enable:function(){this.element.removeClass("ui-droppable-disabled");
this.options.disabled=false
},disable:function(){this.element.addClass("ui-droppable-disabled");
this.options.disabled=true
},over:function(e){var draggable=$.ui.ddmanager.current;
if(!draggable||(draggable.currentItem||draggable.element)[0]==this.element[0]){return 
}if(true||this.options.accept.call(this.element,(draggable.currentItem||draggable.element))){$.ui.plugin.call(this,"over",[e,this.ui(draggable)]);
this.element.triggerHandler("dropover",[e,this.ui(draggable)],this.options.over)
}},out:function(e){var draggable=$.ui.ddmanager.current;
if(!draggable||(draggable.currentItem||draggable.element)[0]==this.element[0]){return 
}if(true||this.options.accept.call(this.element,(draggable.currentItem||draggable.element))){$.ui.plugin.call(this,"out",[e,this.ui(draggable)]);
this.element.triggerHandler("dropout",[e,this.ui(draggable)],this.options.out)
}},drop:function(e,custom){var draggable=custom||$.ui.ddmanager.current;
if(!draggable||(draggable.currentItem||draggable.element)[0]==this.element[0]){return 
}var childrenIntersection=false;
this.element.find(".ui-droppable").each(function(){var inst=$.data(this,"droppable");
if(inst.options.greedy&&$.ui.intersect(draggable,{item:inst,offset:inst.element.offset()},inst.options.tolerance)){childrenIntersection=true;
return false
}});
if(childrenIntersection){return 
}if(true||this.options.accept.call(this.element,(draggable.currentItem||draggable.element))){$.ui.plugin.call(this,"drop",[e,this.ui(draggable)]);
this.element.triggerHandler("drop",[e,this.ui(draggable)],this.options.drop)
}},activate:function(e){var draggable=$.ui.ddmanager.current;
$.ui.plugin.call(this,"activate",[e,this.ui(draggable)]);
if(draggable){this.element.triggerHandler("dropactivate",[e,this.ui(draggable)],this.options.activate)
}},deactivate:function(e){var draggable=$.ui.ddmanager.current;
$.ui.plugin.call(this,"deactivate",[e,this.ui(draggable)]);
if(draggable){this.element.triggerHandler("dropdeactivate",[e,this.ui(draggable)],this.options.deactivate)
}}});
$.ui.intersect=function(draggable,droppable,toleranceMode){if(!droppable.offset){return false
}var x1=(draggable.positionAbs||draggable.position.absolute).left,x2=x1+draggable.helperProportions.width,y1=(draggable.positionAbs||draggable.position.absolute).top,y2=y1+draggable.helperProportions.height;
var l=droppable.offset.left,r=l+droppable.item.proportions.width,t=droppable.offset.top,b=t+droppable.item.proportions.height;
switch(toleranceMode){case"fit":if(!((y2-(draggable.helperProportions.height/2)>t&&y1<t)||(y1<b&&y2>b)||(x2>l&&x1<l)||(x1<r&&x2>r))){return false
}if(y2-(draggable.helperProportions.height/2)>t&&y1<t){return 1
}if(y1<b&&y2>b){return 2
}if(x2>l&&x1<l){return 1
}if(x1<r&&x2>r){return 2
}break;
case"intersect":return(l<x1+(draggable.helperProportions.width/2)&&x2-(draggable.helperProportions.width/2)<r&&t<y1+(draggable.helperProportions.height/2)&&y2-(draggable.helperProportions.height/2)<b);
break;
case"pointer":return(l<((draggable.positionAbs||draggable.position.absolute).left+draggable.clickOffset.left)&&((draggable.positionAbs||draggable.position.absolute).left+draggable.clickOffset.left)<r&&t<((draggable.positionAbs||draggable.position.absolute).top+draggable.clickOffset.top)&&((draggable.positionAbs||draggable.position.absolute).top+draggable.clickOffset.top)<b);
break;
case"touch":return((y1>=t&&y1<=b)||(y2>=t&&y2<=b)||(y1<t&&y2>b))&&((x1>=l&&x1<=r)||(x2>=l&&x2<=r)||(x1<l&&x2>r));
break;
default:return false;
break
}};
$.ui.ddmanager={current:null,droppables:[],prepareOffsets:function(t,e){var m=$.ui.ddmanager.droppables;
var type=e?e.type:null;
for(var i=0;
i<m.length;
i++){if(!m[i].enabledAjaxSwing){if(m[i].item.options.disabled||(t&&!(true||m[i].item.options.accept.call(m[i].item.element,(t.currentItem||t.element))))){continue
}}m[i].offset=$(m[i].item.element).offset();
m[i].item.proportions={width:m[i].item.element.outerWidth(),height:m[i].item.element.outerHeight()};
if(type=="dragstart"){m[i].item.activate.call(m[i].item,e)
}m[i].enabledAjaxSwing=true
}},drop:function(draggable,e){$.each($.ui.ddmanager.droppables,function(){if(!this.item.options.disabled&&$.ui.intersect(draggable,this,this.item.options.tolerance)){this.item.drop.call(this.item,e)
}if(!this.item.options.disabled&&(true||this.item.options.accept.call(this.item.element,(draggable.currentItem||draggable.element)))){this.out=1;
this.over=0;
this.item.deactivate.call(this.item,e)
}})
},drag:function(draggable,e){if(draggable.options.refreshPositions){$.ui.ddmanager.prepareOffsets(draggable,e)
}$.each($.ui.ddmanager.droppables,function(){if(this.item.disabled||this.greedyChild){return 
}var intersects=$.ui.intersect(draggable,this,this.item.options.tolerance);
var c=!intersects&&this.over==1?"out":(intersects&&this.over==0?"over":null);
if(!c){return 
}var instance=$.data(this.item.element[0],"droppable");
if(instance.options.greedy){this.item.element.parents(".ui-droppable:eq(0)").each(function(){var parent=this;
$.each($.ui.ddmanager.droppables,function(){if(this.item.element[0]!=parent){return 
}this[c]=0;
this[c=="out"?"over":"out"]=1;
this.greedyChild=(c=="over"?1:0);
this.item[c=="out"?"over":"out"].call(this.item,e);
return false
})
})
}this[c]=1;
this[c=="out"?"over":"out"]=0;
this.item[c].call(this.item,e)
})
}};
$.ui.plugin.add("droppable","activeClass",{activate:function(e,ui){$(this).addClass(ui.options.activeClass)
},deactivate:function(e,ui){$(this).removeClass(ui.options.activeClass)
},drop:function(e,ui){$(this).removeClass(ui.options.activeClass)
}});
$.ui.plugin.add("droppable","hoverClass",{over:function(e,ui){$(this).addClass(ui.options.hoverClass)
},out:function(e,ui){$(this).removeClass(ui.options.hoverClass)
},drop:function(e,ui){$(this).removeClass(ui.options.hoverClass)
}})
})(jQuery);
var fdTableSort={regExp_Currency:/^[�$���]/,regExp_Number:/^(\-)?[0-9]+(\.[0-9]*)?$/,pos:-1,uniqueHash:1,thNode:null,tableId:null,tableCache:{},tmpCache:{},
/*@cc_on
        /*@if (@_win32)
        colspan:                "colSpan",
        rowspan:                "rowSpan",
        @else @*/
colspan:"colspan",rowspan:"rowspan",
/*@end
        @*/
addEvent:function(obj,type,fn,tmp){tmp||(tmp=true);
if(obj.attachEvent){obj["e"+type+fn]=fn;
obj[type+fn]=function(){obj["e"+type+fn](window.event)
};
obj.attachEvent("on"+type,obj[type+fn])
}else{obj.addEventListener(type,fn,true)
}},removeEvent:function(obj,type,fn,tmp){tmp||(tmp=true);
if(obj.detachEvent){obj.detachEvent("on"+type,obj[type+fn]);
obj[type+fn]=null
}else{obj.removeEventListener(type,fn,true)
}},stopEvent:function(e){e=e||window.event;
if(e.stopPropagation){e.stopPropagation();
e.preventDefault()
}
/*@cc_on@*/
/*@if(@_win32)
                e.cancelBubble = true;
                e.returnValue = false;
                /*@end@*/
return false
},initEvt:function(e){fdTableSort.init(false)
},init:function(tableId){if(!document.getElementsByTagName||!document.createElement||!document.getElementById){return 
}var tables=tableId&&document.getElementById(tableId)?new Array(document.getElementById(tableId)):document.getElementsByTagName("table");
var workArr,sortable,headers,thtext,aclone,a,span,columnNum,noArrow,colCnt,cel,allRowArr,rowArr,sortableTable,celCount,colspan,rowspan,rowLength;
var onLoadTables=[];
a=document.createElement("a");
a.href="#";
a.onkeypress=fdTableSort.keyWrapper;
span=document.createElement("span");
for(var k=0,tbl;
tbl=tables[k];
k++){if(tbl.id){fdTableSort.removeTableCache(tbl.id)
}if(tbl.id){fdTableSort.removeTmpCache(tbl.id)
}allRowArr=tbl.getElementsByTagName("thead").length?tbl.getElementsByTagName("thead")[0].getElementsByTagName("tr"):tbl.getElementsByTagName("tr");
rowArr=[];
sortableTable=false;
for(var i=0,tr;
tr=allRowArr[i];
i++){if(tr.getElementsByTagName("td").length||!tr.getElementsByTagName("th").length){continue
}rowArr[rowArr.length]=tr.getElementsByTagName("th");
for(var j=0,th;
th=rowArr[rowArr.length-1][j];
j++){if(th.className.search(/sortable/)!=-1){sortableTable=true
}}}if(!sortableTable){continue
}if(!tbl.id){tbl.id="fd-table-"+fdTableSort.uniqueHash++
}sortable=false;
columnNum=tbl.className.search(/sortable-onload-([0-9]+)/)!=-1?parseInt(tbl.className.match(/sortable-onload-([0-9]+)/)[1])-1:-1;
showArrow=tbl.className.search(/no-arrow/)==-1;
reverse=tbl.className.search(/sortable-onload-([0-9]+)-reverse/)!=-1;
rowLength=rowArr[0].length;
for(var c=0;
c<rowArr[0].length;
c++){if(rowArr[0][c].getAttribute(fdTableSort.colspan)&&rowArr[0][c].getAttribute(fdTableSort.colspan)>1){rowLength=rowLength+(rowArr[0][c].getAttribute(fdTableSort.colspan)-1)
}}workArr=new Array(rowArr.length);
for(var c=rowArr.length;
c--;
){workArr[c]=new Array(rowLength)
}for(var c=0;
c<workArr.length;
c++){celCount=0;
for(var i=0;
i<rowLength;
i++){if(!workArr[c][i]){cel=rowArr[c][celCount];
colspan=(cel.getAttribute(fdTableSort.colspan)>1)?cel.getAttribute(fdTableSort.colspan):1;
rowspan=(cel.getAttribute(fdTableSort.rowspan)>1)?cel.getAttribute(fdTableSort.rowspan):1;
for(var t=0;
((t<colspan)&&((i+t)<rowLength));
t++){for(var n=0;
((n<rowspan)&&((c+n)<workArr.length));
n++){workArr[(c+n)][(i+t)]=cel
}}if(++celCount==rowArr[c].length){break
}}}}for(var c=0;
c<workArr.length;
c++){for(var i=0;
i<workArr[c].length;
i++){if(workArr[c][i].className.search("fd-column-")==-1&&workArr[c][i].className.search("sortable")!=-1){workArr[c][i].className=workArr[c][i].className+" fd-column-"+i
}if(workArr[c][i].className.match("sortable")){workArr[c][i].className=workArr[c][i].className.replace(/forwardSort|reverseSort/,"");
if(i==columnNum){sortable=workArr[c][i]
}thtext=fdTableSort.getInnerText(workArr[c][i]);
if(workArr[c][i].getElementsByTagName&&workArr[c][i].getElementsByTagName("a").length){workArr[c][i].getElementsByTagName("a")[0].onclick=workArr[c][i].getElementsByTagName("a")[0].onkeypress=null
}while(workArr[c][i].firstChild){workArr[c][i].removeChild(workArr[c][i].firstChild)
}aclone=a.cloneNode(true);
aclone.appendChild(document.createTextNode(thtext));
aclone.title="Sort on \u201c"+thtext+"\u201d";
aclone.onclick=workArr[c][i].onclick=fdTableSort.clickWrapper;
workArr[c][i].appendChild(aclone);
if(showArrow){workArr[c][i].appendChild(span.cloneNode(false))
}workArr[c][i].className=workArr[c][i].className.replace(/fd-identical|fd-not-identical/,"");
fdTableSort.disableSelection(workArr[c][i]);
aclone=null
}}}fdTableSort.tmpCache[tbl.id]={cols:rowLength,headers:workArr};
workArr=null;
if(sortable){onLoadTables[onLoadTables.length]=sortable;
if(reverse){onLoadTables[onLoadTables.length]=sortable
}}}for(var i=0,thNode;
thNode=onLoadTables[i];
i++){fdTableSort.thNode=thNode;
fdTableSort.initSort(false)
}if(a){a.onkeypress=null
}aclone=a=span=workArr=sortable=thNode=onLoadTables=tbl=allRowArr=rowArr=null
},disableSelection:function(element){element.onselectstart=function(){return false
};
element.unselectable="on";
element.style.MozUserSelect="none"
},clickWrapper:function(e){e=e||window.event;
if(fdTableSort.thNode==null){var targ=this;
while(targ.tagName.toLowerCase()!="th"){targ=targ.parentNode
}fdTableSort.thNode=targ;
fdTableSort.addSortActiveClass();
setTimeout(fdTableSort.initSort,5,false)
}return fdTableSort.stopEvent(e)
},keyWrapper:function(e){e=e||window.event;
var kc=e.keyCode!=null?e.keyCode:e.charCode;
if(kc==13){var targ=this;
while(targ.tagName.toLowerCase()!="th"){targ=targ.parentNode
}fdTableSort.thNode=targ;
fdTableSort.addSortActiveClass();
setTimeout(fdTableSort.initSort,5,false);
return fdTableSort.stopEvent(e)
}return true
},jsWrapper:function(tableid,colNum){if(!fdTableSort.tmpCache[tableid]||fdTableSort.tmpCache[tableid].headers[0].length<=colNum||fdTableSort.tmpCache[tableid].headers[0][colNum].className.search(/fd-column/)==-1){return false
}fdTableSort.thNode=fdTableSort.tmpCache[tableid].headers[0][colNum];
fdTableSort.initSort(true)
},addSortActiveClass:function(){if(fdTableSort.thNode==null){return 
}fdTableSort.addClass(fdTableSort.thNode,"sort-active");
fdTableSort.addClass(document.getElementsByTagName("body")[0],"sort-active");
var tableElem=fdTableSort.thNode;
while(tableElem.tagName.toLowerCase()!="table"&&tableElem.parentNode){tableElem=tableElem.parentNode
}if("sortInitiatedCallback-"+tableElem.id in window){window["sortInitiatedCallback-"+tableElem.id]()
}else{if("sortInitiatedCallback" in window){sortInitiatedCallback(tableElem.id)
}}},removeSortActiveClass:function(){fdTableSort.removeClass(fdTableSort.thNode,"sort-active");
fdTableSort.removeClass(document.getElementsByTagName("body")[0],"sort-active");
var tableElem=fdTableSort.thNode;
while(tableElem.tagName.toLowerCase()!="table"&&tableElem.parentNode){tableElem=tableElem.parentNode
}if("sortCompleteCallback-"+tableElem.id in window){window["sortCompleteCallback-"+tableElem.id]()
}else{if("sortCompleteCallback" in window){sortCompleteCallback(tableElem.id)
}}},addClass:function(e,c){if(new RegExp("(^|\\s)"+c+"(\\s|$)").test(e.className)){return 
}e.className+=(e.className?" ":"")+c
},removeClass:function(e,c){e.className=!c?"":e.className.replace(new RegExp("(^|\\s*\\b[^-])"+c+"($|\\b(?=[^-]))","g"),"")
},prepareTableData:function(table){var data=[];
var start=table.getElementsByTagName("tbody");
start=start.length?start[0]:table;
var trs=start.getElementsByTagName("tr");
var ths=table.getElementsByTagName("th");
var numberOfRows=trs.length;
var numberOfCols=fdTableSort.tmpCache[table.id].cols;
var data=[];
var identical=new Array(numberOfCols);
var identVal=new Array(numberOfCols);
for(var tmp=0;
tmp<numberOfCols;
tmp++){identical[tmp]=true
}var tr,td,th,txt,tds,col,row;
var re=new RegExp(/fd-column-([0-9]+)/);
var rowCnt=0;
var sortableColumnNumbers=[];
for(var tmp=0,th;
th=ths[tmp];
tmp++){if(th.className.search(re)==-1){continue
}sortableColumnNumbers[sortableColumnNumbers.length]=th
}for(row=0;
row<numberOfRows;
row++){tr=trs[row];
if(tr.parentNode!=start||tr.getElementsByTagName("th").length||(tr.parentNode.tagName&&tr.parentNode.tagName.toLowerCase()=="tfoot")){continue
}data[rowCnt]=[];
tds=tr.getElementsByTagName("td");
for(var tmp=0,th;
th=sortableColumnNumbers[tmp];
tmp++){col=th.className.match(re)[1];
td=tds[col];
txt=fdTableSort.getInnerText(td)+" ";
txt=txt.replace(/^\s+/,"").replace(/\s+$/,"");
if(th.className.search(/sortable-date/)!=-1){txt=fdTableSort.dateFormat(txt,th.className.search(/sortable-date-dmy/)!=-1)
}else{if(th.className.search(/sortable-numeric|sortable-currency/)!=-1){txt=parseFloat(txt.replace(/[^0-9\.\-]/g,""));
if(isNaN(txt)){txt=""
}}else{if(th.className.search(/sortable-text/)!=-1){txt=txt.toLowerCase()
}else{if(th.className.search(/sortable-keep/)!=-1){txt=rowCnt
}else{if(th.className.search(/sortable-([a-zA-Z\_]+)/)!=-1){if((th.className.match(/sortable-([a-zA-Z\_]+)/)[1]+"PrepareData") in window){txt=window[th.className.match(/sortable-([a-zA-Z\_]+)/)[1]+"PrepareData"](td,txt)
}}else{if(txt!=""){fdTableSort.removeClass(th,"sortable");
if(fdTableSort.dateFormat(txt)!=0){fdTableSort.addClass(th,"sortable-date");
txt=fdTableSort.dateFormat(txt)
}else{if(txt.search(fdTableSort.regExp_Number)!=-1||txt.search(fdTableSort.regExp_Currency)!=-1){fdTableSort.addClass(th,"sortable-numeric");
txt=parseFloat(txt.replace(/[^0-9\.\-]/g,""));
if(isNaN(txt)){txt=""
}}else{fdTableSort.addClass(th,"sortable-text");
txt=txt.toLowerCase()
}}}}}}}}if(rowCnt>0&&identical[col]&&identVal[col]!=txt){identical[col]=false
}identVal[col]=txt;
data[rowCnt][col]=txt
}data[rowCnt][numberOfCols]=tr;
rowCnt++
}var colStyle=table.className.search(/colstyle-([\S]+)/)!=-1?table.className.match(/colstyle-([\S]+)/)[1]:false;
var rowStyle=table.className.search(/rowstyle-([\S]+)/)!=-1?table.className.match(/rowstyle-([\S]+)/)[1]:false;
fdTableSort.tableCache[table.id]={data:data,identical:identical,colStyle:colStyle,rowStyle:rowStyle,noArrow:table.className.search(/no-arrow/)!=-1};
sortableColumnNumbers=data=tr=td=th=trs=identical=identVal=null
},onUnload:function(){for(tbl in fdTableSort.tableCache){fdTableSort.removeTableCache(tbl)
}for(tbl in fdTableSort.tmpCache){fdTableSort.removeTmpCache(tbl)
}fdTableSort.removeEvent(window,"load",fdTableSort.initEvt);
fdTableSort.removeEvent(window,"unload",fdTableSort.onUnload);
fdTableSort.tmpCache=fdTableSort.tableCache=null
},removeTableCache:function(tableId){if(!(tableId in fdTableSort.tableCache)){return 
}var data=fdTableSort.tableCache[tableId].data;
for(var i=0,row;
row=data[i];
i++){row[row.length-1]=null
}data=row=null;
fdTableSort.tableCache[tableId]=null;
delete fdTableSort.tableCache[tableId];
var tbl=document.getElementById(tableId);
if(!tbl){return 
}var ths=tbl.getElementsByTagName("th");
var a;
for(var i=0,th;
th=ths[i];
i++){a=th.getElementsByTagName("a");
if(a.length){a[0].onkeypress=a[0].onclick=null
}th.onclick=th.onselectstart=th=a=null
}},removeTmpCache:function(tableId){if(!(tableId in fdTableSort.tmpCache)){return 
}var headers=fdTableSort.tmpCache[tableId].headers;
var a;
for(var i=0,row;
row=headers[i];
i++){for(var j=0,th;
th=row[j];
j++){a=th.getElementsByTagName("a");
if(a.length){a[0].onkeypress=a[0].onclick=null
}th.onclick=th.onselectstart=th=a=null
}}fdTableSort.tmpCache[tableId]=null;
delete fdTableSort.tmpCache[tableId]
},initSort:function(noCallback){var span;
var thNode=fdTableSort.thNode;
var tableElem=fdTableSort.thNode;
while(tableElem.tagName.toLowerCase()!="table"&&tableElem.parentNode){tableElem=tableElem.parentNode
}if(!tableElem.id||!(tableElem.id in fdTableSort.tableCache)){fdTableSort.prepareTableData(tableElem)
}fdTableSort.tableId=tableElem.id;
fdTableSort.pos=thNode.className.match(/fd-column-([0-9]+)/)[1];
var dataObj=fdTableSort.tableCache[tableElem.id];
var lastPos=dataObj.pos?dataObj.pos.className.match(/fd-column-([0-9]+)/)[1]:-1;
var data=dataObj.data;
var colStyle=dataObj.colStyle;
var rowStyle=dataObj.rowStyle;
var len1=data.length;
var len2=data.length>0?data[0].length-1:0;
var identical=dataObj.identical[fdTableSort.pos];
var noArrow=dataObj.noArrow;
if(lastPos!=fdTableSort.pos&&lastPos!=-1){var th=dataObj.pos;
fdTableSort.removeClass(th,"(forwardSort|reverseSort)");
if(!noArrow){span=th.getElementsByTagName("span")[0];
while(span.firstChild){span.removeChild(span.firstChild)
}}}var classToAdd="forwardSort";
if((lastPos==fdTableSort.pos&&!identical)||(thNode.className.search(/sortable-keep/)!=-1&&lastPos==-1)){data.reverse();
classToAdd=thNode.className.search(/reverseSort/)!=-1?"forwardSort":"reverseSort";
if(thNode.className.search(/sortable-keep/)!=-1&&lastPos==-1){fdTableSort.tableCache[tableElem.id].pos=thNode
}}else{fdTableSort.tableCache[tableElem.id].pos=thNode;
if(!identical){if(thNode.className.match(/sortable-(numeric|currency|date|keep)/)){data.sort(fdTableSort.sortNumeric)
}else{if(thNode.className.match("sortable-text")){data.sort(fdTableSort.sortText)
}else{if(thNode.className.search(/sortable-([a-zA-Z\_]+)/)!=-1&&thNode.className.match(/sortable-([a-zA-Z\_]+)/)[1] in window){data.sort(window[thNode.className.match(/sortable-([a-zA-Z\_]+)/)[1]])
}}}}}fdTableSort.removeClass(thNode,"(forwardSort|reverseSort)");
fdTableSort.addClass(thNode,classToAdd);
var webCreamTableId="";
if(tableElem.id){webCreamTableId=tableElem.id.substring(0,(tableElem.id.length-5))
}if(document.getElementById("srtIdx_"+webCreamTableId+"_hidden")!=null){document.getElementById("srtIdx_"+webCreamTableId+"_hidden").value=fdTableSort.pos
}if(document.getElementById("srtCls_"+webCreamTableId+"_hidden")!=null){document.getElementById("srtCls_"+webCreamTableId+"_hidden").value=classToAdd
}if(!noArrow){var arrow=thNode.className.search(/forwardSort/)!=-1?" \u2193":" \u2191";
span=thNode.getElementsByTagName("span")[0];
while(span.firstChild){span.removeChild(span.firstChild)
}span.appendChild(document.createTextNode(arrow))
}if(!rowStyle&&!colStyle&&identical){if(!noCallback){fdTableSort.removeSortActiveClass()
}fdTableSort.thNode=null;
return 
}var hook=tableElem.getElementsByTagName("tbody");
hook=hook.length?hook[0]:tableElem;
var tr,tds;
var rowReg=rowStyle?new RegExp("(^|\\s*\\b[^-])"+rowStyle+"($|\\b(?=[^-]))","g"):false;
var colReg=colStyle?new RegExp("(^|\\s*\\b[^-])"+colStyle+"($|\\b(?=[^-]))","g"):false;
for(var i=0;
i<len1;
i++){tr=data[i][len2];
if(colStyle){tds=tr.getElementsByTagName("td");
if(lastPos!=-1){tds[lastPos].className=tds[lastPos].className.replace(colReg,"")
}fdTableSort.addClass(tds[fdTableSort.pos],colStyle);
tds=null
}if(!identical){if(rowStyle){if(i%2){fdTableSort.addClass(tr,rowStyle)
}else{tr.className=tr.className.replace(rowReg,"")
}}hook.removeChild(tr);
hook.appendChild(tr)
}tr=null
}if(!noCallback){fdTableSort.removeSortActiveClass()
}fdTableSort.thNode=hook=null
},getInnerText:function(el){if(typeof el=="string"||typeof el=="undefined"){return el
}if(el.innerText){return el.innerText
}if(el.tagName){if(el.tagName=="SELECT"){if(el.selectedIndex!=-1){return fdTableSort.getInnerText(el.options[el.selectedIndex])
}else{return fdTableSort.getInnerText(el.options[0])
}}else{if(el.tagName=="INPUT"&&el.type=="text"){return el.value
}}}var txt="",i;
for(i=el.firstChild;
i;
i=i.nextSibling){if(i.nodeType==3){txt+=i.nodeValue
}else{if(i.nodeType==1){txt+=fdTableSort.getInnerText(i)
}}}return txt
},dateFormat:function(dateIn,favourDMY){var dateTest=[{regExp:/^(0?[1-9]|1[012])([- \/.])(0?[1-9]|[12][0-9]|3[01])([- \/.])((\d\d)?\d\d)$/,d:3,m:1,y:5},{regExp:/^(0?[1-9]|[12][0-9]|3[01])([- \/.])(0?[1-9]|1[012])([- \/.])((\d\d)?\d\d)$/,d:1,m:3,y:5},{regExp:/^(\d\d\d\d)([- \/.])(0?[1-9]|1[012])([- \/.])(0?[1-9]|[12][0-9]|3[01])$/,d:5,m:3,y:1}];
var start;
var cnt=0;
var numFormats=dateTest.length;
while(cnt<numFormats){start=(cnt+(favourDMY?numFormats+1:numFormats))%numFormats;
if(dateIn.match(dateTest[start].regExp)){res=dateIn.match(dateTest[start].regExp);
y=res[dateTest[start].y];
m=res[dateTest[start].m];
d=res[dateTest[start].d];
if(m.length==1){m="0"+String(m)
}if(d.length==1){d="0"+String(d)
}if(y.length!=4){y=(parseInt(y)<50)?"20"+String(y):"19"+String(y)
}return y+String(m)+d
}cnt++
}return 0
},sortDate:function(a,b){var aa=a[fdTableSort.pos];
var bb=b[fdTableSort.pos];
return aa-bb
},sortNumeric:function(a,b){var aa=a[fdTableSort.pos];
var bb=b[fdTableSort.pos];
if(aa==bb){return 0
}if(aa===""&&!isNaN(bb)){return -1
}if(bb===""&&!isNaN(aa)){return 1
}return aa-bb
},sortText:function(a,b){var aa=a[fdTableSort.pos];
var bb=b[fdTableSort.pos];
if(aa==bb){return 0
}if(aa<bb){return -1
}return 1
}};
fdTableSort.addEvent(window,"load",fdTableSort.initEvt);
fdTableSort.addEvent(window,"unload",fdTableSort.onUnload);
var TblId,StartRow,SearchFlt,ModFn,ModFnId;
var ExactMatchId,ExactMatch,isPageChanged;
TblId=new Array(),StartRow=new Array();
ModFn=new Array(),ModFnId=new Array();
ExactMatchId=new Array(),ExactMatch=new Array();
SlcArgs=new Array(),isPageChanged=false;
function setFilterGrid(id){var tbl=document.getElementById(id);
var ref_row,fObj;
if(tbl!=null&&tbl.nodeName.toLowerCase()=="table"&&typeof tbl.filterInserted=="undefined"){tbl.filterInserted=true;
TblId.push(id);
if(arguments.length>1){for(var i=0;
i<arguments.length;
i++){var argtype=typeof arguments[i];
switch(argtype.toLowerCase()){case"number":ref_row=arguments[i];
break;
case"object":fObj=arguments[i];
break
}}}ref_row==undefined?StartRow.push(2):StartRow.push(ref_row+2);
var ncells=getCellsNb(id,ref_row);
AddRow(id,tbl,ncells,fObj)
}}function AddRow(id,t,n,f){var fltrow=t.insertRow(0);
var inpclass,displayBtn,btntext,enterkey;
var modfilter_fn,display_allText,on_slcChange;
var displaynrows,totrows_text,btnreset,btnreset_text;
var sort_slc,displayPaging,pagingLength,displayLoader;
var load_text,exactMatch;
var tableId=id.substring(0,(id.length-5));
f!=undefined&&f.btn==false?displayBtn=false:displayBtn=true;
f!=undefined&&f.btn_text!=undefined?btntext=f.btn_text:btntext="go";
f!=undefined&&f.enter_key==false?enterkey=false:enterkey=true;
f!=undefined&&f.mod_filter_fn?modfilter_fn=true:modfilter_fn=false;
f!=undefined&&f.display_all_text!=undefined?display_allText=f.display_all_text:display_allText="";
f!=undefined&&f.on_change==true?on_slcChange=true:on_slcChange=false;
f!=undefined&&f.display_nrows==true?displaynrows=true:displaynrows=false;
f!=undefined&&f.nrows_text!=undefined?totrows_text=f.nrows_text:totrows_text="Data rows: ";
f!=undefined&&f.btn_reset==true?btnreset=true:btnreset=false;
f!=undefined&&f.btn_reset_text!=undefined?btnreset_text=f.btn_reset_text:btnreset_text="Reset";
f!=undefined&&f.sort_select==true?sort_slc=true:sort_slc=false;
f!=undefined&&f.paging==true?displayPaging=true:displayPaging=false;
f!=undefined&&f.paging_length!=undefined?pagingLength=f.paging_length:pagingLength=10;
f!=undefined&&f.loader==true?displayLoader=true:displayLoader=false;
f!=undefined&&f.loader_text!=undefined?load_text=f.loader_text:load_text="Loading...";
f!=undefined&&f.exact_match==true?exactMatch=true:exactMatch=false;
if(modfilter_fn){ModFnId.push(id);
ModFn.push(f.mod_filter_fn)
}if(exactMatch){ExactMatchId.push(id);
ExactMatch.push(exactMatch)
}if(displaynrows||btnreset||displayPaging||displayLoader){var infdiv=document.createElement("div");
infdiv.setAttribute("id","inf_"+id);
infdiv.className="inf";
t.parentNode.insertBefore(infdiv,t);
if(displaynrows){var totrows;
var ldiv=document.createElement("div");
ldiv.setAttribute("id","ldiv_"+id);
displaynrows?ldiv.className="ldiv":ldiv.style.display="none";
displayPaging?totrows=pagingLength:totrows=getRowsNb(id);
var totrows_span=document.createElement("span");
totrows_span.setAttribute("id","totrows_span_"+id);
totrows_span.className="tot";
totrows_span.appendChild(document.createTextNode(totrows));
var totrows_txt=document.createTextNode(totrows_text);
ldiv.appendChild(totrows_txt);
ldiv.appendChild(totrows_span);
infdiv.appendChild(ldiv)
}if(displayLoader){var loaddiv=document.createElement("div");
loaddiv.setAttribute("id","load_"+id);
loaddiv.className="loader";
loaddiv.style.display="none";
loaddiv.appendChild(document.createTextNode(load_text));
infdiv.appendChild(loaddiv)
}if(displayPaging){var mdiv=document.createElement("div");
mdiv.setAttribute("id","mdiv_"+id);
displayPaging?mdiv.className="mdiv":mdiv.style.display="none";
infdiv.appendChild(mdiv);
var start_row=getStartRow(id);
var nrows=t.getElementsByTagName("tr").length;
var npages=Math.ceil((nrows-start_row)/pagingLength);
var slcPages=document.createElement("select");
slcPages.setAttribute("id","slcPages_"+id);
slcPages.onchange=function(){isPageChanged=true;
if(displayLoader){showLoader(id,"")
}GroupByPage(id,pagingLength,this.value);
if(displayLoader){showLoader(id,"none")
}};
document.getElementById("mdiv_"+id).appendChild(document.createTextNode(" Page "));
document.getElementById("mdiv_"+id).appendChild(slcPages);
document.getElementById("mdiv_"+id).appendChild(document.createTextNode(" of "+npages+" "));
for(var t=0;
t<npages;
t++){var currOpt=new Option((t+1),t*pagingLength,false,false);
document.getElementById("slcPages_"+id).options[t]=currOpt
}GroupByPage(id,pagingLength,0);
if(displayLoader){showLoader(id,"none")
}}if(btnreset){var rdiv=document.createElement("div");
rdiv.setAttribute("id","reset_"+id);
btnreset?rdiv.className="rdiv":rdiv.style.display="none";
var fltreset=document.createElement("a");
fltreset.setAttribute("href","javascript:clearFilters('"+id+"');Filter('"+id+"','"+exactMatch+"');");
fltreset.appendChild(document.createTextNode(btnreset_text));
rdiv.appendChild(fltreset);
infdiv.appendChild(rdiv)
}}for(var i=0;
i<n;
i++){var fltcell=fltrow.insertCell(i);
fltcell.noWrap=true;
i==n-1&&displayBtn==true?style="width:80%":style="width:100%";
style+="; padding:1px 0";
if(f==undefined||f["col_"+i]==undefined||f["col_"+i]=="none"){var inp=document.createElement("input");
inp.setAttribute("id","flt"+i+"_"+tableId);
if(f==undefined||f["col_"+i]==undefined){inp.setAttribute("type","text")
}else{inp.setAttribute("type","hidden")
}inp.className="text filterText";
var width=getTableColumnWidth(t,i,n);
$(inp).css("width",width);
if(document.getElementById("flt"+i+"_"+tableId+"_hidden")!=null){inp.setAttribute("value",document.getElementById("flt"+i+"_"+tableId+"_hidden").value)
}fltcell.appendChild(inp);
if(enterkey){inp.onkeyup=DetectKey
}}else{if(f["col_"+i]=="select"){var slc=document.createElement("select");
slc.setAttribute("id","flt"+i+"_"+tableId);
slc.className="choice";
var width=getTableColumnWidth(t,i,n);
$(slc).css("width",width);
fltcell.appendChild(slc);
PopulateOptions(id,i,n,display_allText,sort_slc,displayPaging);
if(displayPaging){var args=new Array();
args.push(id);
args.push(i);
args.push(n);
args.push(display_allText);
args.push(sort_slc);
args.push(displayPaging);
SlcArgs.push(args)
}if(enterkey){slc.onkeypress=DetectKey
}if(on_slcChange){(!modfilter_fn)?slc.onchange=function(){Filter(id,exactMatch)
}:slc.onchange=f.mod_filter_fn
}}}if(i==n-1&&displayBtn==true){var btn=document.createElement("input");
btn.setAttribute("id","btn"+i+"_"+id);
btn.setAttribute("type","button");
btn.setAttribute("value",btntext);
btn.className="button";
fltcell.appendChild(btn);
(!modfilter_fn)?btn.onclick=function(){Filter(id,exactMatch)
}:btn.onclick=f.mod_filter_fn
}}Filter(id,exactMatch)
}function PopulateOptions(id,cellIndex,ncells,opt0txt,sort_opts,paging){var t=document.getElementById(id);
var start_row=getStartRow(id);
var row=t.getElementsByTagName("tr");
var OptArray=new Array();
var optIndex=0;
var currOpt=new Option(opt0txt,"",false,false);
var tableId=id.substring(0,(id.length-5));
document.getElementById("flt"+cellIndex+"_"+tableId).options[optIndex]=currOpt;
var tableId=id.substring(0,(id.length-5));
for(var k=start_row;
k<row.length;
k++){var cell=getChildElms(row[k]).childNodes;
var nchilds=cell.length;
var isPaged=row[k].getAttribute("paging");
if(nchilds==ncells){for(var j=0;
j<nchilds;
j++){if(cellIndex==j){var cell_data=getCellText(cell[j]);
var isMatched=false;
for(w in OptArray){if(cell_data==OptArray[w]){isMatched=true
}}if(!isMatched&&!paging){OptArray.push(cell_data)
}else{if(!isMatched&&paging&&isPaged=="true"){OptArray.push(cell_data)
}}}}}}if(sort_opts){OptArray.sort()
}for(y in OptArray){optIndex++;
optionSelected=false;
if(document.getElementById("flt"+cellIndex+"_"+tableId+"_hidden")!=null&&document.getElementById("flt"+cellIndex+"_"+tableId+"_hidden").value==OptArray[y].toLowerCase()){optionSelected=true
}var currOpt=new Option(OptArray[y],OptArray[y],false,optionSelected);
document.getElementById("flt"+cellIndex+"_"+tableId).options[optIndex]=currOpt
}}function Filter(id,ematch){showLoader(id,"");
getFilters(id);
var t=document.getElementById(id);
var SearchArgs=new Array();
var ncells=getCellsNb(id);
var totrows=getRowsNb(id),hiddenrows=0;
for(var i=0;
i<SearchFlt.length;
i++){filterValue=(document.getElementById(SearchFlt[i]).value).toLowerCase();
var searchFilterInput=document.getElementById(SearchFlt[i]+"_hidden");
if(searchFilterInput){searchFilterInput.value=filterValue
}SearchArgs.push(filterValue)
}var start_row=getStartRow(id);
var row=t.getElementsByTagName("tr");
for(var k=start_row;
k<row.length;
k++){if(row[k].style.display=="none"){row[k].style.display="";
showRowColumns(row[k])
}var cell=getChildElms(row[k]).childNodes;
var nchilds=cell.length;
if(nchilds==ncells){var cell_value=new Array();
var occurence=new Array();
var isRowValid=true;
for(var j=0;
j<nchilds;
j++){var cell_data=getCellText(cell[j]).toLowerCase();
var filter_ele=document.getElementById(SearchFlt[j]);
cell_value.push(cell_data);
if(SearchArgs[j]!=""){var num_cell_data=parseFloat(cell_data);
if(/<=/.test(SearchArgs[j])&&!isNaN(num_cell_data)){num_cell_data<=parseFloat(SearchArgs[j].replace(/<=/,""))?occurence[j]=true:occurence[j]=false
}else{if(/>=/.test(SearchArgs[j])&&!isNaN(num_cell_data)){num_cell_data>=parseFloat(SearchArgs[j].replace(/>=/,""))?occurence[j]=true:occurence[j]=false
}else{if(/</.test(SearchArgs[j])&&!isNaN(num_cell_data)){num_cell_data<parseFloat(SearchArgs[j].replace(/</,""))?occurence[j]=true:occurence[j]=false
}else{if(/>/.test(SearchArgs[j])&&!isNaN(num_cell_data)){num_cell_data>parseFloat(SearchArgs[j].replace(/>/,""))?occurence[j]=true:occurence[j]=false
}else{if(filter_ele.nodeName=="SELECT"){occurence[j]=(cell_data==SearchArgs[j])
}else{var regexp;
if(ematch){regexp=new RegExp("(^)"+SearchArgs[j]+"($)","gi")
}else{regexp=new RegExp(SearchArgs[j],"gi")
}occurence[j]=regexp.test(cell_data)
}}}}}}}for(var t=0;
t<ncells;
t++){if(SearchArgs[t]!=""&&!occurence[t]){isRowValid=false
}}}var isPaged=row[k].getAttribute("paging");
if(isPaged!=null&&isPaged!=""){if(!isRowValid&&isPaged=="false"){row[k].style.display="none";
hideRowColumns(row[k]);
hiddenrows++
}else{if(isRowValid&&isPaged=="true"){row[k].style.display="";
showRowColumns(row[k])
}else{if(isRowValid&&isPaged=="false"){row[k].style.display="none";
hideRowColumns(row[k]);
hiddenrows++
}else{row[k].style.display="none";
hideRowColumns(row[k]);
hiddenrows++
}}}}else{if(!isRowValid){row[k].style.display="none";
hideRowColumns(row[k]);
hiddenrows++
}else{row[k].style.display="";
showRowColumns(row[k])
}}}showTotRowsN(id,parseInt(totrows-hiddenrows));
showLoader(id,"none")
}function showRowColumns(rowObject){cols=rowObject.getElementsByTagName("td");
for(l=0;
l<cols.length;
l++){cols[l].style.display=""
}}function hideRowColumns(rowObject){cols=rowObject.getElementsByTagName("td");
for(l=0;
l<cols.length;
l++){cols[l].style.display="none"
}}function GroupByPage(id,pagelength,start){var t=document.getElementById(id);
var start_row=parseInt(getStartRow(id));
var paging_start_row=start_row+parseInt(start);
var row=t.getElementsByTagName("tr");
var nrows=0,counter=paging_start_row;
var param1,param2,param3,param4,param5,param6;
for(var j=start_row;
j<row.length;
j++){row[j].setAttribute("paging","false");
row[j].style.display="none"
}for(var k=paging_start_row;
k<row.length;
k++){counter++;
if(counter<=paging_start_row+pagelength){row[k].setAttribute("paging","true");
row[k].style.display="";
nrows++
}}if(isPageChanged){getFilters(id);
for(var i=0;
i<SlcArgs.length;
i++){if(SlcArgs[i][0]==id){var cSlc;
for(var u=0;
u<SearchFlt.length;
u++){cSlc=document.getElementById(SearchFlt[u]);
if(cSlc.nodeName.toLowerCase()=="select"&&u==i){for(t=cSlc.length-1;
t>=0;
t--){cSlc.remove(t)
}}}param1=SlcArgs[i][0],param2=SlcArgs[i][1];
param3=SlcArgs[i][2],param4=SlcArgs[i][3];
param5=SlcArgs[i][4],param6=SlcArgs[i][5];
PopulateOptions(param1,param2,param3,param4,param5,param6)
}}isPageChanged=false
}if(start>0){showTotRowsN(id,parseInt(nrows))
}}function getCellsNb(id,nrow){var t=document.getElementById(id);
var tr;
if(nrow==undefined){tr=t.getElementsByTagName("tr")[0]
}else{tr=t.getElementsByTagName("tr")[nrow]
}var n=getChildElms(tr);
return n.childNodes.length
}function getRowsNb(id){var t=document.getElementById(id);
var s=getStartRow(id);
var ntrs=t.getElementsByTagName("tr").length;
return parseInt(ntrs-s)
}function getFilters(id){SearchFlt=new Array();
var t=document.getElementById(id);
var tr=t.getElementsByTagName("tr")[0];
var enfants=tr.childNodes;
for(var i=0;
i<enfants.length;
i++){SearchFlt.push(enfants[i].firstChild.getAttribute("id"))
}}function getStartRow(id){var r;
var tableId=id;
for(j in TblId){if(TblId[j]==tableId){r=StartRow[j]
}}return r
}function clearFilters(id){getFilters(id);
for(i in SearchFlt){document.getElementById(SearchFlt[i]).value=""
}}function showLoader(id,p){var loaderdiv=document.getElementById("load_"+id);
if(loaderdiv!=null&&p=="none"){setTimeout("document.getElementById('load_"+id+"').style.display = '"+p+"'",250)
}else{if(loaderdiv!=null&&p!="none"){loaderdiv.style.display=p
}}}function showTotRowsN(id,p){var totrows_displayer=document.getElementById("totrows_span_"+id);
if(totrows_displayer!=null&&totrows_displayer.nodeName.toLowerCase()=="span"){totrows_displayer.innerHTML=p
}}function getChildElms(n){if(n.nodeType==1){var enfants=n.childNodes;
for(var i=0;
i<enfants.length;
i++){var child=enfants[i];
if(child.nodeType==3){n.removeChild(child)
}}return n
}}String.prototype.trim=function(){a=this.replace(/^\s+/,"");
return a.replace(/\s+$/,"")
};
function getCellText(n){var s="";
var enfants=n.childNodes;
for(var i=0;
i<enfants.length;
i++){var child=enfants[i];
if(child.nodeType==3){s+=child.data
}else{if(child.nodeName=="INPUT"){s+=child.getAttribute("value")
}else{if(child.nodeName=="SELECT"){s+=child.options[child.selectedIndex].text
}else{s+=getCellText(child)
}}}}s=s.trim();
return s
}function DetectKey(e){var evt=(e)?e:(window.event)?window.event:null;
if(evt){var key=(evt.charCode)?evt.charCode:((evt.keyCode)?evt.keyCode:((evt.which)?evt.which:0));
if(key=="13"){var cid,leftstr,tblid,CallFn,Match;
cid=this.getAttribute("id");
leftstr=this.getAttribute("id").split("_")[0];
tblid=cid.substring(leftstr.length+1,cid.length);
tblid+="Table";
for(i in ModFn){ModFnId[i]==tblid?CallFn=true:CallFn=false
}for(j in ExactMatchId){if(ExactMatchId[j]==tblid){Match=ExactMatch[j]
}}(CallFn)?ModFn[i].call():Filter(tblid,Match)
}}}var rowmarker="";
var colorsave="";
function rowmarkerinit(){rowmarker=""
}function wa_rowcolor(id,action,key){if(action=="over"){if(id!=rowmarker){document.getElementById(id).bgColor="#e0e0e0"
}}if(action=="out"){if(id!=rowmarker){document.getElementById(id).bgColor="#eeeeee"
}}if(action=="click"){if(rowmarker!=""){document.getElementById(rowmarker).bgColor="#eeeeee"
}rowmarker=id;
document.getElementById(id).bgColor="#CEBD9C"
}}function getTableColumnWidth(t,i,n){var width=(i<n-1)?"100%":"96%";
var colgroup=t.childNodes[1];
if(typeof (colgroup)!="undefined"&&colgroup.nodeName=="COLGROUP"){var colIndex=i*2+1;
if(colgroup.childNodes[colIndex]&&colgroup.childNodes[colIndex].width){width=colgroup.childNodes[colIndex].width+"px";
if($.browser.mozilla){width-=2
}}}return width
}var ajaxSwingUpdateSeconds=0;
var ajaxSwingUpdateInterval=null;
var ajaxSwingSessionExpirationInterval=null;
var ajaxSwingKeepSessionAlive=false;
var ajaxSwingSubmitAction=null;
var ajaxSwingPingActive=false;
var ajaxSwingFocusBackElement=null;
var ajaxSwingSubmitFocusBackElement=null;
var AjaxSwing_oldFocusedComponentId=null;
var AjaxSwing_focusedComponentId=null;
var AjaxSwingDoNotHidePopups=false;
var ajaxSwingQueueEvent=false;
var AjaxSwingActiveFastSubmits=0;
var AjaxSwingPingProgressBarInterval=null;
var ajaxSwingSubmittingTableSelection=false;
window.ajaxSwing=new Object();
$(document).ready(function(){if(typeof ajaxswingDisableAjax=="undefined"||ajaxswingDisableAjax==false){$(document.AjaxSwingForm).ajaxForm()
}ajaxSwing.firstClick=true;
$(document).mousedown(onDocumentMouseDown);
$(document).click(onDocumentClick);
$(document).keydown(onDocumentKeyDown);
if(document.AjaxSwingForm){$(document.AjaxSwingForm).bind("cut",onDocumentClick);
$(document.AjaxSwingForm).bind("paste",onDocumentClick)
}onerror=handleJavascriptError;
checkWindowSize();
initializeDragAndDrop()
});
function doSubmit(action,blockUI,extraData){if(ajaxSwingSubmitAction!=null){if(ajaxSwingSubmittingTableSelection||ajaxSwingQueueEvent){ajaxSwingQueueEvent=false;
setTimeout(function(){ajaxSwingQueueEvent=true;
doSubmit(action,blockUI,extraData)
},300)
}return false
}ajaxSwingQueueEvent=false;
if(ajaxSwingSubmittingTableSelection){ajaxSwingSubmittingTableSelection=false;
setTimeout(function(){AjaxSwingDoNotHidePopups=true;
doSubmit(action,blockUI,extraData);
AjaxSwingDoNotHidePopups=false;
ajaxSwingSubmittingTableSelection=true
},200);
return true
}ajaxSwingSubmitAction=action;
if(typeof (extraData)!="undefined"){if(!extraData.keepSessionAlive){ajaxSwingSubmitAction="keepSessionAliveFalse"
}}if(typeof blockUI=="undefined"){blockUI=true
}if(action!="update"){if(blockUI!="false!important"){blockUI=true
}else{blockUI=false
}}else{blockUI=false;
AjaxSwing_oldFocusedComponentId=AjaxSwing_focusedComponentId
}ajaxSwingSubmitFocusBackElement=ajaxSwingFocusBackElement;
if(ajaxSwingFocusBackElement){blockUI=false
}if(action){document.AjaxSwingForm.__Action.value=action
}customOnSubmit();
ajaxSwingOnSubmit();
$("*").each(function(){if(this.AJS_onSubmit){this.AJS_onSubmit()
}});
if(typeof ajaxswingDisableAjax!="undefined"&&ajaxswingDisableAjax){document.AjaxSwingForm.submit()
}else{if(action.length>8&&action.substring(0,8)=="/upload/"){document.AjaxSwingForm.submit()
}else{document.AjaxSwingForm.__Ajax.value="true";
$(document.AjaxSwingForm).ajaxSubmit({success:processResponse,complete:onSubmitAjaxComplete,contentType:"application/x-www-form-urlencoded; charset=utf-8",data:extraData})
}}preSubmitAjaxSwingWindows(blockUI);
return false
}function doFastSubmit(action,extraData,extraOnComplete){if(ajaxSwingSubmitAction!=null){return false
}if(action!="ping"){ajaxSwingSubmitAction=action
}var data={};
var onComplete=onFastAjaxComplete;
if(extraOnComplete){onComplete=extraOnComplete
}if(extraData){data=extraData
}data.__Action=action;
data.__FastSubmit=true;
var options={};
options.url=getSubmitURL();
options.type="POST";
options.data=data;
options.global=false;
options.success=processResponse;
options.complete=onComplete;
if(onComplete==onFastAjaxComplete){}AjaxSwingActiveFastSubmits=AjaxSwingActiveFastSubmits+1;
$.ajax(options)
}function doNonAjaxSubmit(action){ajaxSwingPingActive=true;
setUpdateInterval(0);
ajaxswingDisableAjax=true;
doSubmit(action)
}function ajaxSwingOnSubmit(){preSubmitAjaxSwingTrees();
if(activeButton!=null){if((!ajaxSwingPingActive)&&(!AjaxSwingDoNotHidePopups)){deactivateButton(activeButton)
}}}function onFastAjaxComplete(xhr,statusText){AjaxSwingActiveFastSubmits=AjaxSwingActiveFastSubmits-1
}function onPingAjaxComplete(xhr,statusText){onAjaxComplete(xhr,statusText);
ajaxSwingPingActive=false
}function onSubmitAjaxComplete(xhr,statusText){onAjaxComplete(xhr,statusText);
if(ajaxSwingSubmitAction!="keepSessionAliveFalse"){resetSessionExpirationInterval()
}document.AjaxSwingForm.__Ajax.value="false";
document.AjaxSwingForm.__HasClientChanges.value="false";
ajaxSwingSubmitAction=null;
if(ajaxSwingCurrentCellEditElement!=null){enableCellEditor(ajaxSwingCurrentCellEditElement.cellEditorId)
}if(ajaxSwingPopupMenuClear){ajaxSwingPopupMenuLeft=-1;
ajaxSwingPopupMenuTop=-1
}ajaxSwingPopupMenuClear=true;
ajaxSwingSubmittingTableSelection=false;
ajaxSwingPingActive=false
}function onAjaxComplete(xhr,statusText){if(statusText!="success"){setUpdateInterval(0);
if(ajaxSwingSubmitAction==null){doSubmit("update")
}else{if(typeof ajaxswingDisableAjax=="undefined"||ajaxswingDisableAjax==false){ajaxSwingPingActive=true;
var errorMessage="Server communication error\n";
errorMessage+="(xhr.status:"+xhr.status+", xhr.statusText:"+xhr.statusText+", statusText:"+statusText+")";
errorMessage+="\n\nWould you like to refresh the page?";
if(confirm(errorMessage)){ajaxSwingPingActive=false;
window.location=window.location
}else{postSubmitAjaxSwingWindows()
}ajaxSwingPingActive=false
}}}}function processResponse(data,statusText){if(typeof ajaxswingDisableAjax!="undefined"&&ajaxswingDisableAjax==true){return 
}if(statusText=="success"){if(data.length>1&&data.substring(0,17)=="{skipUpdate:true}"){finishScrollAjaxRequest();
document.AjaxSwingForm.__Action.value="";
ajaxSwingSubmitAction=null;
return 
}if(data.length>1&&data.substring(0,1)=="{"){if(typeof ajaxswingDisableAjax=="undefined"||ajaxswingDisableAjax==false){data=eval("data = ("+data+")");
processJsonResponse(data)
}}else{refreshPage();
return 
}}customOnAjaxComplete(data);
initializeDragAndDrop();
document.AjaxSwingForm.__Action.value="";
ajaxSwingSubmitAction=null;
if(!AjaxSwingPingProgressBarInterval){AjaxSwingPingProgressBarInterval=setInterval("pingProgressBar()",10000)
}if(AjaxSwingCurrentlyVisibleAjaxLoading){AjaxSwingCurrentlyVisibleAjaxLoading.style.backgroundImage="none"
}}function pingProgressBar(){if($(".indeterminate-bar").size()>0){pingSession(ajaxSwingKeepSessionAlive)
}}function removeElement(element){var parent=element.parentNode;
detachJavaScript(element);
if($.browser.msie){var garbageBin=document.getElementById("IELeakGarbageBin");
if(!garbageBin){garbageBin=document.createElement("DIV");
garbageBin.id="IELeakGarbageBin";
garbageBin.style.display="none";
document.body.appendChild(garbageBin)
}garbageBin.appendChild(element);
garbageBin.innerHTML=""
}else{parent.removeChild(element)
}return parent
}function detachJavaScript(element){if(element.AJS_onRemove){element.AJS_onRemove();
element.AJS_onRemove=null
}if(element.AJS_onSubmit){element.AJS_onSubmit=null
}if(element.childNodes&&element.nodeName!="SELECT"){var children=element.childNodes;
for(var i=0;
i<children.length;
i++){detachJavaScript(children[i])
}}}function processJsonResponse(data){var __curTime=new Date().getTime();
if(data.requestId!=0){document.getElementById("__RequestId").setAttribute("value",data.requestId)
}setUpdateInterval(data.updateInterval);
if(data.update==true){var request={};
request.keepSessionAlive=ajaxSwingKeepSessionAlive;
doSubmit("update",true,request);
ajaxSwingAjaxScrollCurrentRequest=null;
return 
}var state,parent,element;
var elementNotExistArray=new Array();
var removeTempScroll=false;
for(var i=0;
i<data.actionableComponentStates.length;
i++){state=data.actionableComponentStates[i];
if(state.componentId!=null){element=document.getElementById(state.componentId);
if(!element||element==null){elementNotExistArray[state.componentId]="1"
}}}for(var i=0;
i<data.actionableComponentStates.length;
i++){state=data.actionableComponentStates[i];
if(ajaxSwingFocusBackElement&&(state.componentId==ajaxSwingFocusBackElement.id)){if(state.html&&(state.html.indexOf("forcedText")>0)){ajaxSwingFocusBackElement=null
}}var savedMenuStyle=null;
if((activeButton!=null)&&(activeButton.menu!=null)&&(activeButton.menu.id==state.componentId)){savedMenuStyle=activeButton.menu.style
}if((!ajaxSwingFocusBackElement)||(state.componentId!=ajaxSwingFocusBackElement.id)){parent=null;
if(state.action==1||state.action==3){element=document.getElementById(state.componentId);
if((!element||element==null)){if(state.action==1){if(elementNotExistArray[state.componentId]=="1"){if(!data.ignoreClientErrors){}else{refreshPage();
return 
}}}}else{if(element.getAttribute("tree")){removeTree(element.id)
}var scrollParentElement=document.getElementById(state.parentId+"Inner");
if(scrollParentElement){var wr=scrollParentElement.scrollWidth;
var hr=scrollParentElement.scrollHeight;
$(scrollParentElement).append('<div class="tempScrollHolder" style="position:absolute;left:0px;top:0px;width:'+wr+"px;height:"+hr+'px">&nbsp</div>');
removeTempScroll=true
}parent=removeElement(element)
}}if(state.action==2||state.action==3){if(state.action==2||parent==null){parent=document.getElementById(state.parentId)
}if(!parent||parent==null){if(true||!data.ignoreClientErrors){}else{refreshPage();
return 
}}else{$.isReady=false;
$.readyList=[];
$(parent).append(state.html);
$.ready();
if((ajaxSwingTableClickTarget!=null)&&(state.action==3)&&(ajaxSwingTableClickTarget.tableId==state.componentId)){if(ajaxSwingTableClickTarget.id){if((ajaxSwingTableClickTarget.nodeName=="INPUT")&&(ajaxSwingTableClickTarget.getAttribute("type").toLowerCase()=="checkbox")){var newEl=document.getElementById(ajaxSwingTableClickTarget.id);
if(newEl){if(ajaxSwingTableClickTarget.oldValue==newEl.checked){$("#"+ajaxSwingTableClickTarget.id).click();
doSubmit("/event/"+ajaxSwingTableClickTarget.id,false)
}}}}}if(savedMenuStyle!=null){var el=document.getElementById(state.componentId);
if(el){activeButton.menu=el;
el.style.visibility=savedMenuStyle.visibility;
el.style.zIndex=savedMenuStyle.zIndex;
el.style.top=savedMenuStyle.top;
el.style.left=savedMenuStyle.left
}}}}}}if(removeTempScroll){removeTempScroll=false;
$(".tempScrollHolder").remove()
}if(ajaxSwingFocusBackElement){ajaxSwingFocusBackElement.__submittingChanges=false;
data.focusedComponentId=null
}ajaxSwingTableClickTarget=null;
if(data.script!=null&&data.script!=""){$.globalEval(data.script)
}if(data.requestId!=0){postSubmitAjaxSwingWindows(data)
}if(data.errorMessage!=null){if(data.redirectURL!=null){showErrorMessageUI(data.errorMessage,ajaxSwingErrorStatus,data.errorMessageDetails,data.errorMessageTitle,data);
ajaxSwingErrorStatus=ajaxSwingErrorStatus+1;
postSubmitAjaxSwingWindows(data)
}else{showErrorMessageUI(data.errorMessage,0,data.errorMessageDetails,data.errorMessageTitle,data)
}}if(data.redirectURL!=null){if(data.errorMessage==null){window.location=data.redirectURL
}}else{window.ajaxSwing.lastClientWidth=data.clientWidth;
window.ajaxSwing.lastClientHeight=data.clientHeigth;
checkWindowSize()
}finishScrollAjaxRequest()
}function finishScrollAjaxRequest(){ajaxSwingAjaxScrollCurrentRequest=null;
if(ajaxSwingAjaxScrollLastRequest!=null){ajaxSwingAjaxScrollCurrentRequest=ajaxSwingAjaxScrollLastRequest;
doFastSubmit("ajaxScroll",ajaxSwingAjaxScrollLastRequest);
ajaxSwingAjaxScrollLastRequest=null
}}function pingSession(keepSessionAlive){if(ajaxSwingSubmitAction!=null){if(ajaxSwingUpdateSeconds>5000){setTimeout("pingSession("+keepSessionAlive+")",2000)
}return 
}if(ajaxSwingPingActive==true){return 
}ajaxSwingPingActive=true;
var data={};
data.keepSessionAlive=keepSessionAlive;
doFastSubmit("ping",data,onPingAjaxComplete)
}function notifyBeforeSessionExpiration(message){if(ajaxSwingSessionExpirationInterval!=null){clearInterval(ajaxSwingSessionExpirationInterval);
ajaxSwingSessionExpirationInterval=null
}if(confirm(message)){pingSession(true);
resetSessionExpirationInterval()
}}function setUpdateInterval(updateSeconds){if(updateSeconds!=ajaxSwingUpdateSeconds||updateSeconds==0){if(ajaxSwingUpdateInterval!=null){clearInterval(ajaxSwingUpdateInterval);
ajaxSwingUpdateInterval=null
}ajaxSwingUpdateSeconds=updateSeconds;
if(updateSeconds!=0){ajaxSwingUpdateInterval=setInterval("pingSession("+ajaxSwingKeepSessionAlive+")",updateSeconds*1000)
}}}function initializeAndSubmit(){var value="";
value+=getClientWidth();
value+=","+getClientHeight();
var rightNow=new Date();
var date1=new Date(rightNow.getFullYear(),0,1,0,0,0,0);
var date2=new Date(rightNow.getFullYear(),6,1,0,0,0,0);
value+=","+(-rightNow.getTimezoneOffset());
value+=","+(-date1.getTimezoneOffset());
value+=","+(-date2.getTimezoneOffset());
var ajaxswingInput=document.getElementById("AjaxSwingInitData");
ajaxswingInput.setAttribute("value",value);
document.AjaxSwingForm.submit()
}function refreshPage(){window.location=window.location
}var ajaxSwingRootContainer=null;
var ajaxSwingAjaxScrollTimeout=null;
var ajaxSwingAjaxScrollLastRequest=null;
var ajaxSwingAjaxScrollCurrentRequest=null;
var AjaxSwingCurrentlyDraggedElement=null;
var AjaxSwingCurrentlyDroppableElement=null;
var AjaxSwingCurrentlyVisibleAjaxLoading=null;
function getAjaxSwingRootContainer(){if(ajaxSwingRootContainer==null){ajaxSwingRootContainer=document.getElementById("rootContainer")
}return ajaxSwingRootContainer
}function restoreScrollPanePositions(){$(".scrollPane").each(function(){var scrollValue=document.AjaxSwingForm[this.id.substring(0,this.id.length-5)].value;
this.mrestoreScrollPaneDone=true;
if(this.ajaxScrolling){return 
}if(scrollValue){var commaIndex=scrollValue.indexOf(",");
var saveScrollLeft=scrollValue.substring(0,commaIndex);
var saveScrollTop=scrollValue.substring(commaIndex+1);
var element=this;
var $list=$(".list",this);
if($list.length!=0){element=$list.get(0)
}var $tableScroller=$(".tableScroller",this);
if($tableScroller.length!=0){element=$tableScroller.get(0)
}element.scrollLeft=saveScrollLeft;
element.scrollTop=saveScrollTop
}})
}function scrollPaneOnSubmit(){var scrollElement=document.getElementById(this.id+"Inner");
var scrollLeft=this.scrollLeft;
var scrollTop=this.scrollTop;
if(scrollElement){scrollLeft=scrollElement.scrollLeft;
scrollTop=scrollElement.scrollTop
}var $list=$(".list",this);
if($list.length!=0){scrollLeft=$list.get(0).scrollLeft;
scrollTop=$list.get(0).scrollTop
}var $tableScroller=$(".tableScroller",this);
if($tableScroller.length!=0){scrollLeft=$tableScroller.get(0).scrollLeft;
scrollTop=$tableScroller.get(0).scrollTop
}document.AjaxSwingForm[this.id].value=Math.round(scrollLeft)+","+Math.round(scrollTop)
}function tabbedPaneInit(id,selectedTab,disabledTabs,dynamicEvents){var options={};
if(dynamicEvents==true){options.click=tabbedPaneOnChange
}var $tabs=$("#"+id+">ul.ui-tabs-nav");
$tabs.tabs(selectedTab+1,options);
$tabs.get(0).AJS_onSubmit=tabbedPaneOnSubmit;
for(var i=0;
i<disabledTabs.length;
i++){$.ui.tabs.getInstance($tabs.get(0)).disable(disabledTabs[i]+1)
}$tabs.css("visibility","visible")
}function tabbedPaneOnSubmit(){var activeTabIndex=$(this).tabsSelected()-1;
document.getElementById(this.parentNode.id+"Value").setAttribute("value",activeTabIndex)
}function onSliderChange(e,ui){var sliderId=this.id.substring(0,this.id.indexOf("-track"));
$("#"+sliderId+"Value").val(ui.value);
if(typeof this.attributes.submit){doSubmit("/event/"+sliderId)
}}function submitResize(){doSubmit("resize:"+getClientWidth()+","+getClientHeight())
}var pendingResizes=0;
function scheduleSubmitResize(){pendingResizes++;
setTimeout("submitLastResize()",100);
return true
}function submitLastResize(){pendingResizes--;
if(pendingResizes==0){submitResize()
}}function setComboValue(comboName,value){var comboInput=document.getElementById(comboName+"Value");
if(!comboInput){comboInput=document.getElementById(comboName+"Input")
}deactivateButton(activeButton);
comboInput.setAttribute("value",value);
comboInput.value=value;
if(comboInput.fireEvent){comboInput.fireEvent("onchange")
}else{var e=document.createEvent("HTMLEvents");
e.initEvent("change",false,false);
comboInput.dispatchEvent(e)
}}function getEventSource(event){if(!event||event==null){event=window.event
}if(event.srcElement){return event.srcElement
}if(event.target){return event.target
}return null
}function expandImagesUrls(){for(i=0;
i<document.images.length;
i++){if(document.images[i].src==""){document.images[i].src=getDocsURL()+"/images/"+document.images[i].alt
}}}function submitMouseClick(compName,event){if(!event){event=window.event
}var x,y;
if(event.offsetX){x=event.offsetX;
y=event.offsetY
}else{if(event.layerX){x=event.layerX;
y=event.layerY
}}var actionString="/click/"+compName+"/"+x+"_"+y;
doSubmit(actionString)
}function doNothing(){return false
}function getParentByClassName(element,className){if(element==null||element.parentNode==null){return null
}if(element.parentNode.className!=null&&element.parentNode.className.search(className)!=-1){return element.parentNode
}return getParentByClassName(element.parentNode,className)
}function accordionOnChange(accordion){var index=accordion.newHeader.attr("index");
$("#"+this.id+"Value").val(index)
}function tabbedPaneOnChange(initObj,newFocus,oldFocus){var tabName=newFocus.id;
var tabSuffix="-tab-";
var suffixStart=tabName.lastIndexOf(tabSuffix);
var containerId=tabName.substring(0,suffixStart);
var tabIndex=tabName.substring(suffixStart+tabSuffix.length);
doSubmit("/tab/"+containerId+"/"+tabIndex);
return true
}function doTableAutoscroll(componentName){var listdiv=$("#"+componentName);
var list=$("table",listdiv).get(0);
var scrollpane=$(listdiv).parent().get(0);
if(typeof scrollpane!="undefined"){var scrollTop=$(list).height()-$(scrollpane).height()+18;
if(scrollTop>0){scrollpane.scrollTop=scrollTop
}}}function copyToClipboard(copyText){if(window.clipboardData&&clipboardData.setData){clipboardData.setData("Text",copyText)
}else{try{netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
var str=Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
if(!str){return false
}str.data=copyText;
var trans=Components.classes["@mozilla.org/widget/transferable;1"].createInstance(Components.interfaces.nsITransferable);
if(!trans){return false
}trans.addDataFlavor("text/unicode");
trans.setTransferData("text/unicode",str,copyText.length*2);
var clipid=Components.interfaces.nsIClipboard;
var clip=Components.classes["@mozilla.org/widget/clipboard;1"].getService(clipid);
if(!clip){return false
}clip.setData(trans,null,clipid.kGlobalClipboard)
}catch(err){alert("Your browser does not allow access to your computer's clipboard")
}}}function isEmpty(variable){if(typeof variable=="unknown"){return true
}if(typeof variable=="undefined"){return true
}if(variable==null||variable==""){return true
}return false
}function onUploadClicked(){var fileInput=document.getElementById("upload.file");
if(isEmpty(fileInput.value)){alert("Please select a file");
return false
}return doSubmit("/upload/ok")
}function showRollover(button,show){var $img=$("IMG",button);
if($img.length!=0){var img=$img.get(0);
if(show==true){if(typeof img.originalSrc=="undefined"){img.originalSrc=img.src
}img.src=button.getAttribute("rolloverSrc")
}else{img.src=img.originalSrc
}}}function onDocumentKeyDown(event){if(document.AjaxSwingForm){if(ajaxSwingSubmitAction==null){document.AjaxSwingForm.__HasClientChanges.value="true"
}if(event.keyCode==27){disableCellEditor()
}if(event.keyCode==13){disableCellEditor()
}}if(event.keyCode==8){
	if(event.srcElement){
		if(event.srcElement.tagName&&(event.srcElement.tagName.toUpperCase()!="INPUT")&&(event.srcElement.tagName.toUpperCase()!="TEXTAREA")){
			return false;
		} else {
			if (event.srcElement.tagName && event.srcElement.readOnly) {
				return false;
			}
		}
	}else{
		if(event.target&&event.target.nodeName&&(event.target.nodeName.toUpperCase()!="INPUT")&&(event.target.nodeName.toUpperCase()!="TEXTAREA")){
			return false;
		} else {
			if (event.target.tagName && event.target.readOnly) {
				return false;
			}
		}
	}
}
}
function onDocumentClick(event){if(document.AjaxSwingForm){if(ajaxSwingSubmitAction==null){document.AjaxSwingForm.__HasClientChanges.value="true"
}else{if(ajaxSwingSubmittingTableSelection){document.AjaxSwingForm.__HasClientChanges.value="true"
}}}}function textAreaOnKeyPressHandler(element,event,hasEventListeners,eventSubmitAction){if(hasEventListeners){if(!element.__lastonkeydown){element.__lastonkeydown=new Date().getTime()
}element.__lastonkeydown=new Date().getTime();
ajaxSwingFocusBackElement=element;
setTimeout(function(){submitTextAreaChanges(element,eventSubmitAction)
},1000)
}if(event.keyCode==13){event.cancelBubble=true;
if(event.stopPropagation){event.stopPropagation()
}event.disableSubmit=true;
return true
}return true
}function submitTextAreaChanges(element,eventSubmitAction){if(element.__lastonkeydown&&new Date().getTime()-element.__lastonkeydown>500){if(!element.__submittingChanges){element.__submittingChanges=true;
if(ajaxSwingFocusBackElement){ajaxSwingFocusBackElement=element;
ajaxSwingFocusBackElement.oldReadOnly=element.readOnly;
ajaxSwingFocusBackElement.beforeSubmitValue=element.value
}doSubmit(eventSubmitAction)
}}}function inputFieldOnActionDown(element,event){if(event.keyCode==13){doSubmit("/event/"+element.id+"/action",false)
}else{return inputFieldOnKeyDownHandler(element,event)
}}function inputFieldOnKeyDownHandler(element,event){var currentTime=new Date().getTime();
if(!element.__lastonkeydown){element.__lastonkeydown=currentTime
}else{if(currentTime-element.__lastonkeydown<500){return true
}}if(event.keyCode==9||event.keyCode==13||event.keyCode==27){return true
}element.__lastonkeydown=new Date().getTime();
element.__value=element.value;
var requestId=document.getElementById("__RequestId").getAttribute("value");
ajaxSwingFocusBackElement=element;
setTimeout(function(){submitInputFieldChanges(element,requestId)
},1000);
return true
}function submitInputFieldChanges(element,requestId){var currentRequestId=document.getElementById("__RequestId").getAttribute("value");
if(currentRequestId!=requestId){return 
}if(element.__value==element.value){return 
}if(element.__lastonkeydown&&new Date().getTime()-element.__lastonkeydown>500){var onChangeCode=element.getAttribute("submitOnChange");
if(onChangeCode&&!element.__submittingChanges){element.__submittingChanges=true;
if(ajaxSwingFocusBackElement){ajaxSwingFocusBackElement=element;
ajaxSwingFocusBackElement.oldReadOnly=element.readOnly;
ajaxSwingFocusBackElement.beforeSubmitValue=element.value
}eval(onChangeCode)
}}}$.fn.insertAtCaret=function(myValue){return this.each(function(){if(document.selection){this.focus();
sel=document.selection.createRange();
sel.text=myValue;
this.focus()
}else{if(this.selectionStart||this.selectionStart=="0"){var startPos=this.selectionStart;
var endPos=this.selectionEnd;
var scrollTop=this.scrollTop;
this.value=this.value.substring(0,startPos)+myValue+this.value.substring(endPos,this.value.length);
this.focus();
this.selectionStart=startPos+myValue.length;
this.selectionEnd=startPos+myValue.length;
this.scrollTop=scrollTop
}else{this.value+=myValue;
this.focus()
}}})
};
function readioButtonOnClick(id){var el=document.getElementById(id);
if(el.getAttribute("wasChecked")=="true"){if(el.getAttribute("singleRadio")){el.checked=false
}el.setAttribute("wasChecked","false")
}else{el.setAttribute("wasChecked","true")
}return true
}function getButtonOuterObject(object){if(object==null){return null
}if($(object).hasClass("button")){return object
}return getButtonOuterObject(object.parentNode)
}function btnOnMouseDown(el,event){el=getButtonOuterObject(el);
if(el){if(!$(el).hasClass("btn-non-enabled")){$(el).addClass("btn-pressed")
}}return true
}function btnOnMouseUp(el,event){el=getButtonOuterObject(el);
if(el){$(el).removeClass("btn-pressed")
}return true
}function btnOnMouseOut(el,event){el=getButtonOuterObject(el);
if(el){$(el).removeClass("btn-pressed")
}return true
}function printPdf(name){var path=getDocsURL()+"/../temp/"+name;
var html='<iframe width=0 height=0 src="'+path+'" name="printPdfObj"></iframe>';
var screen=document.getElementById("AjaxSwingForm");
$(screen).append(html);
setTimeout('window.frames["printPdfObj"].focus();window.frames["printPdfObj"].print();',2000);
setTimeout('var el = window.frames["printPdfObj"];if(el)$(el).remove();',10000)
}function ValidateMaxLength(obj){var mlength=obj.getAttribute?parseInt(obj.getAttribute("maxlength")):"";
if(obj.getAttribute&&obj.value.length>mlength){obj.value=obj.value.substring(0,mlength)
}return true
}function isUnsignedInteger(s){if(!s){return false
}if(s.toString().search(/^[0-9]+$/)==0){if(s.toString().length>1){if(s.toString()[0]!="0"){return true
}}else{if(s.toString().length==1){return true
}}}return false
}function ValidateOnlyInt(obj){if(!isUnsignedInteger(obj.value)){obj.value=obj.getAttribute("lastvalue")
}obj.setAttribute("lastvalue",obj.value);
return true
}function fenOnMazimizedKeydown(eleId,event){if(!event){event=window.event
}var ele=xGetElementById(eleId);
if((event.keyCode==13)&&(!event.disableSubmit)){var $defaultButton=$("div.button[@default=true]",ele);
if($defaultButton.length>0){event.returnValue=false;
event.cancel=true;
doSubmit("/button/"+$defaultButton.attr("id"))
}}if(event.keyCode==27){disableCellEditor()
}return true
}function initializeDragAndDrop(){}function initDragSource(elem,ev){if(!elem.dragInitialized){elem.onselectstart=function(){return false
};
var screen=document.getElementById("AjaxSwingForm");
elem.startDragPosition=$(elem).offset();
elem.screenOffset=$(screen).offset();
elem.startDragPosition.left=elem.startDragPosition.left-elem.screenOffset.left;
elem.startDragPosition.top=elem.startDragPosition.top-elem.screenOffset.top;
$(elem).draggable({helper:"clone",revert:true,appendTo:screen,zIndex:99999,start:function(event,ui){AjaxSwingCurrentlyDraggedElement=elem;
var x,y;
var posx=0;
var posy=0;
if(event.pageX||event.pageY){posx=event.pageX;
posy=event.pageY
}else{if(event.clientX||event.clientY){posx=event.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
posy=event.clientY+document.body.scrollTop+document.documentElement.scrollTop
}}var offsetQ=$(elem).offset();
y=posy-offsetQ.top;
x=posx-offsetQ.left;
elem.dragRelativeX=x;
elem.dragRelativeY=y
},stop:function(event,ui){AjaxSwingCurrentlyDraggedElement=null;
elem.stopDragPosition=$(ui.helper).position();
var dx=-elem.startDragPosition.left+elem.stopDragPosition.left;
var dy=-elem.startDragPosition.top+elem.stopDragPosition.top;
$("#AjaxSwingForm > .ui-draggable").hide();
var posx=0;
var posy=0;
var e=event;
if(!e){var e=window.event
}if(e.pageX||e.pageY){posx=e.pageX;
posy=e.pageY
}else{if(e.clientX||e.clientY){posx=e.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
posy=e.clientY+document.body.scrollTop+document.documentElement.scrollTop
}}var element=document.elementFromPoint(posx,posy);
while((element!=null)&&(!element.hasAttribute("dragTarget"))){if(element.parentNode.id=="AjaxSwingScreen"){element=null;
break
}else{element=element.parentNode
}}if(AjaxSwingCurrentlyDroppableElement){$(AjaxSwingCurrentlyDroppableElement).removeClass("ui-drop-hover");
AjaxSwingCurrentlyDroppableElement=null
}if(element){doSubmit("/draganddrop/"+$(elem).attr("id")+"/to/"+$(element).attr("id"))
}},drag:function(event,ui){$("#AjaxSwingForm > .ui-draggable").hide();
var posx=0;
var posy=0;
var e=event;
if(!e){var e=window.event
}if(e.pageX||e.pageY){posx=e.pageX;
posy=e.pageY
}else{if(e.clientX||e.clientY){posx=e.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
posy=e.clientY+document.body.scrollTop+document.documentElement.scrollTop
}}var element=document.elementFromPoint(posx,posy);
while((element!=null)&&(!$(element).attr("dragTarget"))){element=element.parentNode
}if(AjaxSwingCurrentlyDroppableElement!=element){if(AjaxSwingCurrentlyDroppableElement){$(AjaxSwingCurrentlyDroppableElement).removeClass(AjaxSwingCurrentlyDroppableElement.dragHoverClass)
}if(element){AjaxSwingCurrentlyDroppableElement=element;
var cssClassHover="ui-drop-hover";
if(window.getDragDropHighlightClass){cssClassHover=getDragDropHighlightClass(elem,element)
}if(cssClassHover){$(element).addClass(cssClassHover);
element.dragHoverClass=cssClassHover
}}else{AjaxSwingCurrentlyDroppableElement=null
}}$("#AjaxSwingForm > .ui-draggable").show()
}});
elem.dragInitialized=true;
elem.mfirstMove=true;
dispatchMouveDown(ev,elem)
}return false
}function dispatchMouveDown(event,target){if(event.initMouseEvent){var mousedownEvent=document.createEvent("MouseEvent");
mousedownEvent.initMouseEvent("mousedown",true,true,window,0,event.screenX,event.screenY,event.clientX,event.clientY,event.ctrlKey,event.altKey,event.shiftKey,event.metaKey,0,null);
target.dispatchEvent(mousedownEvent)
}else{if(document.createEventObject){}}}function moveDragSource(elem,ev){if(elem.mfirstMove){$(elem).mouseInteraction("drag",ev)
}}function upDragSource(elem,ev){if(true||elem.mfirstMove){$(elem).mouseInteraction("stop",ev)
}}var ajaxSwingAjaxScrollSyncTimeout=null;
function scrollPaneSyncronize(scrollId){if(ajaxSwingAjaxScrollSyncTimeout==null){ajaxSwingAjaxScrollSyncTimeout=setTimeout("scrollPaneSyncronizeImpl('"+scrollId+"');",100)
}}

function scrollPaneSyncronizeImpl(scrollId) {
try{
if(ajaxSwingAjaxScrollSyncTimeout){
	clearTimeout(ajaxSwingAjaxScrollSyncTimeout);
	ajaxSwingAjaxScrollSyncTimeout=null
}
var pane=document.getElementById(scrollId+"Inner");
var paneRowId=pane.getAttribute("scrollRowElement");
var paneColumnId=pane.getAttribute("scrollColumnElement");
var paneRow=null;
var paneColumn=null;
if(paneRowId!=null) {
	paneRow=document.getElementById(paneRowId)
}
if(paneColumnId!=null) {
	paneColumn=document.getElementById(paneColumnId)
}
if(paneRow!=null) {
	try{
		if(!paneRow.initialTop){
			paneRow.initialTop=paneRow.style.top
		}
		paneRow.style.top=(0-pane.scrollTop+parseInt(paneRow.initialTop))+"px";	
		paneRow.style.clip="rect("+pane.scrollTop+"px,"+paneRow.style.width+","+(0+parseInt(pane.style.height)+pane.scrollTop)+"px,"+0+"px)"
	}catch(ex){}
}
if(paneColumn!=null){
	try{
		if(!paneColumn.initialLeft){
			paneColumn.initialLeft=paneColumn.style.left
		}
		paneColumn.style.left=(0-pane.scrollLeft+parseInt(paneColumn.initialLeft))+"px";		
		paneColumn.style.clip="rect(0px,"+(0+parseInt(pane.style.width)+pane.scrollLeft)+"px,"+paneColumn.style.height+","+pane.scrollLeft+"px)"
	}catch(ex){}
}
}catch(ex){}
}
function scrollPaneScroll(scrollId){if(ajaxSwingAjaxScrollTimeout){clearTimeout(ajaxSwingAjaxScrollTimeout)
}ajaxSwingAjaxScrollTimeout=setTimeout("scrollPaneScrollAjax('"+scrollId+"');",370);
var pane=document.getElementById(scrollId+"Inner");
var pageHolder=document.getElementById(scrollId+"PageHolder");
if(pageHolder){var heightPane=xHeight(pane);
var numPages=(pane.scrollHeight/heightPane);
var curPage=((pane.scrollTop/pane.scrollHeight)*numPages)+1;
if(parseFloat(numPages)>parseInt(numPages)){numPages=parseInt(numPages)+1
}if(parseFloat(curPage)>parseInt(curPage)){curPage=parseInt(curPage)+1
}$(pageHolder).text(curPage+"/"+numPages);
pageHolder.style.display="block";
var paneWidth=xWidth(pageHolder)+18;
var paneTop=parseInt(((pane.scrollTop/pane.scrollHeight)*(heightPane-18*3)))+20+parseInt(pageHolder.getAttribute("origHeight"));
pageHolder.style.top=paneTop+"px";
pageHolder.style.marginLeft="-"+paneWidth+"px"
}}function scrollPaneScrollAjax(scrollId){var pane=document.getElementById(scrollId+"Inner");
var mrunAnyway=false;
if(pane){if(!pane.oldScrollLeft){if(pane.oldScrollLeft!=0){pane.oldScrollLeft=Math.round(pane.scrollLeft);
pane.oldScrollTop=Math.round(pane.scrollTop);
mrunAnyway=(pane.mrestoreScrollPaneDone==true)
}}if(mrunAnyway||(Math.round(pane.scrollLeft)!=pane.oldScrollLeft)||(Math.round(pane.scrollTop)!=pane.oldScrollTop)){var loading=document.getElementById(scrollId+"PlaceHolder");
if(loading){loading.style.backgroundImage='url("'+getDocsURL()+'/images/ajaxswing/ajax-loader.gif")';
loading.style.backgroundRepeat="no-repeat"
}if(AjaxSwingCurrentlyVisibleAjaxLoading){if(AjaxSwingCurrentlyVisibleAjaxLoading==loading){}else{AjaxSwingCurrentlyVisibleAjaxLoading.style.backgroundImage="none"
}}AjaxSwingCurrentlyVisibleAjaxLoading=loading;
var data={};
data.scrollLeft=Math.round(pane.scrollLeft);
data.scrollTop=Math.round(pane.scrollTop);
data.scrollPaneId=scrollId;
document.AjaxSwingForm[scrollId].value=Math.round(pane.scrollLeft)+","+Math.round(pane.scrollTop);
pane.ajaxScrolling=true;
if(ajaxSwingAjaxScrollCurrentRequest==null){ajaxSwingAjaxScrollCurrentRequest=data;
doFastSubmit("ajaxScroll",data)
}else{ajaxSwingAjaxScrollLastRequest=data
}}pane.oldScrollLeft=Math.round(pane.scrollLeft);
pane.oldScrollTop=Math.round(pane.scrollTop);
var pageHolder=document.getElementById(scrollId+"PageHolder");
if(pageHolder){pageHolder.style.display="none"
}}}function scrollPaneStartPreCache(scrollId){}function scrollPaneStartPreCacheAjax(scrollId){var pane=document.getElementById(scrollId+"Inner");
if(pane){var data={};
data.scrollLeft=pane.scrollLeft;
data.scrollTop=pane.scrollTop;
data.scrollPanePreload=true;
data.scrollPaneId=scrollId;
document.AjaxSwingForm[scrollId].value=Math.round(pane.scrollLeft)+","+Math.round(pane.scrollTop);
if(ajaxSwingAjaxScrollCurrentRequest==null){doFastSubmit("ajaxScroll",data)
}}}function onOverDrop(el,event){if(AjaxSwingCurrentlyDraggedElement){$(el).removeClass("ui-drop-hover")
}}function onOutDrop(el,event){if(AjaxSwingCurrentlyDraggedElement){$(el).removeClass("ui-drop-hover")
}}var ajaxSwingPopupMenuLeft=-1;
var ajaxSwingPopupMenuTop=-1;
var ajaxSwingPopupMenuClear=true;
var activeButton=null;
var menuActive=false;
var eventSource=null;
function submitMouseMenuTable(compName,event,row){if(!event){event=window.event
}event.cancelBubble=true;
event.cancelBubbleOverride=true;
var x,y;
var comp=document.getElementById(compName);
var posx=0;
var posy=0;
if(event.pageX){posx=event.pageX
}else{if(event.clientX||event.clientY){posx=event.clientX+document.body.scrollLeft+document.documentElement.scrollLeft
}}x=posx-xPageX(comp);
var actionString="/event/"+compName+"/mousemenu/"+x+"_r"+row;
ajaxSwingPopupMenuLeft=event.clientX;
ajaxSwingPopupMenuTop=event.clientY;
ajaxSwingPopupMenuClear=false;
doSubmit(actionString);
return false
}function submitMouseMenuTableR(compName,rowObject,event,row){if(!event){event=window.event
}event.cancelBubble=true;
event.cancelBubbleOverride=true;
if(row!=-1){if(rowObject.className!="tableSelRow"){event.processRightClick=true;
tableClick(rowObject,event)
}else{disableCellEditor()
}var x,y;
var comp=document.getElementById(compName);
var columnObject=null;
if(event.target){columnObject=findTableColumn(rowObject,event.target)
}else{columnObject=findTableColumn(rowObject,event.srcElement)
}var columnNum=-1;
if(rowObject.childNodes){var columnCounter=0
}for(var count=0;
count<rowObject.childNodes.length;
count++){if(rowObject.childNodes[count].nodeName=="TD"){if(rowObject.childNodes[count]==columnObject){columnNum=columnCounter;
break
}columnCounter++
}}var actionString="/event/"+compName+"/mousemenu/c"+columnNum+"_r"+row;
ajaxSwingPopupMenuLeft=event.clientX;
ajaxSwingPopupMenuTop=event.clientY;
ajaxSwingPopupMenuClear=false;
doSubmit(actionString)
}else{disableCellEditor();
var actionString="/event/"+compName+"/mousemenu/0_r-1";
ajaxSwingPopupMenuLeft=event.clientX;
ajaxSwingPopupMenuTop=event.clientY;
ajaxSwingPopupMenuClear=false;
doSubmit(actionString)
}return false
}function submitMouseMenu(compName,event){if(!event){event=window.event
}event.cancelBubble=true;
event.cancelBubbleOverride=true;
var x,y;
var comp=document.getElementById(compName);
var posx=0;
var posy=0;
if(event.pageX||event.pageY){posx=event.pageX;
posy=event.pageY
}else{if(event.clientX||event.clientY){posx=event.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
posy=event.clientY+document.body.scrollTop+document.documentElement.scrollTop
}}y=posy-xPageY(comp);
x=posx-xPageX(comp);
var actionString="/event/"+compName+"/mousemenu/"+x+"_"+y;
ajaxSwingPopupMenuLeft=event.clientX;
ajaxSwingPopupMenuTop=event.clientY;
ajaxSwingPopupMenuClear=false;
doSubmit(actionString);
return false
}function findParentElement(reqObject,targetElement){if(targetElement==null){return null
}if(targetElement==reqObject){return targetElement
}return findParentElement(reqObject,targetElement.parentNode)
}function onDocumentMouseDown(event){menuActive=false;
if(!event){event=window.event
}var evSrc=getEventSource(event);
var className=evSrc.className;
if(ajaxSwingFocusBackElement){if(!findParentElement(ajaxSwingFocusBackElement,evSrc)){ajaxSwingFocusBackElement=null
}}if(typeof className=="undefinded"){if(typeof evSrc.parentNode!="undefined"){className=parentNode.className
}}if((!className)&&(!(className==""))){return false
}var parentMenuItem=findParentMenuItem(evSrc);
if(parentMenuItem==undefined&&className.indexOf("menuButton")==-1&&className.indexOf("menuText")==-1&&className.indexOf("menuItem")==-1&&className.indexOf("wcPulldownMenu")==-1&&className.indexOf("wcComboMenu")==-1&&className.indexOf("menuTextSep")==-1){if(activeButton){deactivateButton(activeButton)
}}else{return true
}}function findParentMenuItem(element){if(element==null){return undefined
}if(element==document){return undefined
}var className=element.className;
if(className.indexOf("menuButton")==-1&&className.indexOf("menuText")==-1&&className.indexOf("menuItem")==-1&&className.indexOf("wcPulldownMenu")==-1&&className.indexOf("wcComboMenu")==-1&&className.indexOf("menuTextSep")==-1){return findParentMenuItem(element.parentNode)
}else{return element
}}function rightClick(source,menuName,event){source.context=new Object();
source.context.componentId=source.id;
buttonClick(source,menuName,event)
}function buttonClick(button,menuName,event){if(event){if(getEventSource(event)==eventSource){return false
}eventSource=getEventSource(event)
}if(menuName){button.menu=document.getElementById(menuName);
button.menu.parentButton=button
}if(activeButton&&activeButton!=button){if(getParentButton(button)!=activeButton){deactivateButton(activeButton)
}}if(button.isActivated){deactivateButton(button)
}else{activateButton(button,event)
}return false
}function buttonMouseover(button,menuName){if(menuActive==true&&activeButton!=button){deactivateButton(activeButton);
if(menuName){buttonClick(button,menuName)
}}}function submenuMouseover(button,menuName){if(activeButton&&activeButton!=button){if(menuName){if(activeButton!=parentButton){submenuMouseover(button,null)
}}var buttonContext=null;
if(typeof activeButton.context!="undefined"){buttonContext=activeButton.context
}var parentButton=getParentButton(button);
if(parentButton){parentButton.isActivated=false
}if(menuName){if(button.className.indexOf("menuButton")!=-1){deactivateButton(activeButton)
}if(parentButton&&parentButton.menu){parentButton.menu.style.visibility="visible"
}buttonClick(button,menuName)
}else{if(parentButton&&activeButton!=parentButton){if(parentButton){if(parentButton.menu.style.visibility=="visible"){parentButton.saveOldPosition=true
}}deactivateButton(activeButton);
var parent2=getParentButton(parentButton);
if(parent2&&parent2.menu){parent2.menu.style.visibility="visible"
}buttonClick(parentButton,parentButton.menu.id);
parentButton.saveOldPosition=false
}}if(buttonContext!=null){activeButton.context=buttonContext
}}recursevlyActivateParentButton(activeButton);
return true
}function getParentButton(el){if(typeof el.offsetParent=="unknown"||isEmpty(el.offsetParent)==true){return null
}if(el.offsetParent.className=="wcPulldownMenu"||el.offsetParent.className=="wcComboMenu"){return el.offsetParent.parentButton
}return null
}function activateButton(button,event){menuActive=true;
var screen=document.getElementById("AjaxSwingForm");
if(!button.menu.oldParent){button.menu.oldParent=button.menu.parentNode
}if(button.menu.parentNode!=screen){screen.appendChild(button.menu)
}var x=-1,y=-1;
if(event){if(typeof (event.savedX)!="undefined"){if(ajaxSwingPopupMenuLeft!=-1){x=ajaxSwingPopupMenuLeft-xPageX(screen);
y=ajaxSwingPopupMenuTop-xPageY(screen)
}else{x=event.savedX-xPageX(screen);
y=event.savedY-xPageY(screen)
}}else{x=event.clientX-xPageX(screen);
y=event.clientY-xPageY(screen)
}}else{if(button.className.indexOf("menuButton")!=-1||button.className.indexOf("submenuText")!=-1){x=getSubMenuX(button,screen);
y=getSubMenuY(button,screen)
}}if(button.className=="menuButton"){y+=button.offsetHeight+2
}if(x+xWidth(button.menu)>getClientWidth()){x=getClientWidth()-xWidth(button.menu)
}if(y+xHeight(button.menu)>getClientHeight()){y=getClientHeight()-xHeight(button.menu)
}button.isActivated=true;
if(button.menu){if((x!=-1)&&(!button.saveOldPosition)){button.menu.style.left=x+"px";
button.menu.style.top=y+"px"
}button.menu.style.visibility="visible";
button.menu.style.zIndex=900000
}activeButton=button;
var parentButton=getParentButton(activeButton);
if(parentButton!=null&&parentButton.menu){parentButton.menu.style.visibility="visible"
}}function recursevlyActivateParentButton(button){if(button){var screen=document.getElementById("AjaxSwingForm");
if(!button.menu.oldParent){button.menu.oldParent=button.menu.parentNode
}if(button.menu.parentNode!=screen){screen.appendChild(button.menu)
}button.menu.style.visibility="visible";
recursevlyActivateParentButton(getParentButton(button))
}}function deactivateButton(button){if(button!=null){if(button.menu){button.menu.style.visibility="hidden";
if(button.menu.oldParent){button.menu.oldParent.appendChild(button.menu)
}}if(button.parent&&button.parent.menu){deactivateButton(button.parent);
button.parent.menu.style.visibility="hidden"
}var parentButton=getParentButton(button);
if(parentButton&&parentButton.menu){deactivateButton(parentButton);
parentButton.menu.style.visibility="hidden"
}button.isActivated=false
}if(activeButton!=null){activeButton.context=null
}activeButton=null;
eventSource=null
}function submenuMouseclick(eventSrc,action){var returnValue=doSubmit(action);
if($.browser.msie){var clone=eventSrc.cloneNode(true);
eventSrc.replaceNode(clone)
}return returnValue
}function submenuUploadMouseover(button){$(button).parent().parent().addClass("menuHover");
$(button).parent().prev().addClass("menuHover")
}function submenuUploadMouseout(button){$(button).parent().parent().removeClass("menuHover");
$(button).parent().prev().removeClass("menuHover")
}function getSubMenuX(el,screen){var x=el.offsetLeft;
if(el.offsetParent){if(el.menu){var i=0;
var menuParent=el.menu.offsetParent;
var element=el;
var borderWidth;
while(element.offsetParent&&element.offsetParent!=screen){if((!$.browser.msie)||(element.offsetParent.id!="AjaxSwingScreen")){x=x+element.offsetParent.offsetLeft-element.offsetParent.scrollLeft;
borderWidth=parseInt($(element.offsetParent).css("border-left-width"));
if(borderWidth){x+=borderWidth
}}if(i++==100){alert("internal error, element = "+element+", menuParent = "+menuParent);
break
}element=element.offsetParent
}}else{x+=el.offsetParent.offsetLeft
}}if(el.className=="submenuText"){x+=el.offsetParent.offsetWidth
}return x
}function getSubMenuY(el,screen){if(el.offsetParent){if(el.menu){var y=el.offsetTop;
var i=0;
var menuParent=el.menu.offsetParent;
var borderWidth;
while(el.offsetParent&&el.offsetParent!=menuParent&&el.offsetParent!=screen){if((!$.browser.msie)||(el.offsetParent.id!="AjaxSwingScreen")){y=y+el.offsetParent.offsetTop-el.offsetParent.scrollTop;
borderWidth=parseInt($(el.offsetParent).css("border-top-width"));
if(borderWidth){y+=borderWidth
}}if(i++==100){alert("internal error, el = "+el+", menuParent = "+menuParent);
break
}el=el.offsetParent
}return y
}else{return el.offsetParent.offsetTop+el.offsetTop
}}else{return el.offsetTop
}}function submenuGetEventSourceId(eventSource){if(typeof eventSource.context!="undefined"&&eventSource.context!=null){return eventSource.context.componentId
}else{return eventSource.id
}}var ajaxSwingErrorStatus=0;
var ajaxSwingErrorRefreshInterval=null;
var ajaxSwingErrorRefreshTime=0;
var ajaxSwingErrorRefreshCalled=false;
function showErrorMessage(message,title){showErrorMessageUI(message,0,null,title,null)
}function handleJavascriptError(msg,url,line){showErrorMessageUI("There was a JavaScript error while processing your request. Please try again or contact system administrator<br><br>"+msg+"<br>Line: "+line,0,null,null,null);
return true
}function showErrorMessageUI(message,statusCount,details,title,data){ajaxSwingErrorRefreshCalled=false;
ajaxSwingErrorRefreshTime=-1;
if(data){if(data.errorRefreshTime){ajaxSwingErrorRefreshTime=data.errorRefreshTime
}if(data.errorRefreshTime==0){ajaxSwingErrorRefreshTime=0
}}if(ajaxSwingErrorRefreshTime==0){if(statusCount==0){errorRefresh(true);
return 
}else{ajaxSwingErrorRefreshTime=-1
}}if(document.getElementById("errorPopupDialog")){$("#errorPopupDialog").dialog("close");
removeElement(document.getElementById("errorPopupDialog"))
}var screen=document.getElementById("AjaxSwingScreen");
if(!title){title="Internal Error"
}$(screen).append("<div id='errorPopupDialog' style='display:block; width:350px'><div class=\"windowBar windowBar-selected\">"+title+'</div><div id="errorDialogText" style="padding:5px;">'+message+'<div id="errorDialogDetails" style="display:none; width:700px; height:300px; overflow:auto;"><pre>'+details+"</pre></div></div></div>");
var buttonsObj={Refresh:function(){clearInterval(ajaxSwingErrorRefreshInterval);
errorRefresh()
}};
if(details){buttonsObj.Details=function(){$("#errorDialogDetails").css("display","block");
$("#errorPopupDialog").css("width","auto");
$(".ui-dialog").css("margin","-150px 0px 0px -175px");
$(".ui-dialog").css("width","700px");
clearInterval(ajaxSwingErrorRefreshInterval);
var dTimer=document.getElementById("errorPopupDialogTimer");
if(dTimer){dTimer.innerHTML="-"
}}
}var refrTextInner="Auto refresh in";
if(data){if(data.errorMessageAutoRefreshText){refrTextInner=data.errorMessageAutoRefreshText
}else{if(!data.errorMessage){refrTextInner=data
}}}var refrText='<div style="padding-top:1px; font-style:italic;">'+refrTextInner+' <span style="font-style:normal" id="errorPopupDialogTimer">'+ajaxSwingErrorRefreshTime+"</span></div>";
if(statusCount>=1){refrText=""
}if(ajaxSwingErrorRefreshTime==100){ajaxSwingErrorRefreshTime=0
}if(ajaxSwingErrorRefreshTime==0){refrText=""
}$("#errorPopupDialog").dialog({draggable:false,resizable:false,modal:true,height:"auto",width:"350px",overlay:{backgroundColor:"#000",opacity:0.5},buttonsText:refrText,buttons:buttonsObj,close:function(event,ui){clearInterval(ajaxSwingErrorRefreshInterval);
errorRefresh(true);
var shadow=$(".ui-dialog + .fx-shadow");
if(shadow){$(shadow).remove()
}}});
if(ajaxSwingErrorRefreshInterval){clearInterval(ajaxSwingErrorRefreshInterval)
}if(statusCount<=1){if(ajaxSwingErrorRefreshTime>0){ajaxSwingErrorRefreshInterval=setInterval(function(){ajaxSwingErrorRefreshTime=ajaxSwingErrorRefreshTime-1;
var dTimer=document.getElementById("errorPopupDialogTimer");
if(dTimer){dTimer.innerHTML=ajaxSwingErrorRefreshTime
}if(ajaxSwingErrorRefreshTime==0){clearInterval(ajaxSwingErrorRefreshInterval);
errorRefresh()
}},1000)
}}}function errorRefresh(inClose){if(!ajaxSwingErrorRefreshCalled){ajaxSwingErrorRefreshCalled=true;
if(!inClose){var errdialog=$("#errorPopupDialog");
if(errdialog){$(errdialog).dialog("close")
}}ajaxSwingErrorStatus=ajaxSwingErrorStatus+1;
if(ajaxSwingErrorRefreshInterval){clearInterval(ajaxSwingErrorRefreshInterval)
}ajaxSwingErrorRefreshTime=100;
$(document.AjaxSwingForm).ajaxSubmit({success:errorProcessResponse,contentType:"application/x-www-form-urlencoded; charset=utf-8"})
}}function errorProcessResponse(data,statusText){if(ajaxSwingErrorRefreshTime==100){ajaxSwingErrorRefreshTime=0;
if(ajaxSwingErrorRefreshInterval){clearInterval(ajaxSwingErrorRefreshInterval)
}if(statusText=="success"){if(data.length>1&&data.substring(0,1)=="{"){data=eval("data = ("+data+")");
if((data.errorMessage!=null)&&(data.redirectURL!=null)){showErrorMessageUI(data.errorMessage,ajaxSwingErrorStatus,data.errorMessageDetails,data.errorMessageTitle,data)
}else{refreshPage()
}}else{refreshPage()
}}}}var ajaxSwingCurrentCellEditElement=null;
var ajaxSwingCurrentCellEditClickElement=null;
var ajaxSwingTableClickTarget=null;
function tableInit(tableId,isPaginated,scrollPaneId,prevPage,nextPage){var table=document.getElementById(tableId);
if(table){initResizeColumnOnTable(table)
}if((table)&&(table.getAttribute("singleSelection")=="false")){table.lastClickedColumn=-1;
table.lastClickedRow=-1;
table.AJS_onSubmit=tableOnSubmit;
var $selectedRow=$("tr.tableSelRow:first",table);
if($selectedRow.length>0){var selectionAnchor=$selectedRow[0];
table.selectionAnchor=$selectedRow[0];
var an
}}var parentScroll=document.getElementById(scrollPaneId+"Inner");
if(!parentScroll){parentScroll=document.getElementById(scrollPaneId)
}if(isPaginated&&parentScroll){var current=parentScroll.style.overflow;
parentScroll.originalOwerflow=(current?current:"");
parentScroll.style.overflow="hidden";
$(table).bind("mousewheel",function(event,delta){if(delta>0&&prevPage>=0){tableSubmitPageClick("/tablePageClick/"+tableId+"/"+prevPage)
}if(delta<0&&nextPage>=0){tableSubmitPageClick("/tablePageClick/"+tableId+"/"+nextPage)
}})
}else{if(parentScroll&&parentScroll.originalOwerflow){parentScroll.style.overflow=parentScroll.originalOwerflow
}}}function tableClickS(tableId,rowObject,rowNum,submit){if(rowObject.className!="tableSelRow"){rowObject.className="tableSelRow";
setRowColumnBackground(rowObject,"transparent");
var tableInput=document.getElementById(tableId+"Value");
var oldRowId=tableId+"_"+tableInput.getAttribute("value");
var oldRow=document.getElementById(oldRowId);
if(oldRow==rowObject){return true
}if(oldRow){oldRow.className=null;
setRowColumnBackground(oldRow,"")
}tableInput.setAttribute("value",rowNum);
if(submit=="true"){ajaxSwingSubmittingTableSelection=true;
var table=document.getElementById(tableId);
doSubmit("/event/"+tableId,table.blockUIOnSelection)
}}return true
}function tableClickM(tableId,rowObject,rowNum,submit,table,event){var doDefaultProcessing=true;
if(event.ctrlKey){if(!$(rowObject).hasClass("tableSelRow")){$(rowObject).addClass("tableSelRow");
setRowColumnBackground(rowObject,"transparent")
}else{$(rowObject).removeClass("tableSelRow");
setRowColumnBackground(rowObject,"")
}if(table.lastClickedRow==rowNum){table.lastClickedRow=-1
}doDefaultProcessing=false
}else{if(event.shiftKey&&table.selectionAnchor){var selectionStart=Math.min(rowObject.rowIndex,table.selectionAnchor.rowIndex);
var selectionEnd=Math.max(rowObject.rowIndex,table.selectionAnchor.rowIndex);
$("tr",table).each(function(i){if(this.rowIndex>=selectionStart&&this.rowIndex<=selectionEnd){if(!$(this).hasClass("tableSelRow")){$(this).addClass("tableSelRow");
setRowColumnBackground(this,"transparent")
}}else{if($(this).hasClass("tableSelRow")){$(this).removeClass("tableSelRow");
setRowColumnBackground(this,"")
}}});
doDefaultProcessing=false;
xStopPropagation(event);
event.cancel=true;
event.cancelBubble=true;
event.returnValue=false;
if(event.preventDefault){event.preventDefault()
}onDocumentMouseDown(event)
}else{var selectionReset=false;
$("#"+tableId+"Table tr.tableSelRow").each(function(i){if(this!=rowObject){$(this).removeClass("tableSelRow");
setRowColumnBackground(this,"");
selectionReset=true
}});
if($(rowObject).hasClass("tableSelRow")){if(!selectionReset){submit=false
}}else{$(rowObject).addClass("tableSelRow");
setRowColumnBackground(rowObject,"transparent")
}table.selectionAnchor=rowObject;
var selectionResetInput=document.getElementById(tableId+"SelectionReset");
if(selectionResetInput){selectionResetInput.value="true"
}}}if(submit=="true"){ajaxSwingSubmittingTableSelection=true;
doSubmit("/event/"+tableId,table.blockUIOnSelection)
}return doDefaultProcessing
}function tableClick(rowObject,event){if(!event){event=window.event
}if(event.button==2&&!event.processRightClick){return false
}var lastUnderscore=rowObject.id.lastIndexOf("_");
var tableId=rowObject.id.substring(0,lastUnderscore);
var rowNum=rowObject.id.substring(lastUnderscore+1);
var table=document.getElementById(tableId);
if(!table){table=rowObject.parentNode;
while(table.tagName!="TABLE"){if(table==null){return 
}table=table.parentNode
}table=table.parentNode;
tableId=table.id
}var submit=table.getAttribute("dynamicEventsEnabled");
var singleSelection=table.getAttribute("singleSelection");
table.lastClickTarget=null;
var columnObject=null;
if(event.target){columnObject=findTableColumn(rowObject,event.target);
ajaxSwingTableClickTarget=event.target
}else{columnObject=findTableColumn(rowObject,event.srcElement);
ajaxSwingTableClickTarget=event.srcElement
}table.blockUIOnSelection=false;
if(ajaxSwingTableClickTarget){ajaxSwingTableClickTarget.tableId=tableId;
if(ajaxSwingTableClickTarget.getAttribute("type")){table.blockUIOnSelection=(ajaxSwingTableClickTarget.getAttribute("type").toLowerCase()=="checkbox")
}ajaxSwingTableClickTarget.oldValue=ajaxSwingTableClickTarget.checked
}if(!table.blockUIOnSelection){table.blockUIOnSelection="false!important"
}var columnNum=-1;
if(rowObject.childNodes){var columnCounter=0
}for(var count=0;
count<rowObject.childNodes.length;
count++){if(rowObject.childNodes[count].nodeName=="TD"){if(rowObject.childNodes[count]==columnObject){columnNum=columnCounter;
break
}columnCounter++
}}table.lastClickedColumn=columnNum;
table.lastClickedRow=rowNum;
if(ajaxSwingCurrentCellEditElement){if(ajaxSwingCurrentCellEditElement.parentNode!=columnObject){disableCellEditor()
}}if(singleSelection=="true"){return tableClickS(tableId,rowObject,rowNum,submit)
}else{return tableClickM(tableId,rowObject,rowNum,submit,table,event)
}}function treeMouseClick(tree,event){tree.lastClickTarget=null;
var nodeObject=null;
if(event.target){nodeObject=findTreeNode(event.target)
}else{nodeObject=findTreeNode(event.srcElement)
}if(nodeObject){var x,y;
var posx=0;
var posy=0;
if(event.pageX){posx=event.pageX
}else{if(event.clientX||event.clientY){posx=event.clientX+document.body.scrollLeft+document.documentElement.scrollLeft
}}x=posx-xPageX(tree);
var rowNum=-1;
var holder=nodeObject.parentNode;
if(holder.childNodes){var rowCounter=0
}for(var count=0;
count<holder.childNodes.length;
count++){if(holder.childNodes[count].nodeName=="DIV"){if(holder.childNodes[count]==nodeObject){rowNum=rowCounter+1;
break
}rowCounter++
}}var actionString="/event/"+tree.id+"/mouseclick/"+x+"_r"+rowNum;
doSubmit(actionString)
}else{doSubmit("/event/"+tree.id+"/mouseclick/0_-100")
}}function findTreeNode(object){if(object==null){return null
}if(object.className=="tableTreeElement"){return object
}return findTreeNode(object.parentNode)
}function findTableColumn(rowObject,targetElement){if(targetElement==null){return null
}if(targetElement.parentNode==rowObject){return targetElement
}return findTableColumn(rowObject,targetElement.parentNode)
}function tableOuterClick(tableId,event){if(!event){event=window.event
}if(event.button==2&&!event.processRightClick){return false
}var target=null;
var tableDiv=document.getElementById(tableId);
var tableTable=document.getElementById(tableId+"Table");
if(event.target){target=event.target
}else{target=event.srcElement
}if(target!=tableDiv){while(target!=null){target=target.parentNode;
if(target==tableTable){return true
}if(target==null){return true
}}}var tableInput=document.getElementById(tableId+"Value");
var tableInputValue=tableInput.getAttribute("value");
var oldRowId=tableId+"_"+tableInputValue;
var oldRow=document.getElementById(oldRowId);
if(oldRow){oldRow.className=null;
setRowColumnBackground(oldRow,"")
}var rowNum=-1;
var table=document.getElementById(tableId);
var submit=table.getAttribute("dynamicEventsEnabled");
var singleSelection=table.getAttribute("singleSelection");
table.lastClickedRow=rowNum;
tableInput.setAttribute("value",rowNum);
if(tableInputValue==-1){submit=false
}if(submit=="true"){doSubmit("/event/"+tableId,false)
}return true
}function tableRClick(rowObject,menuName,event){if(!event){event=window.event
}event.savedX=event.clientX;
event.savedY=event.clientY;
if(!$(rowObject).hasClass("tableSelRow")){event.processRightClick=true;
tableClick(rowObject,event)
}var source=rowObject;
if(typeof source.context=="undefined"||source.context==null){source.context=new Object()
}var lastUnderscore=rowObject.id.lastIndexOf("_");
var tableId=rowObject.id.substring(0,lastUnderscore);
source.context.componentId=tableId;
return buttonClick(source,menuName,event)
}function tableSubmitPageClick(action){var blockUI=document.AjaxSwingForm.__HasClientChanges.value=="true";
return doSubmit(action,blockUI)
}function tableOnSubmit(){var selectedIds=this.lastClickedRow?this.lastClickedRow:"-1";
$("#"+this.id+"Table .tableSelRow").each(function(i){var rowNumIndex=this.id.lastIndexOf("_");
selectedIds+=",";
selectedIds+=this.id.substring(rowNumIndex+1)
});
$("#"+this.id+"Value").val(selectedIds)
}function disableCellEditor(){if(ajaxSwingCurrentCellEditElement!=null){if(ajaxSwingCurrentCellEditElement.parentNode){$(ajaxSwingCurrentCellEditElement.parentNode).removeClass("cellEditor")
}var savedValue=ajaxSwingCurrentCellEditElement.savedValue;
saveCellEditorChildValues(ajaxSwingCurrentCellEditElement);
if(!(ajaxSwingCurrentCellEditElement.savedValue==savedValue)){updateCellEditorChildValue(ajaxSwingCurrentCellEditElement.renderElement,savedValue)
}else{if(ajaxSwingCurrentCellEditElement.dirtyInputId){var dirtyInput=document.getElementById(ajaxSwingCurrentCellEditElement.dirtyInputId);
if(dirtyInput){dirtyInput.value=parseInt(dirtyInput.value)-1
}}}ajaxSwingCurrentCellEditElement.style.display="none";
ajaxSwingCurrentCellEditElement.renderElement.style.display="block";
ajaxSwingCurrentCellEditElement=null
}}function setDirty(id){if(id){var dirtyInput=document.getElementById(id);
if(dirtyInput){dirtyInput.value=parseInt(dirtyInput.value)+1
}}}function enableCellEditor(id){disableCellEditor();
ajaxSwingCurrentCellEditElement=document.getElementById(id+"editor");
if(ajaxSwingCurrentCellEditElement!=null){ajaxSwingCurrentCellEditElement.cellEditorId=id;
ajaxSwingCurrentCellEditElement.style.display="block";
ajaxSwingCurrentCellEditElement.dirtyInputId=$(ajaxSwingCurrentCellEditElement).attr("dirtyInputId");
if(ajaxSwingCurrentCellEditElement.dirtyInputId){var dirtyInput=document.getElementById(ajaxSwingCurrentCellEditElement.dirtyInputId);
if(dirtyInput){dirtyInput.value=parseInt(dirtyInput.value)+1
}}if(ajaxSwingCurrentCellEditElement.parentNode){ajaxSwingCurrentCellEditElement.parentNode.backgroundBeforeEditor=ajaxSwingCurrentCellEditElement.parentNode.style.backgroundColor;
$(ajaxSwingCurrentCellEditElement.parentNode).addClass("cellEditor")
}ajaxSwingCurrentCellEditElement.renderElement=document.getElementById(id+"renderer");
ajaxSwingCurrentCellEditElement.renderElement.style.display="none";
if(!ajaxSwingCurrentCellEditElement.cellEditorBackgroundEnabled){enableCellEditorBackground(ajaxSwingCurrentCellEditElement);
ajaxSwingCurrentCellEditElement.cellEditorBackgroundEnabled=true
}saveCellEditorChildValues(ajaxSwingCurrentCellEditElement)
}}function enableCellEditorFocus(child){if(child){if(child.nodeName=="INPUT"){child.focus();
return true
}else{if(child.childNodes){for(var count=0;
count<child.childNodes.length;
count++){if(enableCellEditorFocus(child.childNodes[count])){return true
}}}}}return false
}function enableCellEditorBackground(child){if(child){if(child.childNodes){for(var count=0;
count<child.childNodes.length;
count++){enableCellEditorBackground(child.childNodes[count])
}}if(child.style){$(child).addClass("cellEditor")
}}}function updateCellEditorChildValue(child,oldValue){if(child.childNodes){for(var count=0;
count<child.childNodes.length;
count++){if(updateCellEditorChildValue(child.childNodes[count],oldValue)){return true
}}}if(ajaxSwingCurrentCellEditElement.savedIsCheckBox){if((child.nodeName=="INPUT")&&(child.getAttribute("type").toLowerCase()=="radio")){child.checked=ajaxSwingCurrentCellEditElement.savedValue
}}else{if((child.nodeName=="SPAN")&&((child.innerHTML==oldValue)||((child.innerHTML=="&nbsp;")&&(oldValue==""))||((ajaxSwingCurrentCellEditElement.savedIsNumber)&&(parseInt(child.innerHTML)==parseInt(oldValue))))){child.innerHTML=ajaxSwingCurrentCellEditElement.savedValue;
return true
}else{if((child.nodeName=="INPUT")&&(child.getAttribute("type").toLowerCase()=="text")&&((child.value==oldValue)||(((""+child.value)=="")&&(oldValue==""))||((ajaxSwingCurrentCellEditElement.savedIsNumber)&&(parseInt(child.value)==parseInt(oldValue))))){child.value=ajaxSwingCurrentCellEditElement.savedValue;
return true
}}}return false
}function saveCellEditorChildValues(child){if(child.childNodes){for(var count=0;
count<child.childNodes.length;
count++){saveCellEditorChildValues(child.childNodes[count])
}}if(child.nodeName=="SELECT"){ajaxSwingCurrentCellEditElement.savedValue=child.options[child.selectedIndex].text
}if(child.nodeName=="INPUT"){if(child.getAttribute("type").toLowerCase()=="radio"){ajaxSwingCurrentCellEditElement.savedValue=child.checked;
ajaxSwingCurrentCellEditElement.savedIsCheckBox=true
}else{if(!(child.getAttribute("type").toLowerCase()=="button")){ajaxSwingCurrentCellEditElement.savedValue=child.value;
if(!(isNaN(parseInt(child.value)))){ajaxSwingCurrentCellEditElement.savedIsNumber=true
}}}}}function setRowColumnBackground(rowObject,background){$("input, textarea",rowObject).css("background-color",background)
}function getScrollX(el){if(!el){return 0
}if(el.scrollLeft){return getScrollX(el.parentNode)+el.scrollLeft
}return getScrollX(el.parentNode)
}function getScrollY(el){if(!el){return 0
}if(el.scrollTop){return getScrollY(el.parentNode)+el.scrollTop
}return getScrollY(el.parentNode)
}function initResizeColumnOnTable(table){var tid=table.id;
var hederId=$(table).attr("headerId");
var oldSelect=document.onselectstart;
table.zLeft=xPageX(document.getElementById(tid+"Table"));
$("#"+hederId+" .tableColumnResize").bind("mousedown",function(e){document.onselectstart=function(){return false
};
var th=$(this).parent();
var left=e.clientX-table.zLeft+getScrollX(table);
z.resizeStart(this,th,left)
});
var z_sel="vertical-resize-divider"+new Date().getTime();
var z=$('<div id="'+z_sel+'"></div>').css({backgroundColor:"#ababab",height:"100%",width:"4px",position:"absolute",zIndex:"10",display:"block"}).extend({resizeStart:function(cfg,th,eventX){var pos=th.offset();
pos.top=xPageY(th);
pos.left=xPageX(th);
$(this).show().css({top:0,left:eventX});
$("body").bind("mousemove",{col:th},function(e){var th=e.data.col;
var pos=th.offset();
var col_w=e.clientX-(pos.left);
if(col_w>cfg.getAttribute("minColWidth")){if(col_w<cfg.getAttribute("maxColWidth")){var leftVal=e.clientX-table.zLeft+getScrollX(table);
$("#"+z_sel).css("left",leftVal)
}}});
$("body").bind("mouseup",{col:th},function(e){$(this).unbind("mousemove").unbind("mouseup");
$("#"+z_sel).hide();
document.onselectstart=oldSelect;
var th=e.data.col;
var pos=th.offset();
var col_w=e.clientX-pos.left;
var actionString="/event/"+table.id+"/resizeColumn/"+cfg.getAttribute("columnIndex")+"_";
if(col_w>cfg.getAttribute("minColWidth")){if(col_w<cfg.getAttribute("maxColWidth")){doSubmit(actionString+col_w)
}else{doSubmit(actionString+cfg.getAttribute("maxColWidth"))
}}else{doSubmit(actionString+cfg.getAttribute("minColWidth"))
}})
}});
var g=$(table);
var gtbody=$(table).find("tbody");
if(gtbody){g=gtbody
}g.append(z.hide()).extend({z:z})
}var defaultDateFormat="MM/DD/YYYY";
var retaiendFocusInputName="";
function validateCompleteForm(objForm,strErrorClass){return _validateInternal(objForm,strErrorClass,0)
}function validateStandard(objForm,strErrorClass){return _validateInternal(objForm,strErrorClass,1)
}function _validateInternal(form,strErrorClass,nErrorThrowType){var strErrorMessage="";
var objFirstError=null;
if(nErrorThrowType==0){strErrorMessage=(form.err)?form.err:_getLanguageText("err_form")
}var fields=_GenerateFormFields(form);
for(var i=0;
i<fields.length;
++i){var field=fields[i];
if(!field.IsValid(fields)){field.SetClass(strErrorClass);
if(nErrorThrowType==1){_throwError(field);
return false
}else{if(objFirstError==null){objFirstError=field
}strErrorMessage=_handleError(field,strErrorMessage);
bError=true
}}else{field.ResetClass()
}}if(objFirstError!=null){alert(strErrorMessage);
objFirstError.element.focus();
return false
}return true
}function _getLanguageText(id){objTextsInternal=new _jsVal_Language();
objTexts=null;
try{objTexts=new jsVal_Language()
}catch(ignored){}switch(id){case"err_form":strResult=(!objTexts||!objTexts.err_form)?objTextsInternal.err_form:objTexts.err_form;
break;
case"err_enter":strResult=(!objTexts||!objTexts.err_enter)?objTextsInternal.err_enter:objTexts.err_enter;
break;
case"err_select":strResult=(!objTexts||!objTexts.err_select)?objTextsInternal.err_select:objTexts.err_select;
break
}return strResult
}function _GenerateFormFields(form){var arr=new Array();
for(var i=0;
i<form.length;
++i){var element=form.elements[i];
var index=_getElementIndex(arr,element);
if(index==-1){arr[arr.length]=new Field(element,form)
}else{arr[index].Merge(element)
}}return arr
}function _getElementIndex(arr,element){if(element.name){var elementName=element.name.toLowerCase();
for(var i=0;
i<arr.length;
++i){if(arr[i].element.name){if(arr[i].element.name.toLowerCase()==elementName){return i
}}}}return -1
}function _jsVal_Language(){this.err_form="Please enter/select values for the following fields:\n\n";
this.err_select='Please select a valid "%FIELDNAME%"';
this.err_enter='Please enter a valid "%FIELDNAME%"'
}function Field(element,form){this.type=element.type;
this.element=element;
this.exclude=element.exclude||element.getAttribute("exclude");
this.err=element.err||element.getAttribute("err");
this.required=_parseBoolean(element.required||element.getAttribute("required"));
this.realname=element.realname||element.getAttribute("realname");
this.elements=new Array();
switch(this.type){case"textarea":case"password":case"text":case"file":this.value=element.value;
this.minLength=element.minlength||element.getAttribute("minlength");
this.maxLength=element.maxlength||element.getAttribute("maxlength");
this.regexp=this._getRegEx(element);
this.intMinValue=element.intminvalue||element.getAttribute("intminvalue");
this.intMaxValue=element.intmaxvalue||element.getAttribute("intmaxvalue");
this.floatMinValue=element.floatminvalue||element.getAttribute("floatminvalue");
this.floatMaxValue=element.floatmaxvalue||element.getAttribute("floatmaxvalue");
this.dateMinValue=element.dateminvalue||element.getAttribute("dateminvalue");
this.dateMaxValue=element.datemaxvalue||element.getAttribute("datemaxvalue");
this.equals=element.equals||element.getAttribute("equals");
this.callback=element.callback||element.getAttribute("callback");
break;
case"select-one":case"select-multiple":this.values=new Array();
for(var i=0;
i<element.options.length;
++i){if(element.options[i].selected&&(!this.exclude||element.options[i].value!=this.exclude)){this.values[this.values.length]=element.options[i].value
}}this.min=element.min||element.getAttribute("min");
this.max=element.max||element.getAttribute("max");
this.equals=element.equals||element.getAttribute("equals");
break;
case"checkbox":this.min=element.min||element.getAttribute("min");
this.max=element.max||element.getAttribute("max");
case"radio":this.required=_parseBoolean(this.required||element.getAttribute("required"));
this.values=new Array();
if(element.checked){this.values[0]=element.value
}this.elements[0]=element;
break
}}Field.prototype.Merge=function(element){var required=_parseBoolean(element.getAttribute("required"));
if(required){this.required=true
}if(!this.err){this.err=element.getAttribute("err")
}if(!this.equals){this.equals=element.getAttribute("equals")
}if(!this.callback){this.callback=element.getAttribute("callback")
}if(!this.realname){this.realname=element.getAttribute("realname")
}if(!this.max){this.max=element.getAttribute("max")
}if(!this.min){this.min=element.getAttribute("min")
}if(!this.regexp){this.regexp=this._getRegEx(element)
}if(element.checked){this.values[this.values.length]=element.value
}this.elements[this.elements.length]=element
};
Field.prototype.IsValid=function(arrFields){switch(this.type){case"textarea":case"password":case"text":case"file":return this._ValidateText(arrFields);
case"select-one":case"select-multiple":case"radio":case"checkbox":return this._ValidateGroup(arrFields);
default:return true
}};
Field.prototype.SetClass=function(newClassName){if((newClassName)&&(newClassName!="")){if((this.elements)&&(this.elements.length>0)){for(var i=0;
i<this.elements.length;
++i){if(this.elements[i].className!=newClassName){this.elements[i].oldClassName=this.elements[i].className;
this.elements[i].className=newClassName
}}}else{if(this.element.className!=newClassName){this.element.oldClassName=this.element.className;
this.element.className=newClassName
}}}};
Field.prototype.ResetClass=function(){if((this.type!="button")&&(this.type!="submit")&&(this.type!="reset")){if((this.elements)&&(this.elements.length>0)){for(var i=0;
i<this.elements.length;
++i){if(this.elements[i].oldClassName){this.elements[i].className=this.elements[i].oldClassName
}else{this.element.className=""
}}}else{if(this.element.oldClassName){this.element.className=this.element.oldClassName
}else{this.element.className=""
}}}};
Field.prototype._getRegEx=function(element){regex=element.regexp||element.getAttribute("regexp");
if(regex==null){return null
}retype=typeof (regex);
if(retype.toUpperCase()=="FUNCTION"){return regex
}else{if((retype.toUpperCase()=="STRING")&&!(regex=="JSVAL_RX_EMAIL")&&!(regex=="JSVAL_RX_TEL")&&!(regex=="JSVAL_RX_PC")&&!(regex=="JSVAL_RX_ZIP")&&!(regex=="JSVAL_RX_MONEY")&&!(regex=="JSVAL_RX_CREDITCARD")&&!(regex=="JSVAL_RX_POSTALZIP")&&!(regex=="JSVAL_RX_DATE")&&!(regex=="JSVAL_RX_INT")&&!(regex=="JSVAL_RX_FLOAT")){nBegin=0;
nEnd=0;
if(regex.charAt(0)=="/"){nBegin=1
}if(regex.charAt(regex.length-1)=="/"){nEnd=0
}return new RegExp(regex.slice(nBegin,nEnd))
}else{return regex
}}};
Field.prototype._ValidateText=function(arrFields){if((this.required)&&(this.callback)){nCurId=this.element.id?this.element.id:"";
nCurName=this.element.name?this.element.name:"";
eval("bResult = "+this.callback+"('"+nCurId+"', '"+nCurName+"', '"+this.value+"');");
if(bResult==false){return false
}}else{if(this.required&&!this.value){return false
}if(this.value&&(this.minLength&&this.value.length<this.minLength)){return false
}if(this.value&&(this.maxLength&&this.value.length>this.maxLength)){return false
}if(this.regexp){if(!_checkRegExp(this.regexp,this.value)){if(!this.required&&this.value){return false
}if(this.required){return false
}}}if(this.equals){for(var i=0;
i<arrFields.length;
++i){var field=arrFields[i];
if((field.element.name==this.equals)||(field.element.id==this.equals)){if(field.element.value!=this.value){return false
}break
}}}if(this.required){var fValue=parseFloat(this.value);
if((this.floatMinValue||this.floatMaxValue)&&isNaN(fValue)){return false
}if((this.floatMinValue)&&(fValue<this.floatMinValue)){return false
}if((this.floatMaxValue)&&(fValue>this.floatMaxValue)){return false
}var iValue=parseInt(this.value);
if((this.intMinValue||this.intMaxValue)&&isNaN(iValue)){return false
}if((this.intMinValue)&&(iValue<this.intMinValue)){return false
}if((this.intMaxValue)&&(iValue>this.intMaxValue)){return false
}var dValue=this.value;
if((this.dateMinValue||this.dateMaxValue)&&!_isDate(dValue,defaultDateFormat)){return false
}if(((this.dateMinValue)&&!_isDate(this.dateMinValue,defaultDateFormat))||((this.dateMinValue)&&_isDate(this.dateMinValue,defaultDateFormat)&&(_conmpareTwoDates(this.dateMinValue,dValue)==-1))){return false
}if(((this.dateMaxValue)&&!_isDate(this.dateMaxValue,defaultDateFormat))||((this.dateMaxValue)&&_isDate(this.dateMaxValue,defaultDateFormat)&&(_conmpareTwoDates(this.dateMaxValue,dValue)==1))){return false
}}}return true
};
Field.prototype._ValidateGroup=function(arrFields){if(this.required&&this.values.length==0){return false
}if(this.required&&this.min&&this.min>this.values.length){return false
}if(this.required&&this.max&&this.max<this.values.length){return false
}return true
};
function _handleError(field,strErrorMessage){var obj=field.element;
strNewMessage=strErrorMessage+((field.realname)?field.realname:((obj.id)?obj.id:obj.name))+"\n";
return strNewMessage
}function _throwError(field){var obj=field.element;
switch(field.type){case"text":case"password":case"textarea":case"file":alert(_getError(field,"err_enter"));
try{obj.focus()
}catch(ignore){}break;
case"select-one":case"select-multiple":case"radio":case"checkbox":alert(_getError(field,"err_select"));
break
}}function _getError(field,str){var obj=field.element;
strErrorTemp=(field.err)?field.err:_getLanguageText(str);
idx=strErrorTemp.indexOf("\\n");
while(idx>-1){strErrorTemp=strErrorTemp.replace("\\n","\n");
idx=strErrorTemp.indexOf("\\n")
}return strErrorTemp.replace("%FIELDNAME%",(field.realname)?field.realname:((obj.id)?obj.id:obj.name))
}function _parseBoolean(value){return !(!value||value==0||value=="0"||value=="false")
}function _checkRegExp(regx,value){switch(regx){case"JSVAL_RX_EMAIL":return((/^[a-zA-Z]+([\.-]?[a-zA-Z]+)*@[a-zA-Z]+([\.-]?[a-zA-Z]+)*(\.\w{2,5})+$/).test(value));
case"JSVAL_RX_TEL":return((/^1?[\-]?\(?\d{3}\)?[\-]?\d{3}[\-]?\d{4}$/).test(value));
case"JSVAL_RX_PC":return((/^[a-z]\d[a-z]?\d[a-z]\d$/i).test(value));
case"JSVAL_RX_ZIP":return((/^\d{5}$/).test(value));
case"JSVAL_RX_MONEY":return((/^\d+([\.]\d\d)?$/).test(value));
case"JSVAL_RX_CREDITCARD":return(!isNaN(value));
case"JSVAL_RX_DATE":return _isDate(value,defaultDateFormat);
case"JSVAL_RX_POSTALZIP":if(value.length==6||value.length==7){return((/^[a-zA-Z]\d[a-zA-Z] ?\d[a-zA-Z]\d$/).test(value))
}if(value.length==5||value.length==10){return((/^\d{5}(\-\d{4})?$/).test(value))
}break;
case"JSVAL_RX_INT":return((/^[+-]?(\d{1,3})(,?\d{3})*$/).test(value));
case"JSVAL_RX_FLOAT":return((/^[+-]?(\d*(,?\d{3})*\.?\d+|\d+(,?\d{3})*\.?\d*)$/).test(value));
default:return(regx.test(value))
}}function _validateElement(element,strErrorClass,retainFocus){var strErrorMessage="";
var objFirstError=null;
var form=element.form;
var field=new Field(element,form);
var fields=new Array();
fields[0]=field;
_hideErrorDiv(element.name);
if(!element.oldClassName){element.oldClassName=element.className
}if(!field.IsValid(fields)){if(retaiendFocusInputName==""||retaiendFocusInputName==element.name){if(retainFocus==true){retaiendFocusInputName=element.name
}if(element.className.indexOf(strErrorClass)==-1){field.SetClass(element.className+" "+strErrorClass)
}_throwErrorInDiv(field,retainFocus)
}}else{retaiendFocusInputName="";
field.ResetClass()
}}function _throwErrorInDiv(field,retainFocus){var element=field.element;
var errorText;
switch(field.type){case"text":case"password":case"textarea":case"file":errorText=_getError(field,"err_enter");
break;
case"select-one":case"select-multiple":case"radio":case"checkbox":errorText=_getError(field,"err_select");
break
}_showErrorDiv(element,errorText,retainFocus);
setTimeout("_hideErrorDiv('"+element.name+"')",4000)
}function _showErrorDiv(element,errorText,retainFocus){var errorDiv=document.createElement("div");
errorDiv.setAttribute("id",element.name+"_error");
var parentDiv=document.getElementById("AjaxSwingScreen");
errorDiv.style.display="";
errorDiv.style.position="absolute";
errorDiv.style.zIndex=10000000;
errorDiv.style.left=(_elementAbsoluteLeftPosition(element,parentDiv)+parseInt(element.style.width)+6)+"px";
errorDiv.style.top=(_elementAbsoluteTopPosition(element,parentDiv)+6)+"px";
errorDiv.innerHTML="<span class='validationMessage'>"+errorText+"</span>";
errorDiv.style.width="auto";
parentDiv.appendChild(errorDiv);
if(retainFocus==true){setTimeout("_setFocus('"+element.name+"')",0)
}}function _setFocus(elementName){document.getElementById(elementName).focus()
}function _hideErrorDiv(elementName){if(document.getElementById(elementName+"_error")){var parentDiv=document.getElementById("AjaxSwingScreen");
parentDiv.removeChild(document.getElementById(elementName+"_error"))
}}function _isDate(sDate,sFormat){var aDaysInMonth=new Array(31,28,31,30,31,30,31,31,30,31,30,31);
var sSepDate=sDate.charAt(sDate.search(/\D/));
var sSepFormat=sFormat.charAt(sFormat.search(/[^MDY]/i));
if(sSepDate!=sSepFormat){return false
}var aValueMDY=sDate.split(sSepDate);
var aFormatMDY=sFormat.split(sSepFormat);
var iMonth,iDay,iYear;
iMonth=aValueMDY[0];
iDay=aValueMDY[1];
iYear=aValueMDY[2];
if(!_isNum(iMonth)||!_isNum(iDay)||!_isNum(iYear)){return false
}if(iYear.length!=4){return false
}var iDaysInMonth=(iMonth!=2)?aDaysInMonth[iMonth-1]:((iYear%4==0&&iYear%100!=0||iYear%400==0)?29:28);
return(iDay!=null&&iMonth!=null&&iYear!=null&&iMonth<13&&iMonth>0&&iDay>0&&iDay<=iDaysInMonth)
}function _isNum(v){return(typeof v!="undefined"&&v.toString()&&!/\D/.test(v))
}function _elementAbsoluteLeftPosition(element,parentElement){var x=0;
var currentElement=element;
while(currentElement&&currentElement!=parentElement){x+=currentElement.offsetLeft-currentElement.scrollLeft;
currentElement=currentElement.offsetParent?currentElement.offsetParent:null
}return x
}function _elementAbsoluteTopPosition(element,parentElement){var y=0;
var currentElement=element;
while(currentElement&&currentElement!=parentElement){y+=currentElement.offsetTop-currentElement.scrollTop;
currentElement=currentElement.offsetParent?currentElement.offsetParent:null
}return y
}function _conmpareTwoDates(firstDateAsString,secondDateAsString){firstDateObj=new Date(firstDateAsString+" 00:00:00");
secondDateObj=new Date(secondDateAsString+" 00:00:00");
if(firstDateObj>secondDateObj){return -1
}else{if(firstDateObj==secondDateObj){return 0
}else{return 1
}}}AjaxSwingWindow.z=1000;
AjaxSwingWindow.instances={};
function checkWindowSize(){if(!window.ajaxSwing.lastClientWidth){var maximizedFrame=$(".frame-maximized .frame");
if($(maximizedFrame).size()>0){window.ajaxSwing.lastClientWidth=$(maximizedFrame).width();
window.ajaxSwing.lastClientHeight=$(maximizedFrame).height()
}}if(window.ajaxSwing.lastClientWidth||(window.ajaxSwing.lastClientWidth==0)){if(Math.abs(window.ajaxSwing.lastClientWidth-getClientWidth())>3||Math.abs(window.ajaxSwing.lastClientHeight-getClientHeight())>3){scheduleSubmitResize()
}}}function AjaxSwingWindow(eleId,iniX,iniY,isMaximized,isDynamicEventsEnabled,barId,resBtnId,maxBtnId,closeBtnId){var me=this;
var closeBtnId=closeBtnId;
var x,y,w,h,maximized=false;
this.onunload=function(){var ele=xGetElementById(eleId);
var maximizeButton=xGetElementById(maxBtnId);
var resizeButton=xGetElementById(resBtnId);
if(!window.opera){xDisableDrag(titleBar);
xDisableDrag(resizeButton);
if(maximizeButton){maximizeButton.onclick=ele.onmousedown=null
}me=ele=resizeButton=maximizeButton=null
}ele=resizeButton=maximizeButton=null
};
this.paint=function(){var ele=xGetElementById(eleId);
var resizeButton=xGetElementById(resBtnId);
var maximizeButton=xGetElementById(maxBtnId);
xMoveTo(resizeButton,xWidth(ele)-xWidth(resizeButton)-2,xHeight(ele)-xHeight(resizeButton)-2);
var currentWidth=2;
if(closeBtnId!=null){var closeBtn=xGetElementById(closeBtnId);
currentWidth+=xWidth(closeBtn);
xMoveTo(closeBtn,xWidth(ele)-currentWidth,-1);
currentWidth+=2
}if(maximizeButton){currentWidth+=xWidth(maximizeButton)
}xMoveTo(maximizeButton,xWidth(ele)-currentWidth,-1);
ele=resizeButton=maximizeButton=null
};
function barOnDrag(e,mdx,mdy){var ele=xGetElementById(eleId);
var x=xLeft(ele)+mdx;
var y=xTop(ele)+mdy;
var parent=ele.parentNode;
var __checkForParentOnDialogDrag=true;
if(__checkForParentOnDialogDrag&&parent){var parentX=xLeft(parent);
var parentY=xTop(parent);
var xr=xWidth(ele);
var parentYr=xHeight(parent);
var parentXr=xWidth(parent);
if((x+xr-50)<parentX){x=parentX-xr+50
}if(y<parentY){y=parentY
}if((y+20)>(parentY+parentYr)){y=parentY+parentYr-20
}if((x+50)>(parentX+parentXr)){x=parentX+parentXr-50
}}xMoveTo(ele,x,y);
ele=null
}function barOnDragStart(){ajaxSwingShadowDestroy(xGetElementById(eleId))
}function barOnDragEnd(e,mdx,mdy){ajaxSwingShadow(xGetElementById(eleId))
}function resOnDragStart(){ajaxSwingShadowDestroy(xGetElementById(eleId))
}function resOnDrag(e,mdx,mdy){var ele=xGetElementById(eleId);
xResizeTo(ele,xWidth(ele)+mdx,xHeight(ele)+mdy);
ele=null;
me.paint()
}function resOnDragEnd(e,mdx,mdy){if(me.dynamicEventsEnabled){doSubmit("update")
}else{ajaxSwingShadow(xGetElementById(eleId))
}}function maxOnClick(initializing){var ele=xGetElementById(eleId);
var resizeButton=xGetElementById(resBtnId);
if(maximized){maximized=false;
xResizeTo(ele,w,h);
xMoveTo(ele,x,y);
xShow(resizeButton)
}else{w=xWidth(ele);
h=xHeight(ele);
x=xLeft(ele);
y=xTop(ele);
var desktopNode=getParentByClassName(ele,"desktop");
if(desktopNode!=null){maximized=true;
xMoveTo(ele,0,0);
xResizeTo(ele,xWidth(desktopNode),xHeight(desktopNode));
xHide(resizeButton)
}}me.paint();
if(me.dynamicEventsEnabled){if($(ele).attr("frame")=="true"){doSubmit("/window/"+ele.id+"/maximize")
}else{doSubmit("update")
}}}function fenOnKeydown(event){if(!event){event=window.event
}var ele=xGetElementById(eleId);
if((event.keyCode==13)&&(!event.disableSubmit)){var $defaultButton=$("div.button[@default=true]",ele);
if($defaultButton.length>0){event.returnValue=false;
event.cancel=true;
doSubmit("/button/"+$defaultButton.attr("id"))
}}else{if(event.keyCode==27&&$(ele).attr("frame")!="true"){if(!ajaxSwingCurrentCellEditElement){event.returnValue=false;
event.cancel=true;
setTimeout("doSubmit('/window/"+ele.id+"/close')",10)
}}}if(event.keyCode==27){disableCellEditor()
}}function fenOnMousedown(event){if(!event){event=window.event
}var ele=xGetElementById(eleId);
var zIndexCurrent;
if(!isModalWindowOpen()&&(zIndexCurrent=xZIndex(ele))!=AjaxSwingWindow.z-1){var windowOrder=new Array();
var possibleZIndex=new Array();
for(var e in AjaxSwingWindow.instances){var zi=xZIndex(e);
if(zi>=AjaxSwingWindow.z){AjaxSwingWindow.z=zi+1
}}for(var e in AjaxSwingWindow.instances){var zi;
if(eleId==e){zi=zIndexCurrent;
windowOrder.push({frame:ele,zIndex:zIndexCurrent,zIndexNew:(AjaxSwingWindow.z+1)})
}else{var frameEle=AjaxSwingWindow.instances[e].getEle();
var zi=xZIndex(frameEle);
windowOrder.push({frame:frameEle,zIndex:zi,zIndexNew:zi})
}possibleZIndex.push(zi)
}windowOrder.sort(function(a,b){return a.zIndexNew-b.zIndexNew
});
possibleZIndex.sort(function(a,b){return a-b
});
for(var index=0;
index<windowOrder.length;
index++){if(possibleZIndex[index]!=windowOrder[index].zIndex){xZIndex(windowOrder[index].frame,possibleZIndex[index]);
if(ajaxSwingUseShadows==true){$(windowOrder[index].frame).find("+ .fx-shadow").css("zIndex",possibleZIndex[index]-1)
}}}setAjaxSwingActiveWindow(ele)
}event.cancel=true;
event.cancelBubble=true;
if(event.stopPropagation){event.stopPropagation()
}onDocumentMouseDown(event);
onDocumentClick(event);
ele=null
}var ele=xGetElementById(eleId);
var resizeButton=xGetElementById(resBtnId);
var maximizeButton=xGetElementById(maxBtnId);
var titleBar=xGetElementById(barId);
xMoveTo(ele,iniX,iniY);
this.paint();
xEnableDrag(titleBar,barOnDragStart,barOnDrag,barOnDragEnd);
xEnableDrag(resizeButton,resOnDragStart,resOnDrag,resOnDragEnd);
if(maximizeButton){maximizeButton.onclick=maxOnClick
}xAddEventListener(ele,"mousedown",fenOnMousedown);
xAddEventListener(ele,"keydown",fenOnKeydown);
ele.AJS_onSubmit=AJS_windowWrapper(eleId);
ele.AJS_onRemove=fenOnRemove;
xShow(ele);
this.dynamicEventsEnabled=false;
this.getX=function(){return x
};
this.getY=function(){return y
};
this.getW=function(){return w
};
this.getH=function(){return h
};
this.isMaximized=function(){return maximized
};
this.setMaximized=function(max){maximized=max
};
this.getEle=function(){return xGetElementById(eleId)
};
this.getTitleBar=function(){return xGetElementById(barId)
};
AjaxSwingWindow.instances[ele.id]=this;
if(ele.style.zIndex){z=parseInt(ele.style.zIndex);
if(z>=AjaxSwingWindow.z){AjaxSwingWindow.z=z+1
}else{if(z>=100&&z<200){xZIndex(ele,AjaxSwingWindow.z+z)
}}}if(isMaximized){maxOnClick()
}titleBar=maximizeButton=resizeButton=ele=null;
this.dynamicEventsEnabled=isDynamicEventsEnabled
}function AJS_windowWrapper(eleId){return(function(){windowOnSubmit(AjaxSwingWindow.instances[eleId])
})
}function getWindowFenster(ele){if(!ele){return null
}return AjaxSwingWindow.instances[ele.id]
}function fenOnRemove(){this.onmousedown=null;
this.onkeydown=null;
AjaxSwingWindow.instances[this.id]=null;
delete AjaxSwingWindow.instances[this.id];
if(this==ajaxSwingActiveWindow){ajaxSwingActiveWindow=null
}ajaxSwingShadowDestroy(this)
}function windowOnSubmit(ajsFensterObj){var borderWidth=4;
var windowValue="";
if(ajsFensterObj.isMaximized()){windowValue+=""+ajsFensterObj.getX();
windowValue+=","+ajsFensterObj.getY();
windowValue+=","+(ajsFensterObj.getW()-borderWidth);
windowValue+=","+(ajsFensterObj.getH()-borderWidth)
}else{windowValue+=""+xLeft(ajsFensterObj.getEle());
windowValue+=","+xTop(ajsFensterObj.getEle());
windowValue+=","+(xWidth(ajsFensterObj.getEle())-borderWidth);
windowValue+=","+(xHeight(ajsFensterObj.getEle())-borderWidth)
}windowValue+=",";
var zIndex=xZIndex(ajsFensterObj.getEle());
if(zIndex){windowValue+=zIndex
}windowValue+=","+ajsFensterObj.isMaximized();
windowValue+=","+(ajsFensterObj.getEle()==ajaxSwingActiveWindow);
var windowInput=xFirstChild(ajsFensterObj.getEle(),"input");
windowInput.setAttribute("value",windowValue);
windowInput=null;
ajsFensterObj=null;
windowValue=null
}function isModalWindowOpen(){if(typeof (ajaxSwingModalWindow)!="undefined"&&ajaxSwingModalWindow){return true
}return false
}$(function(){$("#AjaxSwingModalPane").css("opacity","0.7");
$("#AjaxSwingGlassPane").css("opacity","0.0")
});
function preSubmitAjaxSwingWindows(blockUI){if(blockUI){if(!$.browser.safari){document.body.style.cursor="wait"
}$("#AjaxSwingGlassPane").css("z-index",20000)
}}var ajaxSwingModalWindow=null;
var ajaxSwingActiveWindow=null;
var ajaxSwingShadowOptions={offset:5,opacity:0.1,monitor:false};
var ajaxSwingUseShadows=true;
function postSubmitAjaxSwingWindows(ajaxResponse){var newAjaxSwingModalWindow=null;
var blockRootContainer=false;
var $window=null;
var $windows=$("div.window");
for(var i=$windows.length-1;
i>=0;
i--){$window=$windows[i];
if($window.getAttribute("modal")=="true"){newAjaxSwingModalWindow=$window;
break
}}var modalPaneZ=0;
if(newAjaxSwingModalWindow!=null){modalPaneZ=$(newAjaxSwingModalWindow).css("z-index")-1
}$("#AjaxSwingModalPane").css("z-index",modalPaneZ);
$("#AjaxSwingGlassPane").css("z-index",0);
ajaxSwingModalWindow=newAjaxSwingModalWindow;
if(ajaxResponse&&ajaxResponse.activeWindowId!=null){var activeWindow=document.getElementById(ajaxResponse.activeWindowId);
setAjaxSwingActiveWindow(activeWindow)
}for(var i=0;
i<$windows.length;
i++){ajaxSwingShadow($windows[i])
}document.body.style.cursor="auto";
document.body.style.cursor="wait";
document.body.style.cursor="auto";
if($.browser.msie){$("#rootContainer").scrollTop(0)
}restoreScrollPanePositions();
var focusedComponentId="";
if(ajaxResponse&&ajaxResponse.focusedComponentId){focusedComponentId=ajaxResponse.focusedComponentId
}setTimeout("postSubmitSetFocus('"+focusedComponentId+"')",300)
}function postSubmitSetFocus(focusedComponentId){if(ajaxSwingFocusBackElement){var focusBackElement=document.getElementById(ajaxSwingFocusBackElement.id);
if(focusedComponentId&&focusedComponentId!=""){AjaxSwing_focusedComponentId=focusedComponentId
}else{AjaxSwing_focusedComponentId="inputType"
}if(focusBackElement){focusBackElement.readOnly=ajaxSwingFocusBackElement.oldReadOnly;
setFocus(ajaxSwingFocusBackElement.id)
}ajaxSwingFocusBackElement=null
}else{if(focusedComponentId&&focusedComponentId!=""&&focusedComponentId!=AjaxSwing_oldFocusedComponentId){AjaxSwing_focusedComponentId=focusedComponentId;
var focusedComponent=document.getElementById(focusedComponentId);
if(focusedComponent){if(!($.browser.msie&&$(focusedComponent).is("div"))){if(!ajaxSwingFocusBackElement){try{focusedComponent.focus();
fixWebkitFocusScroll(focusedComponent)
}catch(ex){}}}}}else{if(ajaxSwingActiveWindow){var $firstInput=$(":input[@tabindex=1]",ajaxSwingActiveWindow);
if($firstInput.length==0){$firstInput=$(":input:first[@type!=hidden]",ajaxSwingActiveWindow)
}if($firstInput.length==0){$firstInput=$(".button[default] .buttonFocus",ajaxSwingActiveWindow)
}if(($firstInput.length>0)&&(!ajaxSwingFocusBackElement)&&(!AjaxSwing_oldFocusedComponentId)&&(isScrollVisible($firstInput.get(0)))){$firstInput.focus();
if($firstInput.length>0){fixWebkitFocusScroll($firstInput.get(0))
}}AjaxSwing_focusedComponentId="inputType"
}}}AjaxSwing_oldFocusedComponentId=null
}function isScrollVisible(el){while(el&&el.parentNode){var viewTop=el.parentNode.scrollTop;
if(viewTop>0){var viewBottom=viewTop+$(el.parentNode).height();
var elemTop=el.offsetTop;
if(((elemTop>=viewTop)&&(elemTop<=viewBottom))){el=el.parentNode
}else{return false
}}else{el=el.parentNode
}}return true
}function fixWebkitFocusScroll(ele){if(ajaxSwingActiveWindow!=null){$(ajaxSwingActiveWindow).children("table").each(function(){this.scrollLeft=0;
this.scrollTop=0
})
}}function setFocus(elementId){var element=document.getElementById(elementId);
if(element.createTextRange){try{var r=element.createTextRange();
r.moveStart("character",element.value.length);
r.select()
}catch(ex){}}element.focus()
}function ajaxSwingShadow(wnd){var disableShadowForMaximized=false;
if(window.ajaxSwing&&window.ajaxSwing.lastClientHeight&&wnd){if((window.ajaxSwing.lastClientHeight>10)&&((window.ajaxSwing.lastClientHeight-10)<$(wnd).height())){disableShadowForMaximized=true
}}if((!disableShadowForMaximized)&&(ajaxSwingUseShadows==true)){if(typeof wnd.shadow=="undefined"||wnd.shadow==false){$(wnd).shadow(ajaxSwingShadowOptions);
wnd.shadow=true
}}else{if(ajaxSwingUseShadows==false){$element=$(wnd);
zIndex=parseInt($element.css("zIndex"))||0;
$element.css({zIndex:zIndex+1,position:($element.css("position")=="static"?"relative":"")})
}}}function ajaxSwingShadowDestroy(wnd){if(ajaxSwingUseShadows==true){if(typeof wnd.shadow!="undefined"&&wnd.shadow==true){wnd.shadow=false;
$(wnd).find("+ .fx-shadow").each(function(){if(ajaxSwingShadowOptions.monitor==true&&this.style.removeExpression){this.style.removeExpression("left");
this.style.removeExpression("top")
}});
$(wnd).shadowDestroy()
}}}function setAjaxSwingActiveWindow(ele){if(ajaxSwingActiveWindow!=ele){var fenster=getWindowFenster(ajaxSwingActiveWindow);
if(ajaxSwingActiveWindow!=null&&fenster){$(fenster.getTitleBar()).removeClass("windowBar-selected")
}ajaxSwingActiveWindow=ele;
fenster=getWindowFenster(ajaxSwingActiveWindow);
if(ajaxSwingActiveWindow!=null&&fenster){$(fenster.getTitleBar()).addClass("windowBar-selected")
}fenster=null
}}function AjaxSwingSplitter(sSplId,uSplX,uSplY,uSplW,uSplH,bHorizontal,uBarW,uBarPos,uBarLimit,bBarEnabled,uSplBorderW,oSplChild1,oSplChild2,bSubmitOnSplitterResize){var pane1,pane2,splW,splH;
var splEle,barPos,barLim,barEle;
var splitterId,submitOnSplitterResize;
function barOnDrag(ele,dx,dy){var bp;
if(bHorizontal){borderSize=pane1.nodeName=="INPUT"?2:0;
bp=barPos+dx;
if(bp<uBarLimit||bp>splW-uBarLimit){return 
}xWidth(pane1,xWidth(pane1)+borderSize+dx);
xLeft(barEle,xLeft(barEle)+dx);
borderSize=pane2.nodeName=="INPUT"?2:0;
xWidth(pane2,xWidth(pane2)+borderSize-dx);
xLeft(pane2,xLeft(pane2)+dx);
barPos=bp
}else{borderSize=pane2.nodeName=="INPUT"?2:0;
bp=barPos+dy;
if(bp<uBarLimit||bp>splH-uBarLimit){return 
}borderSize=pane1.nodeName=="INPUT"?2:0;
xHeight(pane1,xHeight(pane1)+borderSize+dy);
xTop(barEle,xTop(barEle)+dy);
xHeight(pane2,xHeight(pane2)+borderSize-dy);
xTop(pane2,xTop(pane2)+dy);
barPos=bp
}document.AjaxSwingForm.elements[splitterId].value=barPos+1;
if(oSplChild1){oSplChild1.paint(xWidth(pane1),xHeight(pane1))
}if(oSplChild2){oSplChild2.paint(xWidth(pane2),xHeight(pane2))
}}function barOnDrop(ele,mouseX,mouseY,xEventObj){if(submitOnSplitterResize==true){var id=ele.id.substring(0,ele.id.length-"_Splitter".length);
doSubmit("update",false)
}}this.paint=function(uNewW,uNewH,uNewBarPos,uNewBarLim){if(uNewW==0){return 
}var w1,h1,w2,h2;
splW=uNewW;
splH=uNewH;
barPos=uNewBarPos||barPos;
barLim=uNewBarLim||barLim;
xMoveTo(splEle,uSplX,uSplY);
xResizeTo(splEle,uNewW,uNewH);
if(bHorizontal){w1=barPos;
h1=uNewH-2*uSplBorderW;
w2=uNewW-w1-uBarW-2*uSplBorderW;
h2=h1;
xMoveTo(pane1,0,0);
xResizeTo(pane1,w1,h1);
xMoveTo(barEle,w1,0);
xResizeTo(barEle,uBarW,h1);
xMoveTo(pane2,w1+uBarW,0);
xResizeTo(pane2,w2,h2)
}else{w1=uNewW-2*uSplBorderW;
h1=barPos;
w2=w1;
h2=uNewH-h1-uBarW-2*uSplBorderW;
xMoveTo(pane1,0,0);
xResizeTo(pane1,w1,h1);
xMoveTo(barEle,0,h1);
xResizeTo(barEle,w1,uBarW);
xMoveTo(pane2,0,h1+uBarW);
xResizeTo(pane2,w2,h2)
}if(oSplChild1){pane1.style.overflow="hidden";
oSplChild1.paint(w1,h1)
}if(oSplChild2){pane2.style.overflow="hidden";
oSplChild2.paint(w2,h2)
}};
splEle=xGetElementById(sSplId);
pane1=xFirstChild(splEle);
barEle=xGetElementById(sSplId+"_Splitter");
pane2=xNextSib(barEle);
barEle.style.zIndex=20000;
barPos=uBarPos;
barLim=uBarLimit;
splitterId=sSplId;
submitOnSplitterResize=bSubmitOnSplitterResize;
this.paint(uSplW,uSplH);
if(bBarEnabled){xEnableDrag(barEle,null,barOnDrag,barOnDrop);
barEle.style.cursor=bHorizontal?"e-resize":"n-resize"
}splEle.style.visibility="visible"
}var isIE=(navigator.userAgent.indexOf("MSIE")>0)?1:0;
var isNS6=(navigator.userAgent.indexOf("Gecko")>0)?1:0;
function Folder(isOpen,bundleName){this.hreference=0;
this.id=-1;
this.navObj=0;
this.textObj=0;
this.isLastNode=0;
this.tree=0;
this.children=new Array;
this.nChildren=0;
this.isVisible=true;
if(isOpen==false){this.isOpen=false
}else{this.isOpen=true
}if(typeof bundleName!="undefined"){this.bundleName=bundleName
}else{this.bundleName="default"
}this.initialize=initializeFolder;
this.setState=setStateFolder;
this.add=addChild;
this.createIndex=createEntryIndex;
this.hide=hideFolder;
this.display=display;
this.totalHeight=totalHeight;
this.subEntries=folderSubEntries;
this.initElementRefs=initElementRefs;
this.setSelected=setSelected;
this.setTree=setTree
}function setStateFolder(isOpen){var subEntries;
var totalHeight;
var fIt=0;
var i=0;
if(isOpen==this.isOpen){return 
}this.isOpen=isOpen;
propagateChangesInState(this)
}function propagateChangesInState(folder){var i=0;
folder.initElementRefs();
if(folder.isOpen){if(folder.isLastNode){if((folder.id==0&&folder.tree.isRootVisible)||(folder.id==1&&!folder.tree.isRootVisible)){setFolderImage(folder,"IMG_ROOT_LAST_FOLDER_EXPANDED")
}else{setFolderImage(folder,"IMG_LAST_FOLDER_EXPANDED")
}}else{if((folder.id==0&&folder.tree.isRootVisible)||(folder.id==1&&!folder.tree.isRootVisible)){setFolderImage(folder,"IMG_ROOT_FOLDER_EXPANDED")
}else{setFolderImage(folder,"IMG_FOLDER_EXPANDED")
}}for(i=0;
i<folder.nChildren;
i++){folder.children[i].initElementRefs();
folder.children[i].display()
}}else{if(folder.isLastNode){if(folder.id!=0){setFolderImage(folder,"IMG_LAST_FOLDER_COLLAPSED")
}else{setFolderImage(folder,"IMG_ROOT_LAST_FOLDER_COLLAPSED")
}}else{if(folder.id!=0){setFolderImage(folder,"IMG_FOLDER_COLLAPSED")
}else{setFolderImage(folder,"IMG_ROOT_FOLDER_COLLAPSED")
}}for(i=0;
i<folder.nChildren;
i++){folder.children[i].initElementRefs();
folder.children[i].hide(folder)
}}var stateObjs=$(folder.navObj).find(".treeFolderState");
if(stateObjs){if(stateObjs.size()==2){if($(stateObjs[0]).attr("expand")==(""+folder.isOpen)){stateObjs[0].style.display="block";
stateObjs[1].style.display="none"
}else{stateObjs[0].style.display="none";
stateObjs[1].style.display="block"
}}}}function setFolderImage(folder,imageName){var images=$(folder.navObj).children("img");
var imgElement=images[images.size()-1];
if(!ajaxSwingTreeImageBundles[folder.bundleName]){alert("Error: tree image bundle is not registered: "+folder.bundleName)
}else{imgElement.src=ajaxSwingTreeImageBundles[folder.bundleName][imageName]
}}function hideFolder(visibleParent){this.isVisible=false;
if(this.navObj.style.display=="none"){return 
}this.navObj.style.display="none";
if(this.id==this.tree.getSelectedItemId()){this.tree.setSelectedItemId(visibleParent.id)
}var i=0;
while(i<this.nChildren){this.children[i].initElementRefs();
this.children[i].hide(visibleParent);
i++
}}function initializeFolder(level,lastNode,leftSide){var j=0;
var i=0;
this.createIndex();
this.isLastNode=lastNode;
if(this.nChildren>0){level=level+1;
for(i=0;
i<this.nChildren;
i++){this.children[i].setTree(this.tree);
if(i==this.nChildren-1){this.children[i].initialize(level,1,leftSide)
}else{this.children[i].initialize(level,0,leftSide)
}}}}function addChild(childNode){this.children[this.nChildren]=childNode;
this.nChildren++;
childNode.setTree(this.tree);
if(this.isVisible&&this.isOpen){childNode.isVisible=true
}else{childNode.isVisible=false
}return childNode
}function folderSubEntries(){var i=0;
var se=this.nChildren;
for(i=0;
i<this.nChildren;
i++){if(this.children[i].children){se=se+this.children[i].subEntries()
}}return se
}function Item(bundleName){this.id=-1;
this.navObj=0;
this.textObj=0;
this.tree=0;
this.isVisible=true;
if(typeof bundleName!="undefined"){this.bundleName=bundleName
}else{this.bundleName="default"
}this.initialize=initializeItem;
this.createIndex=createEntryIndex;
this.hide=hideItem;
this.display=display;
this.totalHeight=totalHeight;
this.initElementRefs=initElementRefs;
this.setSelected=setSelected;
this.setTree=setTree
}function hideItem(visibleParent){this.isVisible=false;
if(this.navObj.style.display=="none"){return 
}this.navObj.style.display="none";
if(this.id==this.tree.getSelectedItemId()){this.tree.setSelectedItemId(visibleParent.id)
}}function initializeItem(level,lastNode,leftSide){this.createIndex()
}function initElementRefs(){if(this.navObj){return 
}if(browserVersion==1){this.navObj=doc.getElementById(this.tree.name+"_N"+this.id);
this.textObj=doc.getElementById(this.tree.name+"_T"+this.id)
}else{if(browserVersion==2){this.textObj=document.layers[this.tree.name+"_T"+this.id]
}}}function setTree(tree){this.tree=tree
}function setSelected(isSelected){this.initElementRefs();
if(!this.textObj){return 
}var cb=doc.getElementById(this.tree.name+"_S"+this.id);
if(cb){if(cb.checked){isSelected=true
}else{isSelected=false
}}if(isSelected==true){if(browserVersion==1){this.textObj.className="treeSel"
}else{this.textObj.document.bgColor=document.classes.treeSel.all.backgroundColor
}}else{if(browserVersion==1){this.textObj.className="treeReg"
}else{this.textObj.document.bgColor=document.classes.window.all.backgroundColor
}}}function display(){this.isVisible=true;
this.navObj.style.display="block";
if(this.children&&this.isOpen==true){var i=0;
while(i<this.nChildren){this.children[i].display();
i++
}}}function createEntryIndex(){this.id=this.tree.nEntries;
this.tree.indexOfEntries[this.tree.nEntries]=this;
this.tree.nEntries++
}function totalHeight(){var h=this.navObj.clip.height;
var i=0;
if(this.isOpen){for(i=0;
i<this.nChildren;
i++){h=h+this.children[i].totalHeight()
}}return h
}function ClFl(treeId,folderId){var clickedFolder=0;
var state=0;
var tree=ajaxSwingTrees[treeId];
clickedFolder=tree.indexOfEntries[folderId];
state=clickedFolder.isOpen;
disableCellEditor();
if(clickedFolder.setState){clickedFolder.setState(!state)
}}function ClIt(treeId,itemId){if(itemId!=ajaxSwingTrees[treeId].selectedItemId){disableCellEditor()
}ajaxSwingTrees[treeId].setSelectedItemId(itemId)
}function RClIt(treeId,itemId,source,menuName,event){if(getEventSource(event)==eventSource||typeof (event.cancelBubbleOverride)!="undefined"){return false
}event.cancelBubble=true;
event.cancelBubbleOverride=true;
event.savedX=event.clientX;
event.savedY=event.clientY;
ajaxSwingTrees[treeId].setSelectedItemId(itemId);
if(typeof source.context=="undefined"||source.context==null){source.context=new Object()
}source.context.treeId=treeId;
source.context.itemId=itemId;
return buttonClick(source,menuName,event)
}function RClTree(treeId,source,menuName,event){if(getEventSource(event)==eventSource||typeof (event.cancelBubbleOverride)!="undefined"){return false
}if(typeof source.context=="undefined"||source.context==null){source.context=new Object()
}source.context.treeId=treeId;
source.context.itemId="-1";
return buttonClick(source,menuName,event)
}function AjaxSwingTree_globalInit(){if(ajaxSwingTreeImageBundles["default"]==null){ajaxSwingTreeImageBundles["default"]=createAjaxSwingTreeDefaultImageBundle();
if(doc.all){browserVersion=1
}else{if(doc.layers){browserVersion=2
}else{browserVersion=1
}}if(browserVersion==2){prefix+="ns/"
}}}function createAjaxSwingTreeDefaultImageBundle(){var bundle=new Object();
bundle.IMG_BLANK=getDocsURL()+"/images/ajaxswing/tree/Blank.gif";
bundle.IMG_LINE=getDocsURL()+"/images/ajaxswing/tree/Line.gif";
bundle.IMG_ITEM=getDocsURL()+"/images/ajaxswing/tree/Item.gif";
bundle.IMG_LAST_ITEM=getDocsURL()+"/images/ajaxswing/tree/LastItem.gif";
bundle.IMG_FOLDER_EXPANDED=getDocsURL()+"/images/ajaxswing/tree/FolderOpen.gif";
bundle.IMG_FOLDER_COLLAPSED=getDocsURL()+"/images/ajaxswing/tree/FolderClosed.gif";
bundle.IMG_LAST_FOLDER_EXPANDED=getDocsURL()+"/images/ajaxswing/tree/FolderLastOpen.gif";
bundle.IMG_LAST_FOLDER_COLLAPSED=getDocsURL()+"/images/ajaxswing/tree/FolderLastClosed.gif";
bundle.IMG_ROOT_FOLDER_EXPANDED=getDocsURL()+"/images/ajaxswing/tree/RootFolderOpen.gif";
bundle.IMG_ROOT_FOLDER_COLLAPSED=getDocsURL()+"/images/ajaxswing/tree/RootFolderClosed.gif";
bundle.IMG_ROOT_LAST_FOLDER_EXPANDED=getDocsURL()+"/images/ajaxswing/tree/RootLastFolderOpen.gif";
bundle.IMG_ROOT_LAST_FOLDER_COLLAPSED=getDocsURL()+"/images/ajaxswing/tree/RootLastFolderClosed.gif";
return bundle
}function AjaxSwingTree(name,root,rootVisible,selectedId,hasListeners){AjaxSwingTree_globalInit();
this.name=name;
this.indexOfEntries=new Array;
this.nEntries=0;
this.root=root;
this.id=name;
this.selectedItemId=selectedId;
this.isRootVisible=false;
this.hasListeners=hasListeners;
ajaxSwingTrees[name]=this;
this.isEditable=false;
this.root.setTree(this);
if(rootVisible){this.isRootVisible=true
}this.init=AjaxSwingTree_init;
this.getSelectedItemId=AjaxSwingTree_getSelectedItemId;
this.setSelectedItemId=AjaxSwingTree_setSelectedItemId;
this.updateData=AjaxSwingTree_updateData;
this.init();
if(this.indexOfEntries[selectedId]){this.indexOfEntries[selectedId].setSelected(true)
}}function AjaxSwingTree_init(){this.isEditable=true;
this.root.initialize(0,1,"");
if(browserVersion==2){this.indexOfEntries[this.selectedItemId].setSelected(true)
}}function AjaxSwingTree_getSelectedItemId(){return this.selectedItemId
}function treeClickM(treeName,nodeID,event){if(event.ctrlKey){treeClickSEmulate(treeName,nodeID);
var tree=ajaxSwingTrees[treeName];
if(tree){tree.indexOfEntries[nodeID].setSelected(true)
}}else{if(event.shiftKey){var tree=ajaxSwingTrees[treeName];
if(tree){var i1=tree.selectedItemId;
var i2=nodeID;
if(i2<i1){var tmp=i1;
i1=i2;
i2=tmp
}for(i=0;
i<tree.nEntries;
i++){var cb=document.getElementById(treeName+"_S"+i);
if(cb){if((i>=i1)&&(i<=i2)&&(!cb.checked)){cb.click();
tree.indexOfEntries[i].setSelected(cb.checked)
}if(((i<i1)||(i>i2))&&(cb.checked)){cb.click();
tree.indexOfEntries[i].setSelected(cb.checked)
}}}}}else{var tree=ajaxSwingTrees[treeName];
if(tree){for(i=0;
i<tree.nEntries;
i++){var cb=document.getElementById(treeName+"_S"+i);
if(cb){if((i==nodeID)&&(!cb.checked)){cb.click();
if(tree.indexOfEntries[i]&&tree.indexOfEntries[i].setSelected){tree.indexOfEntries[i].setSelected(cb.checked)
}}if((i!=nodeID)&&(cb.checked)){cb.click();
if(tree.indexOfEntries[i]&&tree.indexOfEntries[i].setSelected){tree.indexOfEntries[i].setSelected(cb.checked)
}}}}}}}}function treeClickSEmulate(treeName,nodeID){var cb=document.getElementById(treeName+"_S"+nodeID);
if(cb){cb.click()
}}function AjaxSwingTree_setSelectedItemId(id){if((id!=this.selectedItemId)||(document.getElementById(this.name+"_S"+id))){if(!(this.selectedItemId==0&&this.isRootVisible==false)){this.indexOfEntries[this.selectedItemId].setSelected(false)
}this.selectedItemId=id;
this.indexOfEntries[id].setSelected(true);
if(this.hasListeners==true){ajaxSwingSubmittingTableSelection=true;
if(this.blockUI){doSubmit("/event/"+this.name,true)
}else{doSubmit("/event/"+this.name,false)
}}}}function AjaxSwingTree_updateData(){var value=this.getSelectedItemId();
for(i=0;
i<this.nEntries;
i++){if(this.indexOfEntries[i].isOpen){value+=(","+i)
}}var input=document.getElementById(this.name+"Value");
if(input){input.setAttribute("value",value)
}else{removeTree(name)
}}function preSubmitAjaxSwingTrees(){for(var tree in ajaxSwingTrees){if(ajaxSwingTrees[tree]){ajaxSwingTrees[tree].updateData()
}}}function removeTree(name){ajaxSwingTrees[name]=null
}USETEXTLINKS=1;
doc=document;
browserVersion=0;
var ajaxSwingTrees={};
var ajaxSwingTreeImageBundles=new Object();
var ajaxSwingShowTimeoutId=null;
var ajaxSwingHideTimeoutId=null;
var ajaxSwingDynamicTooltipId=null;
var ajaxSwingToolTipX=null;
var ajaxSwingToolTipY=null;
function onShowDynamicToolTip(componentId){if(AjaxSwingCurrentlyDraggedElement){return 
}if(componentId==ajaxSwingDynamicTooltipId){return 
}if(ajaxSwingDynamicTooltipId!=null){hideDynamicToolTip()
}ajaxSwingShowTimeoutId=setTimeout("showDynamicToolTip('"+componentId+"');",500)
}function onHideDynamicToolTip(){if(ajaxSwingShowTimeoutId!=null){clearTimeout(ajaxSwingShowTimeoutId);
ajaxSwingShowTimeoutId=null
}if(ajaxSwingDynamicTooltipId!=null){hideDynamicToolTip()
}}function showDynamicToolTip(componentId){if(AjaxSwingCurrentlyDraggedElement){return 
}ajaxSwingDynamicTooltipId=componentId;
doSubmit("/mousemove/"+componentId+"/enter","false!important")
}function hideDynamicToolTip(){doSubmit("/mousemove/"+ajaxSwingDynamicTooltipId+"/exit","false!important");
ajaxSwingQueueEvent=false;
ajaxSwingDynamicTooltipId=null
}function onShowToolTip(componentId,tooltipId){if(AjaxSwingCurrentlyDraggedElement){return 
}if(ajaxSwingHideTimeoutId!=null){clearTimeout(ajaxSwingHideTimeoutId)
}if(ajaxSwingShowTimeoutId==null){ajaxSwingShowTimeoutId=setTimeout("showToolTip('"+componentId+"','"+tooltipId+"');",1500)
}jQuery("#"+componentId).mousemove(function(e){ajaxSwingToolTipX=e.pageX;
ajaxSwingToolTipY=e.pageY
})
}function onHideToolTip(tooltipId){if(ajaxSwingShowTimeoutId!=null){clearTimeout(ajaxSwingShowTimeoutId);
ajaxSwingShowTimeoutId=null
}hideToolTip(tooltipId)
}function showToolTip(componentId,tooltipId){if(AjaxSwingCurrentlyDraggedElement){return 
}var screen=document.getElementById("AjaxSwingScreen");
var $screen=$(screen);
var toolTip=document.getElementById(tooltipId);
var $toolTip=$(toolTip);
if(!toolTip){alert("unable to find tooltip, id: "+tooltipId);
return 
}if(!toolTip.oldParent){toolTip.oldParent=toolTip.parentNode
}if(toolTip.parentNode!=screen){screen.appendChild(toolTip)
}var screenOffset=$screen.offset();
var newLeft=ajaxSwingToolTipX-screenOffset.left;
$toolTip.css("display","block");
var left=ajaxSwingToolTipX-screenOffset.left+5;
if(left+toolTip.clientWidth>=screen.clientWidth){left=(screen.clientWidth-toolTip.clientWidth)-15
}var top=ajaxSwingToolTipY-screenOffset.top+15;
if(top+toolTip.clientHeight>=screen.clientHeight){top=(screen.clientHeight-toolTip.clientHeight)-15
}if(top!=(ajaxSwingToolTipY-screenOffset.top+15)&&left!=(ajaxSwingToolTipX-screenOffset.left+5)){left=ajaxSwingToolTipX-toolTip.clientWidth-5
}$toolTip.css("left",left);
$toolTip.css("top",top);
$toolTip.css("z-index",9000);
$("#"+componentId).unbind("mousemove");
$toolTip.fadeIn(200);
ajaxSwingShowTimeoutId=null;
ajaxSwingHideTimeoutId=setTimeout("hideToolTip('"+tooltipId+"');",10000)
}function hideToolTip(tooltipId){if(ajaxSwingHideTimeoutId!=null){clearTimeout(ajaxSwingHideTimeoutId);
ajaxSwingHideTimeoutId=null
}var toolTip=document.getElementById(tooltipId);
if(toolTip){if($(toolTip).css("display")=="block"){$(toolTip).fadeOut(200,function(){if(toolTip.oldParent){toolTip.oldParent.appendChild(toolTip);
$(toolTip).css("display","none")
}else{alert("old parent = "+toolTip.oldParent);
removeElement(toolTip)
}})
}}};