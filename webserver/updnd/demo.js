/***************************************************************
 * 				
 * 			DEMO Page
 * 
 ***************************************************************/
/**
 * 
 * @returns
 */
function prepareDownload_Demo(){
	
	var server = document.getElementById('server').value;
	var sessionID = document.getElementById('sessionId').value;
	var additional = document.getElementById('additional').value
	
	var remoteDir = document.getElementById('server.dir').value;
	var fileList = document.getElementById('fileNamesList').value;
	
	var localDir = document.getElementById('dir.local').value;
	
	var dialog = makeDownloadDialog(localDir, server, remoteDir, sessionID, fileList);
		dialog.appendChild(param("ADD_HEADER", additional));
	
	return cover(dialog);
}

/**
 * 
 * @param autorunDialog
 * @returns
 */
function prepareUpload_Demo(autorunDialog){

	var server = document.getElementById('server').value;
	var sessionID = document.getElementById('sessionId').value;		
	var additional = document.getElementById('additional').value
	
	var uploadDir = document.getElementById('server.dir').value;

	var dialog = makeUploadDialog('false', server, uploadDir, sessionID);
		dialog.appendChild(param("ADD_HEADER", additional));

	
	return cover(dialog);
}