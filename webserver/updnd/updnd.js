/***************************************************************
 * 				
 * 	Parse Applet params from the Manual File transfer Page
 * 
 ***************************************************************/
/**
 * Applet Width
 *  * @returns {Number}
 */
function getAppletWidth(){
	return 640;
}
/**
 * Applet Height
 * @returns {Number}
 */
function getAppletHeight(){
	return 400;
}

/**
 * Retrive path to the server
 * @returns {String}
 */
function createServerPath(serverPath){
	if(serverPath == null){
		serverPath  = '/server'; //default value
	}
	
	var protocol = window.location.protocol;
	var host =  window.location.host;
	var fileServer = serverPath;

	return protocol + '//' + host + fileServer;
}

/**
 * Get jSessionId from the cookie
 */
function parseSessionId(){
	var sessionID = document.cookie;		
	return sessionID.substring(sessionID.indexOf('=') + 1, sessionID.length);
}

/***************************************************************
 * 				
 * 			VLPortal
 * 
 ***************************************************************/

	/**
	 * Show the Applet
	 * Start the applet from the VLPortal
	 * @param element Upload/Download button id on the Manual File transfer page
	 */
	function showUpDndApplet(type, dir, serverPath, filesList){		
		
		//Get Server	
		var server = createServerPath(serverPath);
	
		//Get jSessionID
		var sessionID = parseSessionId();
		
		//
		var localDownloadFolder = null;

		var dialog = (type != null && type.toLowerCase() == "DOWNLOAD".toLowerCase()) ?
					makeDownloadDialog(server, dir, sessionID, localDownloadFolder, filesList) : 
					makeUploadDialog(server, dir, sessionID, 'true');
		
		// Process any remaining arguments as name:value pairs
		var nargs = arguments.length;		
    	if (nargs > 4) {
			for (i = 4; i < nargs; i++) { 
				var pair = arguments[i].split(':'); 
				dialog.appendChild(param(pair[0], pair[1]));
				// alert("Adding:" + pair[0] + " with value:" + pair[1]); 
			}
        }
		showAsIS(cover(dialog));
	}

/***************************************************
 * 
 * private
 *
 ***************************************************/
/**
 * Create <applet> tag
 */
function createTransferDialog(){
	var app = document.createElement('applet');
		app.id='upDndApplet';
		app.code= 'com.creamtec.updnd.UpDndApplet';
		app.codeBase='updnd';
		app.archive= 'updnd.jar';
		app.mayscript='mayscript';
	
	app.appendChild(param('cache_archive', 'lib/httpcore.jar, lib/httpclient.jar, lib/commons-logging.jar'));
	app.appendChild(param('java_arguments', '-Djnlp.packEnabled=true -Dorg.apache.commons.logging.LogFactory=org.apache.commons.logging.impl.LogFactoryImpl -Dorg.apache.commons.logging.Log=org.apache.commons.logging.impl.Jdk14Logger'));
	return app;
}
/**
 * Create a <param> tag
 * @param name param Name
 * @param value param Value
 * @returns {___param1}
 */
function param(name, value){
	var param = document.createElement('param');
	param.name = name;
	param.value = value;
	
	return param;
}

/**
 * Set the Applet params for Uploading
 * @param autorunDialog show File Choose Dialog after starting the applet
 * @param uploadDir server directory for uploaded files
 * @param server server address
 * @param sessionID jSessionId
 * @returns <applet> with the params
 */
function makeUploadDialog(server, uploadDir, sessionID, autorunDialog){
	
	var dialog = createTransferDialog();
		if(autorunDialog){
			dialog.appendChild(param("IS_AUTORUN", autorunDialog));
		}
		dialog.appendChild(param("TRANSFER_TYPE", "UPLOAD"));
		
		
		dialog.appendChild(param("SESSION_ID", sessionID));
		dialog.appendChild(param("DIR_REMOTE", uploadDir));
		dialog.appendChild(param("REMOTE_HOST", server));
		dialog.appendChild(param("LOG_LEVEL", 10));
	 return dialog;
}
/**
 * Set the Applet params for Downloading
 * @param localDir local dir for downloaded files
 * @param server  server address
 * @param remoteDir server folder where are the files to download
 * @param sessionID jSessionId
 * @param fileList string with files to download, separated by a semicolon
 * @returns
 */
function makeDownloadDialog(server, remoteDir, sessionID, localDir, fileList){

	var dialog = createTransferDialog();	
		dialog.appendChild(param("TRANSFER_TYPE", "DOWNLOAD"));
		
		dialog.appendChild(param("SESSION_ID", sessionID));
		dialog.appendChild(param("DIR_REMOTE", remoteDir));
		dialog.appendChild(param("REMOTE_HOST", server));
		
		dialog.appendChild(param("FILE_LIST", fileList));
		if(localDir != null){
			dialog.appendChild(param("DIR_LOCAL", localDir));
		}
		dialog.appendChild(param("LOG_LEVEL", 10));
		return dialog;
}



/*********************************************
 * Displaying
 *********************************************/
/**
 * Cover the <applet> tag 
 */
function cover(dialog){
	
	//Set <applet> styles
		dialog.width = getAppletWidth();
		dialog.height = getAppletHeight();
		dialog.style.cssText = '; margin:0; padding:0 '; 
	
	
	 var table = document.createElement('table');
		 table.width = '100%';
		 table.height = '100%';
		 table.setAttribute('class', 'updnd');
		 table.setAttribute('id', 'updndtable');
		 table.style.cssText  = "height: 100%; z-index: 100; position: absolute; top: 0px; left: 0px; text-align: center;";// + createOpacityStyle(0.7);
		
		 
		var span = document.createElement('span');
			span.setAttribute('class', 'close-btn');
			span.innerHTML = '<a href="#" onclick="closeApplet(); return false;" style="font-size: 24px; font-weight: bold; color: #CC0000; text-decoration: none;">	&times; </a>'; 
			span.style.cssText  = 'top: -25px;  right: -17px; position: absolute; padding: 1px 5px;';
		 
		var div =  document.createElement('div');
			div.style.cssText  = 'border: 15px solid #c2c2c2; border-top: 20px solid #c2c2c2; position: relative; margin:auto; width:' + dialog.width + 'px;  height: ' + dialog.height + 'px; opacity=1; '  + createOpacityStyle(1);
			
			div.appendChild(span);
			div.appendChild(dialog);
			
		var div_back =  document.createElement('span');
			div_back.style.cssText  = 'width: 100%; height: 100%; background:#111; ' + createOpacityStyle(0.7);
			div_back.innerHTML='&nbsp;';

			var td = document.createElement('td');
				td.appendChild(div);
				//td.appendChild(transp);
			
			   
			table.appendChild(document.createElement('tbody'))
				.appendChild(document.createElement('tr'))
				.appendChild(td);
			
			var transp = document.createElement('table');
				 transp.width = '100%';
				 transp.height = '100%';
				 transp.setAttribute('id', 'updndBackground');
				 transp.style.cssText  = "height: 100%; z-index: 99; background:#111; position: absolute; top: 0px; left: 0px; text-align: center;" + createOpacityStyle(0.7);
					
				 transp.appendChild(document.createElement('tbody'))
						.appendChild(document.createElement('tr'))
						.appendChild(document.createElement('td'));

			document.body.appendChild(transp);
			
		 return table;	
}
/**
 * Show the applet
 * @param dialog
 */
function showAsIS(dialog){
	document.body.appendChild(dialog);
}
/**
 * Remove the applet from the web-page
 * @param isPrompt true or skipped if needed show a dialog to confirm the applet closing
 */
function hide(){
	
	var app = document.getElementsByTagName('applet')[0];
	var decorTable = document.getElementById("updndtable");
	var backTable = document.getElementById("updndBackground");
		
	//Hide decoration
	decorTable.style.display='none';
	backTable.style.display='none';
	//Remove 
	app.parentNode.removeChild(app);
	decorTable.parentNode.removeChild(decorTable);
	backTable.parentNode.removeChild(backTable);
	

}

function closeApplet(){
	var applet = document.getElementById("upDndApplet");
		applet.close()
}

/**
 * Create cross-browser opacity style
 * @param opacity opacity level
 * @returns {String}
 */
function createOpacityStyle(opacity){

	if(opacity == 0){
		return '';		
	}else if(opacity > 1){
		opacity = opacity/100;
	}
	
	var str = 'filter: alpha(opacity=' + (opacity*100) + ' ); -khtml-opacity: ' + opacity + ' ;  -moz-opacity: ' + opacity + ' ; opacity: ' + opacity + ';';
	
	return str;	
}