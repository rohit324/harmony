<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE helpset
  PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 1.0//EN" "helpset_1_0.dtd">
<helpset version="2.0">
   <title>  User Guide</title>
   <maps>
      <homeID>CommandRefConrefs</homeID>
      <mapref location="userguide.jhm"/>
   </maps>
   <view>
      <name>TOC</name>
      <label>TOC</label>
      <type>javax.help.TOCView</type>
      <data>userguide.xml</data>
   </view>
   <view mergetype="javax.help.AppendMerge">
      <name>index</name>
      <label>Index</label>
      <type>javax.help.IndexView</type>
      <data>userguide_index.xml</data>
   </view>
</helpset>