{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "A transfer.",
  "type": "object",
  "properties": {
    "id": {
      "description": "The unique identifier for the transfer.",
      "type": "string"
    },
    "serial": {
      "description": "The serial number of the performing VersaLex instance for the transfer.",
      "type": "string"
    },
    "nodeName": {
      "description": "The name of the performing VersaLex instance for the transfer, if configured.",
      "type": "string"
    },
    "status": {
      "description": "The current or final status of the transfer.",
      "enum": [
        "inProgress",
        "receiptPending",
        "success",
        "error",
        "warning",
        "interrupted",
        "exception",
        "discarded",
        "cancelled",
        "interimSuccess",
        "interimWarning",
        "deleteError",
        "deleteResolved"
      ]
    },
    "direction": {
      "description": "The direction of the transfer, regardless of whether solicited or unsolicited.",
      "enum": [
        "outgoing",
        "incoming",
        "local"
      ]
    },
    "originatingIP": {
      "description": "The originating IP address of the inbound connection, if known.",
      "type": "string"
    },
    "transport": {
      "description": "The transport used by the transfer.",
      "enum": [
        "as2",
        "ftp",
        "http",
        "lcopy",
        "sftp"
      ]
    },
    "sessionId": {
      "description": "The session identifier of the transfer, if applicable to the transport.",
      "type": "string"
    },
    "messageId": {
      "description": "The message idenifier of the transfer, if applicable to the transport.",
      "type": "string"
    },
    "fileCount": {
      "description": "The number of files included in the transfer.",
      "type": "integer"
    },
    "seconds": {
      "description": "The total duration of the transfer in seconds, if successful.",
      "type": "number"
    },
    "bytes": {
      "description": "The total number of bytes transferred, if successful.",
      "type": "integer"
    },
    "runtype": {
      "description": "How the action that initiated the transfer was run, if applicable. For example, `api` if the action was run interactively through the REST API, or `scheduled` if run through the VersaLex scheduler.",
      "enum": [
        "autorun",
        "scheduled",
        "interactive",
        "commandline",
        "startup",
        "support",
        "softwareUpdate",
        "download",
        "licensing",
        "api",
        "webservice",
        "router",
        "database",
        "pipe",
        "service",
        "resend",
        "rereceive",
        "keyManagement",
        "ancillary",
        "unsolicited",
        "executeOn"
      ]
    },
    "meta": {
      "description": "Additional information about the transfer.",
      "type": "object",
      "properties": {
        "resourceType": {
          "description": "The type of resource, always `transfer`.",
          "type": "string"
        },
        "created": {
          "description": "The date and time the transfer was started.",
          "type": "string",
          "format": "date-time"
        },
        "lastModified": {
          "description": "The date and time the state of the transfer last changed.",
          "type": "string",
          "format": "date-time"
        }
      },
      "additionalProperties": false
    },
    "_links": {
      "description": "An object that contains objects with useful URIs:\n\n- The URI of the transfer itself.\n- The URI of the user associated with the transfer, if applicable.\n- The URI of the connection associated with the transfer, if applicable.\n- The URI of the action that initiated the transfer, if applicable.\n- The URI of the receipt for the transfer, if applicable, for example, an AS2 MDN.\n- The URI to resend the payload, if outgoing.\n- The URI to rereceive the payload, if incoming.\n- The URI of the events associated with the transfer.\n- The URI of the files included in the transfer.",
      "type": "object",
      "properties": {
        "self": {
          "description": "The transfer itself.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "user": {
          "description": "The user associated with the transfer, if applicable.",
          "properties": {
            "href": {
              "description": "The URI of the user resource.",
              "type": "string",
              "pattern": "^.*/.*$"
            },
            "username": {
              "description": "The user's username.",
              "type": "string"
            }
          }
        },
        "connection": {
          "description": "The connection associated with the transfer, if applicable.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "action": {
          "description": "The action that initiated the transfer, if applicable.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "receipt": {
          "description": "The receipt for the transfer, if applicable, for example, an AS2 MDN.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "resend": {
          "description": "Resend the transfer payload, if outgoing.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "rereceive": {
          "description": "Re-receive the transfer payload, if incoming.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "events": {
          "description": "The events associated to the transfer.",
          "$ref": "common.schema#/definitions/HALlink"
        },
        "files": {
          "description": "The files included in the transfer.",
          "type": "array",
          "items": {
            "description": "A file.",
            "type": "object",
            "properties": {
              "href": {
                "description": "The URI of the event associated with the file.",
                "type": "string",
                "pattern": "^.*/.*$"
              },
              "source": {
                "description": "The path of the outgoing file, if applicable.",
                "type": "string"
              },
              "destination": {
                "description": "The path of the incoming file, if applicable.",
                "type": "string"
              },
              "filesize": {
                "description": "The size of the file in bytes.",
                "type": "integer"
              },
              "fileTimeStamp": {
                "description": "The date and time the file was last modified.",
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "additionalProperties": false
        },
        "additionalProperties": false
      }
    }
  }
}
